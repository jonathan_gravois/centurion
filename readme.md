## CENTURION -- The Api-Guard Test Environment

### Installation

* Clone this repository  
* run ```composer install``` in the Terminal  
* create an empty mySQL database (I named my centurion)  
* rename ```.env.example``` file to ```.env``` and edit database settings
* run ```composer dump-autoload``` in the Terminal  
* run ```php artisan migrate``` in the Terminal  
* run ```php artisan migrate:refresh --seed ``` in the Terminal  
* run ```php -S localhost:8888 -t public``` in the Terminal to test in browser

### Laravel PHP Framework

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

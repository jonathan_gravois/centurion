<?php

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;
use App\Condition as Condition;

class ConditionsTableSeeder extends Seeder
{

    public function run()
    {
        Condition::create([
            'condition'	=>	'Agricultural Security Agreement on Crops and Equipment'
        ]);
        Condition::create([
            'condition'	=>	'Assignment of Crop Insurance'
        ]);
        Condition::create([
            'condition'	=>	'Assignment of Rebates'
        ]);
        Condition::create([
            'condition'	=>	'Assignment of FSA Direct and LDP Payment'
        ]);
        Condition::create([
            'condition'	=>	'Approval by Distributor'
        ]);
        Condition::create([
            'condition'	=>	'Personal Guarantee'
        ]);
        Condition::create([
            'condition'	=>	'Cross-Collateralized Loan'
        ]);
        Condition::create([
            'condition'	=>	'Controlled Disbursements'
        ]);
        Condition::create([
            'condition' => 'Grain Storage Agreement'
        ]);
        Condition::create([
            'condition'	=>	'Freeform Conditions'
        ]);
    }

}
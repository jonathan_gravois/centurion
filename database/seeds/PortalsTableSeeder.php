<?php

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;
use App\Portal as Portal;

class PortalsTableSeeder extends Seeder {

    public function run()
    {
        Portal::create([
            'portal' => 'Admin'
        ]);

        Portal::create([
            'portal' => 'Staff'
        ]);

        Portal::create([
            'portal' => 'Client'
        ]);

        Portal::create([
            'portal' => 'Vendor'
        ]);
    }

}
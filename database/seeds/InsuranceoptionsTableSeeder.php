<?php

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;
use App\Insuranceoption as Insuranceoption;

class InsuranceoptionsTableSeeder extends Seeder
{

    public function run()
    {
        Insuranceoption::create([
            'option' => 'EUBUPT'
        ]);

        Insuranceoption::create([
            'option' => 'BU'
        ]);

        Insuranceoption::create([
            'option' => 'RP'
        ]);

        Insuranceoption::create([
            'option' => 'PT'
        ]);

        Insuranceoption::create([
            'option' => 'PF'
        ]);
    }

}
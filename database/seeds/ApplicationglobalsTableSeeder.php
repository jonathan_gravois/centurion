<?php

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;
use App\Applicationglobal as Applicationglobal;

class ApplicationglobalsTableSeeder extends Seeder
{

    public function run()
    {
        Applicationglobal::create([
            'crop_year' => '2015',
            'season' => 'S'
        ]);
    }

}
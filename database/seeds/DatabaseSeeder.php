<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$seeders = [
		    'AddendumtypesTableSeeder',
		    'ApplicationglobalsTableSeeder',
		    'CommitteespecsTableSeeder',
		    'ConditionsTableSeeder',
		    'CountiesTableSeeder',
		    'CountycropdefaultsTableSeeder',
		    'CropsTableSeeder',
		    'DefaultexpensesTableSeeder',
		    'DistributorsTableSeeder',
		    'EntitytypesTableSeeder',
		    'ExceptionsTableSeeder',
		    'InsuranceoptionsTableSeeder',
		    'InsurancetypesTableSeeder',
		    'LoanstatusesTableSeeder',
		    'LoantypesTableSeeder',
		    'LocationsTableSeeder',
		    'MatricesTableSeeder',
		    'PdfappsTableSeeder',
		    'PortalsTableSeeder',
		    'RatioconstraintsTableSeeder',
		    'RegionsTableSeeder',
		    'ReportsTableSeeder',
		    'RequireddocumentsTableSeeder',
		    'RolesTableSeeder',
		    'ScreensTableSeeder',
		    'StatesTableSeeder',
		    'UnitsTableSeeder',
		    
		    // DEV ONLY	    
		    'AddfinsTableSeeder',
		    'AddloansTableSeeder',
		    'AdmingradersTableSeeder',
		    'AgenciesTableSeeder',
		    'AgentsTableSeeder',
		    'ApplicantsTableSeeder',
		    'CalendarsTableSeeder',
		    'CommentsTableSeeder',
		    'CommentstatusTableSeeder',
		    'CommitteesTableSeeder',
		    'CorporationsTableSeeder',
		    'CropdetailsTableSeeder',
		    'CropexpensesTableSeeder',
		    'CroppracticesTableSeeder',
		    'FarmcropsTableSeeder',
		    'FarmersTableSeeder',
		    'FarmexpensesTableSeeder',
		    'FarmsTableSeeder',
		    'GrainstoragesTableSeeder',
		    'GuarantorsTableSeeder',
		    'InsurancesTableSeeder',
		    'JointventuresTableSeeder',
		    'LoanassetsTableSeeder',
		    'LoancapacitiesTableSeeder',
		    'LoanconditionsTableSeeder',
		    'LoancropsTableSeeder',
		    'LoandistributorsTableSeeder',
		    'LoanexceptionsTableSeeder',
		    'LoanfinancialsTableSeeder',
		    'LoanpracticesTableSeeder',
		    'LoanquestionsTableSeeder',
		    'LoansTableSeeder',
		    'NotificationsTableSeeder',
		    'PartnersTableSeeder',
		    'PrerequisitesTableSeeder',
		    'PriorliensTableSeeder',
		    'ProfilesTableSeeder',
		    'ReferencesTableSeeder',
		    'SpendcatsTableSeeder',
		    'SystemicsTableSeeder',
		    'UsersTableSeeder',
		    'ViewoptionsTableSeeder'
		];
		
		foreach ($seeders as $seeder)
        {
            $this->call($seeder);
        }
	}

}

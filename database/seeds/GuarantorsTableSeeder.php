<?php

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;
use App\Guarantor as Guarantor;

class GuarantorsTableSeeder extends Seeder
{

    public function run()
    {
        Guarantor::create([
            'loan_id' => 1,
            'guarantor' => 'Bo Gwin'
        ]);

        Guarantor::create([
            'loan_id' => 1,
            'guarantor' => 'Sharon Gwin'
        ]);
    }

}
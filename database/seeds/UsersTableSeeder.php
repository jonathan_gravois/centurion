<?php

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;
use App\User as User;

class UsersTableSeeder extends Seeder {

    public function run()
    {
        User::create([
            'username' => 'LENDA',
            'email' => 'lenda@armlend.com',
            'password' => 'gluestick',
            'portal_id' => 1,
            'loc_id' => 0,
            'region_id' => 2,
            'manager_id'  => 1,
            'is_admin'    => 0,
            'is_approver' => 0,
            'is_manager'  => 0,
            'role_id' => 16
        ]);

        User::create([
            'username' => 'Jonathan Gravois',
            'email' => 'jongravois@gmail.com',
            'password' => 'password',
            'phone'	=> '9012870209',
            'portal_id' => 1,
            'loc_id' => 6,
            'region_id' => 1,
            'manager_id' => 2,
            'is_admin' => 1,
            'is_approver' => 1,
            'is_manager' => 1,
            'role_id' => 14
        ]);

        User::create([
            'username' => 'Brad Terral',
            'email' => 'bterral@armlend.com',
            'password' => 'changeme',
            'phone' => '3182824037',
            'portal_id' => 1,
            'loc_id' => 5,
            'region_id' => 2,
            'manager_id' => 3,
            'is_admin' => 1,
            'is_approver' => 1,
            'is_manager' => 1,
            'role_id' => 1
        ]);

        User::create([
            'username' => 'Mark Branch',
            'email' => 'mbranch@armlend.com',
            'password' => 'changeme',
            'phone' => '3187285770',
            'region_id' => 2,
            'portal_id' => 1,
            'loc_id' => 5,
            'manager_id' => 3,
            'is_admin' => 0,
            'is_approver' => 1,
            'is_manager' => 1,
            'role_id' => 2
        ]);

        User::create([
            'username' => 'Robby Miller',
            'email' => 'rmiller@armlend.com',
            'password' => 'changeme',
            'phone' => '3187285770',
            'portal_id' => 2,
            'loc_id' => 5,
            'region_id' => 2,
            'manager_id' => 4,
            'is_admin' => 0,
            'is_approver' => 1,
            'is_manager' => 1,
            'role_id' => 4
        ]);

        User::create([
            'username' => 'Mason Williams',
            'email' => 'mwilliams@armlend.com',
            'password' => 'changeme',
            'phone' => '3187285770',
            'portal_id' => 2,
            'loc_id' => 5,
            'region_id' => 2,
            'manager_id' => 5,
            'is_admin' => 0,
            'is_approver' => 0,
            'is_manager' => 0,
            'role_id' => 4
        ]);

        User::create([
            'username' => 'Katie Williams',
            'email' => 'kwilliams@armlend.com',
            'password' => 'changeme',
            'phone' => '3187285770',
            'portal_id' => 2,
            'loc_id' => 5,
            'region_id' => 2,
            'manager_id' => 4,
            'is_admin' => 0,
            'is_approver' => 0,
            'is_manager' => 0,
            'role_id' => 9
        ]);

        User::create([
            'username' => 'Kenn Thompson',
            'email' => 'kennthompson@gmail.com',
            'password' => 'chessmaster',
            'phone' => '9991235648',
            'portal_id' => 1,
            'loc_id' => 4,
            'region_id' => 1,
            'manager_id' => 2,
            'is_admin' => 1,
            'is_approver' => 1,
            'is_manager' => 1,
            'role_id' => 14
        ]);

        User::create([
            'username' => 'Tony Stark',
            'email' => 'ironman@gmail.com',
            'password' => 'changeme',
            'phone' => '9991326548',
            'portal_id' => 3,
            'loc_id' => 5,
            'region_id' => 1,
            'manager_id' => 2,
            'is_admin' => 0,
            'is_approver' => 0,
            'is_manager' => 0,
            'role_id' => 17
        ]);
    }

}
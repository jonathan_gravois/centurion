<?php

use Illuminate\Database\Seeder;
use App\County as County;

class CountiesTableSeeder extends Seeder {

    public function run()
    {
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Autauga",
            "label" => "Autauga ( AL )",
            "locale" => "AL | Autauga"
        ]);
        County::create([
            "state_id" =>  1,
            "location_id" =>  0,
            "county" =>  "Baldwin",
            "label" =>  "Baldwin ( AL )",
            "locale" =>  "AL | Baldwin"
        ]);
        County::create([
            "state_id" =>  1,
            "location_id" =>  0,
            "county" =>  "Barbour",
            "label" =>  "Barbour ( AL )",
            "locale" =>  "AL | Barbour"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Bibb",
            "label" => "Bibb ( AL )",
            "locale" => "AL | Bibb"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Blount",
            "label" => "Blount ( AL )",
            "locale" => "AL | Blount"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Bullock",
            "label" => "Bullock ( AL )",
            "locale" => "AL | Bullock"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Butler",
            "label" => "Butler ( AL )",
            "locale" => "AL | Butler"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Calhoun",
            "label" => "Calhoun ( AL )",
            "locale" => "AL | Calhoun"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Chambers",
            "label" => "Chambers ( AL )",
            "locale" => "AL | Chambers"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Cherokee",
            "label" => "Cherokee ( AL )",
            "locale" => "AL | Cherokee"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Chilton",
            "label" => "Chilton ( AL )",
            "locale" => "AL | Chilton"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Choctaw",
            "label" => "Choctaw ( AL )",
            "locale" => "AL | Choctaw"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Clarke",
            "label" => "Clarke ( AL )",
            "locale" => "AL | Clarke"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Clay",
            "label" => "Clay ( AL )",
            "locale" => "AL | Clay"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Cleburne",
            "label" => "Cleburne ( AL )",
            "locale" => "AL | Cleburne"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Coffee",
            "label" => "Coffee ( AL )",
            "locale" => "AL | Coffee"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Colbert",
            "label" => "Colbert ( AL )",
            "locale" => "AL | Colbert"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Conecuh",
            "label" => "Conecuh ( AL )",
            "locale" => "AL | Conecuh"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Coosa",
            "label" => "Coosa ( AL )",
            "locale" => "AL | Coosa"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Covington",
            "label" => "Covington ( AL )",
            "locale" => "AL | Covington"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Crenshaw",
            "label" => "Crenshaw ( AL )",
            "locale" => "AL | Crenshaw"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Cullman",
            "label" => "Cullman ( AL )",
            "locale" => "AL | Cullman"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Dale",
            "label" => "Dale ( AL )",
            "locale" => "AL | Dale"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Dallas",
            "label" => "Dallas ( AL )",
            "locale" => "AL | Dallas"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "DeKalb",
            "label" => "DeKalb ( AL )",
            "locale" => "AL | DeKalb"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Elmore",
            "label" => "Elmore ( AL )",
            "locale" => "AL | Elmore"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Escambia",
            "label" => "Escambia ( AL )",
            "locale" => "AL | Escambia"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Etowah",
            "label" => "Etowah ( AL )",
            "locale" => "AL | Etowah"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Fayette",
            "label" => "Fayette ( AL )",
            "locale" => "AL | Fayette"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Franklin",
            "label" => "Franklin ( AL )",
            "locale" => "AL | Franklin"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Geneva",
            "label" => "Geneva ( AL )",
            "locale" => "AL | Geneva"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Greene",
            "label" => "Greene ( AL )",
            "locale" => "AL | Greene"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Hale",
            "label" => "Hale ( AL )",
            "locale" => "AL | Hale"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Henry",
            "label" => "Henry ( AL )",
            "locale" => "AL | Henry"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Houston",
            "label" => "Houston ( AL )",
            "locale" => "AL | Houston"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Jackson",
            "label" => "Jackson ( AL )",
            "locale" => "AL | Jackson"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Jefferson",
            "label" => "Jefferson ( AL )",
            "locale" => "AL | Jefferson"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Lamar",
            "label" => "Lamar ( AL )",
            "locale" => "AL | Lamar"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Lauderdale",
            "label" => "Lauderdale ( AL )",
            "locale" => "AL | Lauderdale"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Lawrence",
            "label" => "Lawrence ( AL )",
            "locale" => "AL | Lawrence"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Lee",
            "label" => "Lee ( AL )",
            "locale" => "AL | Lee"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Limestone",
            "label" => "Limestone ( AL )",
            "locale" => "AL | Limestone"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Lowndes",
            "label" => "Lowndes ( AL )",
            "locale" => "AL | Lowndes"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Macon",
            "label" => "Macon ( AL )",
            "locale" => "AL | Macon"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Madison",
            "label" => "Madison ( AL )",
            "locale" => "AL | Madison"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Marengo",
            "label" => "Marengo ( AL )",
            "locale" => "AL | Marengo"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Marion",
            "label" => "Marion ( AL )",
            "locale" => "AL | Marion"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Marshall",
            "label" => "Marshall ( AL )",
            "locale" => "AL | Marshall"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Mobile",
            "label" => "Mobile ( AL )",
            "locale" => "AL | Mobile"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Monroe",
            "label" => "Monroe ( AL )",
            "locale" => "AL | Monroe"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Montgomery",
            "label" => "Montgomery ( AL )",
            "locale" => "AL | Montgomery"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Morgan",
            "label" => "Morgan ( AL )",
            "locale" => "AL | Morgan"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Perry",
            "label" => "Perry ( AL )",
            "locale" => "AL | Perry"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Pickens",
            "label" => "Pickens ( AL )",
            "locale" => "AL | Pickens"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Pike",
            "label" => "Pike ( AL )",
            "locale" => "AL | Pike"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Randolph",
            "label" => "Randolph ( AL )",
            "locale" => "AL | Randolph"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Russell",
            "label" => "Russell ( AL )",
            "locale" => "AL | Russell"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Shelby",
            "label" => "Shelby ( AL )",
            "locale" => "AL | Shelby"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "St. Clair",
            "label" => "St. Clair ( AL )",
            "locale" => "AL | St. Clair"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Sumter",
            "label" => "Sumter ( AL )",
            "locale" => "AL | Sumter"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Talladega",
            "label" => "Talladega ( AL )",
            "locale" => "AL | Talladega"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Tallapoosa",
            "label" => "Tallapoosa ( AL )",
            "locale" => "AL | Tallapoosa"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Tuscaloosa",
            "label" => "Tuscaloosa ( AL )",
            "locale" => "AL | Tuscaloosa"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Walker",
            "label" => "Walker ( AL )",
            "locale" => "AL | Walker"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Washington",
            "label" => "Washington ( AL )",
            "locale" => "AL | Washington"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Wilcox",
            "label" => "Wilcox ( AL )",
            "locale" => "AL | Wilcox"
        ]);
        County::create([
            "state_id" => 1,
            "location_id" => 0,
            "county" => "Winston",
            "label" => "Winston ( AL )",
            "locale" => "AL | Winston"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Akhiok",
            "label" => "Akhiok ( AK )",
            "locale" => "AK | Akhiok"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Akiachak",
            "label" => "Akiachak ( AK )",
            "locale" => "AK | Akiachak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Akiak",
            "label" => "Akiak ( AK )",
            "locale" => "AK | Akiak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Akutan",
            "label" => "Akutan ( AK )",
            "locale" => "AK | Akutan"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Alakanuk",
            "label" => "Alakanuk ( AK )",
            "locale" => "AK | Alakanuk"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Alatna",
            "label" => "Alatna ( AK )",
            "locale" => "AK | Alatna"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Aleknagik",
            "label" => "Aleknagik ( AK )",
            "locale" => "AK | Aleknagik"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Algaaciq",
            "label" => "Algaaciq ( AK )",
            "locale" => "AK | Algaaciq"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Allakaket",
            "label" => "Allakaket ( AK )",
            "locale" => "AK | Allakaket"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Ambler",
            "label" => "Ambler ( AK )",
            "locale" => "AK | Ambler"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Anaktuvuk Pass",
            "label" => "Anaktuvuk Pass ( AK )",
            "locale" => "AK | Anaktuvuk Pass"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Andreafsky",
            "label" => "Andreafsky ( AK )",
            "locale" => "AK | Andreafsky"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Angoon",
            "label" => "Angoon ( AK )",
            "locale" => "AK | Angoon"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Aniak",
            "label" => "Aniak ( AK )",
            "locale" => "AK | Aniak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Anvik",
            "label" => "Anvik ( AK )",
            "locale" => "AK | Anvik"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Arctic Village",
            "label" => "Arctic Village ( AK )",
            "locale" => "AK | Arctic Village"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Atka",
            "label" => "Atka ( AK )",
            "locale" => "AK | Atka"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Atmautluak",
            "label" => "Atmautluak ( AK )",
            "locale" => "AK | Atmautluak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Atqasuk",
            "label" => "Atqasuk ( AK )",
            "locale" => "AK | Atqasuk"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Barrow",
            "label" => "Barrow ( AK )",
            "locale" => "AK | Barrow"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Beaver",
            "label" => "Beaver ( AK )",
            "locale" => "AK | Beaver"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Belkofski",
            "label" => "Belkofski ( AK )",
            "locale" => "AK | Belkofski"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Bethel",
            "label" => "Bethel ( AK )",
            "locale" => "AK | Bethel"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Bill Moore's",
            "label" => "Bill Moore's ( AK )",
            "locale" => "AK | Bill Moore's"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Birch Creek",
            "label" => "Birch Creek ( AK )",
            "locale" => "AK | Birch Creek"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Brevig Mission",
            "label" => "Brevig Mission ( AK )",
            "locale" => "AK | Brevig Mission"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Buckland",
            "label" => "Buckland ( AK )",
            "locale" => "AK | Buckland"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Cantwell",
            "label" => "Cantwell ( AK )",
            "locale" => "AK | Cantwell"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Canyon Village",
            "label" => "Canyon Village ( AK )",
            "locale" => "AK | Canyon Village"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Chalkyitsik",
            "label" => "Chalkyitsik ( AK )",
            "locale" => "AK | Chalkyitsik"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Chefornak",
            "label" => "Chefornak ( AK )",
            "locale" => "AK | Chefornak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Chenega",
            "label" => "Chenega ( AK )",
            "locale" => "AK | Chenega"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Chevak",
            "label" => "Chevak ( AK )",
            "locale" => "AK | Chevak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Chickaloon",
            "label" => "Chickaloon ( AK )",
            "locale" => "AK | Chickaloon"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Chignik",
            "label" => "Chignik ( AK )",
            "locale" => "AK | Chignik"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Chignik Lagoon",
            "label" => "Chignik Lagoon ( AK )",
            "locale" => "AK | Chignik Lagoon"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Chignik Lake",
            "label" => "Chignik Lake ( AK )",
            "locale" => "AK | Chignik Lake"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Chilkat",
            "label" => "Chilkat ( AK )",
            "locale" => "AK | Chilkat"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Chilkoot",
            "label" => "Chilkoot ( AK )",
            "locale" => "AK | Chilkoot"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Chistochina",
            "label" => "Chistochina ( AK )",
            "locale" => "AK | Chistochina"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Chitina",
            "label" => "Chitina ( AK )",
            "locale" => "AK | Chitina"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Chuathbaluk",
            "label" => "Chuathbaluk ( AK )",
            "locale" => "AK | Chuathbaluk"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Chulloonawick",
            "label" => "Chulloonawick ( AK )",
            "locale" => "AK | Chulloonawick"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Circle",
            "label" => "Circle ( AK )",
            "locale" => "AK | Circle"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Clarks Point",
            "label" => "Clarks Point ( AK )",
            "locale" => "AK | Clarks Point"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Copper Center",
            "label" => "Copper Center ( AK )",
            "locale" => "AK | Copper Center"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Council",
            "label" => "Council ( AK )",
            "locale" => "AK | Council"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Craig",
            "label" => "Craig ( AK )",
            "locale" => "AK | Craig"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Crooked Creek",
            "label" => "Crooked Creek ( AK )",
            "locale" => "AK | Crooked Creek"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Deering",
            "label" => "Deering ( AK )",
            "locale" => "AK | Deering"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Dillingham",
            "label" => "Dillingham ( AK )",
            "locale" => "AK | Dillingham"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Dot Lake",
            "label" => "Dot Lake ( AK )",
            "locale" => "AK | Dot Lake"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Douglas",
            "label" => "Douglas ( AK )",
            "locale" => "AK | Douglas"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Eagle",
            "label" => "Eagle ( AK )",
            "locale" => "AK | Eagle"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Eek",
            "label" => "Eek ( AK )",
            "locale" => "AK | Eek"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Egegik",
            "label" => "Egegik ( AK )",
            "locale" => "AK | Egegik"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Eklutna",
            "label" => "Eklutna ( AK )",
            "locale" => "AK | Eklutna"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Ekuk",
            "label" => "Ekuk ( AK )",
            "locale" => "AK | Ekuk"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Ekwok",
            "label" => "Ekwok ( AK )",
            "locale" => "AK | Ekwok"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Elim",
            "label" => "Elim ( AK )",
            "locale" => "AK | Elim"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Emmonak",
            "label" => "Emmonak ( AK )",
            "locale" => "AK | Emmonak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Evansville",
            "label" => "Evansville ( AK )",
            "locale" => "AK | Evansville"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Eyak",
            "label" => "Eyak ( AK )",
            "locale" => "AK | Eyak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "False Pass",
            "label" => "False Pass ( AK )",
            "locale" => "AK | False Pass"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Fort Yukon",
            "label" => "Fort Yukon ( AK )",
            "locale" => "AK | Fort Yukon"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Gakona",
            "label" => "Gakona ( AK )",
            "locale" => "AK | Gakona"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Galena",
            "label" => "Galena ( AK )",
            "locale" => "AK | Galena"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Gambell",
            "label" => "Gambell ( AK )",
            "locale" => "AK | Gambell"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Georgetown",
            "label" => "Georgetown ( AK )",
            "locale" => "AK | Georgetown"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Golovin",
            "label" => "Golovin ( AK )",
            "locale" => "AK | Golovin"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Goodnews Bay",
            "label" => "Goodnews Bay ( AK )",
            "locale" => "AK | Goodnews Bay"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Grayling",
            "label" => "Grayling ( AK )",
            "locale" => "AK | Grayling"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Gulkana",
            "label" => "Gulkana ( AK )",
            "locale" => "AK | Gulkana"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Hamilton",
            "label" => "Hamilton ( AK )",
            "locale" => "AK | Hamilton"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Healy Lake",
            "label" => "Healy Lake ( AK )",
            "locale" => "AK | Healy Lake"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Holy Cross",
            "label" => "Holy Cross ( AK )",
            "locale" => "AK | Holy Cross"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Hoonah",
            "label" => "Hoonah ( AK )",
            "locale" => "AK | Hoonah"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Hooper Bay",
            "label" => "Hooper Bay ( AK )",
            "locale" => "AK | Hooper Bay"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Hughes",
            "label" => "Hughes ( AK )",
            "locale" => "AK | Hughes"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Huslia",
            "label" => "Huslia ( AK )",
            "locale" => "AK | Huslia"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Hydaburg",
            "label" => "Hydaburg ( AK )",
            "locale" => "AK | Hydaburg"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Igiugig",
            "label" => "Igiugig ( AK )",
            "locale" => "AK | Igiugig"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Iliamna",
            "label" => "Iliamna ( AK )",
            "locale" => "AK | Iliamna"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Inalik",
            "label" => "Inalik ( AK )",
            "locale" => "AK | Inalik"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Ivanof Bay",
            "label" => "Ivanof Bay ( AK )",
            "locale" => "AK | Ivanof Bay"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kake",
            "label" => "Kake ( AK )",
            "locale" => "AK | Kake"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kaktovik",
            "label" => "Kaktovik ( AK )",
            "locale" => "AK | Kaktovik"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kalskag",
            "label" => "Kalskag ( AK )",
            "locale" => "AK | Kalskag"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kaltag",
            "label" => "Kaltag ( AK )",
            "locale" => "AK | Kaltag"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Karluk",
            "label" => "Karluk ( AK )",
            "locale" => "AK | Karluk"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kasaan",
            "label" => "Kasaan ( AK )",
            "locale" => "AK | Kasaan"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kasigluk",
            "label" => "Kasigluk ( AK )",
            "locale" => "AK | Kasigluk"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kenaitze",
            "label" => "Kenaitze ( AK )",
            "locale" => "AK | Kenaitze"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Ketchikan",
            "label" => "Ketchikan ( AK )",
            "locale" => "AK | Ketchikan"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kiana",
            "label" => "Kiana ( AK )",
            "locale" => "AK | Kiana"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "King Cove",
            "label" => "King Cove ( AK )",
            "locale" => "AK | King Cove"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "King Salmon",
            "label" => "King Salmon ( AK )",
            "locale" => "AK | King Salmon"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kipnuk",
            "label" => "Kipnuk ( AK )",
            "locale" => "AK | Kipnuk"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kivalina",
            "label" => "Kivalina ( AK )",
            "locale" => "AK | Kivalina"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Klawock",
            "label" => "Klawock ( AK )",
            "locale" => "AK | Klawock"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Knik",
            "label" => "Knik ( AK )",
            "locale" => "AK | Knik"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kobuk",
            "label" => "Kobuk ( AK )",
            "locale" => "AK | Kobuk"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kodiak",
            "label" => "Kodiak ( AK )",
            "locale" => "AK | Kodiak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kokhanok",
            "label" => "Kokhanok ( AK )",
            "locale" => "AK | Kokhanok"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kongiganak",
            "label" => "Kongiganak ( AK )",
            "locale" => "AK | Kongiganak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kotlik",
            "label" => "Kotlik ( AK )",
            "locale" => "AK | Kotlik"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kotzebue",
            "label" => "Kotzebue ( AK )",
            "locale" => "AK | Kotzebue"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Koyuk",
            "label" => "Koyuk ( AK )",
            "locale" => "AK | Koyuk"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Koyukuk",
            "label" => "Koyukuk ( AK )",
            "locale" => "AK | Koyukuk"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kwethluk",
            "label" => "Kwethluk ( AK )",
            "locale" => "AK | Kwethluk"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kwigillingok",
            "label" => "Kwigillingok ( AK )",
            "locale" => "AK | Kwigillingok"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Kwinhagak",
            "label" => "Kwinhagak ( AK )",
            "locale" => "AK | Kwinhagak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Lake Minchumina",
            "label" => "Lake Minchumina ( AK )",
            "locale" => "AK | Lake Minchumina"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Larsen Bay",
            "label" => "Larsen Bay ( AK )",
            "locale" => "AK | Larsen Bay"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Lesnoi",
            "label" => "Lesnoi ( AK )",
            "locale" => "AK | Lesnoi"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Levelock",
            "label" => "Levelock ( AK )",
            "locale" => "AK | Levelock"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Lime Village",
            "label" => "Lime Village ( AK )",
            "locale" => "AK | Lime Village"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Lower Kalskag",
            "label" => "Lower Kalskag ( AK )",
            "locale" => "AK | Lower Kalskag"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Manley Hot Springs",
            "label" => "Manley Hot Springs ( AK )",
            "locale" => "AK | Manley Hot Springs"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Manokotak",
            "label" => "Manokotak ( AK )",
            "locale" => "AK | Manokotak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Marshall",
            "label" => "Marshall ( AK )",
            "locale" => "AK | Marshall"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Mary's Igloo",
            "label" => "Mary's Igloo ( AK )",
            "locale" => "AK | Mary's Igloo"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "McGrath",
            "label" => "McGrath ( AK )",
            "locale" => "AK | McGrath"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Mekoryuk",
            "label" => "Mekoryuk ( AK )",
            "locale" => "AK | Mekoryuk"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Mentasta Lake",
            "label" => "Mentasta Lake ( AK )",
            "locale" => "AK | Mentasta Lake"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Minto",
            "label" => "Minto ( AK )",
            "locale" => "AK | Minto"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Mountain Village",
            "label" => "Mountain Village ( AK )",
            "locale" => "AK | Mountain Village"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Naknek",
            "label" => "Naknek ( AK )",
            "locale" => "AK | Naknek"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Nanwalek",
            "label" => "Nanwalek ( AK )",
            "locale" => "AK | Nanwalek"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Napaimute",
            "label" => "Napaimute ( AK )",
            "locale" => "AK | Napaimute"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Napakiak",
            "label" => "Napakiak ( AK )",
            "locale" => "AK | Napakiak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Napaskiak",
            "label" => "Napaskiak ( AK )",
            "locale" => "AK | Napaskiak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Nelson Lagoon",
            "label" => "Nelson Lagoon ( AK )",
            "locale" => "AK | Nelson Lagoon"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Nenana",
            "label" => "Nenana ( AK )",
            "locale" => "AK | Nenana"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "New Koliganek",
            "label" => "New Koliganek ( AK )",
            "locale" => "AK | New Koliganek"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "New Stuyahok",
            "label" => "New Stuyahok ( AK )",
            "locale" => "AK | New Stuyahok"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Newhalen",
            "label" => "Newhalen ( AK )",
            "locale" => "AK | Newhalen"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Newtok",
            "label" => "Newtok ( AK )",
            "locale" => "AK | Newtok"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Nightmute",
            "label" => "Nightmute ( AK )",
            "locale" => "AK | Nightmute"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Nikolai",
            "label" => "Nikolai ( AK )",
            "locale" => "AK | Nikolai"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Nikolski",
            "label" => "Nikolski ( AK )",
            "locale" => "AK | Nikolski"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Ninilchik",
            "label" => "Ninilchik ( AK )",
            "locale" => "AK | Ninilchik"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Noatak",
            "label" => "Noatak ( AK )",
            "locale" => "AK | Noatak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Nome",
            "label" => "Nome ( AK )",
            "locale" => "AK | Nome"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Nondalton",
            "label" => "Nondalton ( AK )",
            "locale" => "AK | Nondalton"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Noorvik",
            "label" => "Noorvik ( AK )",
            "locale" => "AK | Noorvik"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Northway",
            "label" => "Northway ( AK )",
            "locale" => "AK | Northway"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Nuiqsut",
            "label" => "Nuiqsut ( AK )",
            "locale" => "AK | Nuiqsut"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Nulato",
            "label" => "Nulato ( AK )",
            "locale" => "AK | Nulato"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Nunam Iqua",
            "label" => "Nunam Iqua ( AK )",
            "locale" => "AK | Nunam Iqua"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Nunapitchuk",
            "label" => "Nunapitchuk ( AK )",
            "locale" => "AK | Nunapitchuk"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Ohogamiut",
            "label" => "Ohogamiut ( AK )",
            "locale" => "AK | Ohogamiut"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Old Harbor",
            "label" => "Old Harbor ( AK )",
            "locale" => "AK | Old Harbor"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Oscarville",
            "label" => "Oscarville ( AK )",
            "locale" => "AK | Oscarville"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Ouzinkie",
            "label" => "Ouzinkie ( AK )",
            "locale" => "AK | Ouzinkie"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Paimiut",
            "label" => "Paimiut ( AK )",
            "locale" => "AK | Paimiut"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Pedro Bay",
            "label" => "Pedro Bay ( AK )",
            "locale" => "AK | Pedro Bay"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Perryville",
            "label" => "Perryville ( AK )",
            "locale" => "AK | Perryville"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Petersburg",
            "label" => "Petersburg ( AK )",
            "locale" => "AK | Petersburg"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Pilot Point",
            "label" => "Pilot Point ( AK )",
            "locale" => "AK | Pilot Point"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Pilot Station",
            "label" => "Pilot Station ( AK )",
            "locale" => "AK | Pilot Station"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Pitkas Point",
            "label" => "Pitkas Point ( AK )",
            "locale" => "AK | Pitkas Point"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Platinum",
            "label" => "Platinum ( AK )",
            "locale" => "AK | Platinum"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Point Hope",
            "label" => "Point Hope ( AK )",
            "locale" => "AK | Point Hope"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Point Lay",
            "label" => "Point Lay ( AK )",
            "locale" => "AK | Point Lay"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Port Alsworth",
            "label" => "Port Alsworth ( AK )",
            "locale" => "AK | Port Alsworth"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Port Graham",
            "label" => "Port Graham ( AK )",
            "locale" => "AK | Port Graham"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Port Heiden",
            "label" => "Port Heiden ( AK )",
            "locale" => "AK | Port Heiden"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Port Lions",
            "label" => "Port Lions ( AK )",
            "locale" => "AK | Port Lions"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Portage Creek",
            "label" => "Portage Creek ( AK )",
            "locale" => "AK | Portage Creek"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Rampart",
            "label" => "Rampart ( AK )",
            "locale" => "AK | Rampart"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Red Devil",
            "label" => "Red Devil ( AK )",
            "locale" => "AK | Red Devil"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Ruby",
            "label" => "Ruby ( AK )",
            "locale" => "AK | Ruby"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Russian Mission",
            "label" => "Russian Mission ( AK )",
            "locale" => "AK | Russian Mission"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Salamatof",
            "label" => "Salamatof ( AK )",
            "locale" => "AK | Salamatof"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Sand Point",
            "label" => "Sand Point ( AK )",
            "locale" => "AK | Sand Point"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Savoonga",
            "label" => "Savoonga ( AK )",
            "locale" => "AK | Savoonga"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Saxman",
            "label" => "Saxman ( AK )",
            "locale" => "AK | Saxman"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Scammon Bay",
            "label" => "Scammon Bay ( AK )",
            "locale" => "AK | Scammon Bay"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Selawik",
            "label" => "Selawik ( AK )",
            "locale" => "AK | Selawik"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Seldovia",
            "label" => "Seldovia ( AK )",
            "locale" => "AK | Seldovia"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Shageluk",
            "label" => "Shageluk ( AK )",
            "locale" => "AK | Shageluk"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Shaktoolik",
            "label" => "Shaktoolik ( AK )",
            "locale" => "AK | Shaktoolik"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Shishmaref",
            "label" => "Shishmaref ( AK )",
            "locale" => "AK | Shishmaref"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Shungnak",
            "label" => "Shungnak ( AK )",
            "locale" => "AK | Shungnak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Sitka",
            "label" => "Sitka ( AK )",
            "locale" => "AK | Sitka"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Skagway",
            "label" => "Skagway ( AK )",
            "locale" => "AK | Skagway"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Sleetmute",
            "label" => "Sleetmute ( AK )",
            "locale" => "AK | Sleetmute"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Solomon",
            "label" => "Solomon ( AK )",
            "locale" => "AK | Solomon"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "South Naknek",
            "label" => "South Naknek ( AK )",
            "locale" => "AK | South Naknek"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "St. George",
            "label" => "St. George ( AK )",
            "locale" => "AK | St. George"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "St. Michael",
            "label" => "St. Michael ( AK )",
            "locale" => "AK | St. Michael"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "St. Paul",
            "label" => "St. Paul ( AK )",
            "locale" => "AK | St. Paul"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Stebbins",
            "label" => "Stebbins ( AK )",
            "locale" => "AK | Stebbins"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Stevens Village",
            "label" => "Stevens Village ( AK )",
            "locale" => "AK | Stevens Village"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Stony River",
            "label" => "Stony River ( AK )",
            "locale" => "AK | Stony River"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Takotna",
            "label" => "Takotna ( AK )",
            "locale" => "AK | Takotna"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Tanacross",
            "label" => "Tanacross ( AK )",
            "locale" => "AK | Tanacross"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Tanana",
            "label" => "Tanana ( AK )",
            "locale" => "AK | Tanana"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Tatitlek",
            "label" => "Tatitlek ( AK )",
            "locale" => "AK | Tatitlek"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Tazlina",
            "label" => "Tazlina ( AK )",
            "locale" => "AK | Tazlina"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Telida",
            "label" => "Telida ( AK )",
            "locale" => "AK | Telida"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Teller",
            "label" => "Teller ( AK )",
            "locale" => "AK | Teller"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Tetlin",
            "label" => "Tetlin ( AK )",
            "locale" => "AK | Tetlin"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Togiak",
            "label" => "Togiak ( AK )",
            "locale" => "AK | Togiak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Toksook Bay",
            "label" => "Toksook Bay ( AK )",
            "locale" => "AK | Toksook Bay"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Tuluksak",
            "label" => "Tuluksak ( AK )",
            "locale" => "AK | Tuluksak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Tuntutuliak",
            "label" => "Tuntutuliak ( AK )",
            "locale" => "AK | Tuntutuliak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Tununak",
            "label" => "Tununak ( AK )",
            "locale" => "AK | Tununak"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Twin Hills",
            "label" => "Twin Hills ( AK )",
            "locale" => "AK | Twin Hills"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Tyonek",
            "label" => "Tyonek ( AK )",
            "locale" => "AK | Tyonek"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Ugashik",
            "label" => "Ugashik ( AK )",
            "locale" => "AK | Ugashik"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Unalakleet",
            "label" => "Unalakleet ( AK )",
            "locale" => "AK | Unalakleet"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Unalaska",
            "label" => "Unalaska ( AK )",
            "locale" => "AK | Unalaska"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Venetie",
            "label" => "Venetie ( AK )",
            "locale" => "AK | Venetie"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Wainwright",
            "label" => "Wainwright ( AK )",
            "locale" => "AK | Wainwright"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Wales",
            "label" => "Wales ( AK )",
            "locale" => "AK | Wales"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "White Mountain",
            "label" => "White Mountain ( AK )",
            "locale" => "AK | White Mountain"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Wrangell",
            "label" => "Wrangell ( AK )",
            "locale" => "AK | Wrangell"
        ]);
        County::create([
            "state_id" => 2,
            "location_id" => 0,
            "county" => "Yakutat",
            "label" => "Yakutat ( AK )",
            "locale" => "AK | Yakutat"
        ]);
        County::create([
            "state_id" => 3,
            "location_id" => 0,
            "county" => "Apache",
            "label" => "Apache ( AZ )",
            "locale" => "AZ | Apache"
        ]);
        County::create([
            "state_id" => 3,
            "location_id" => 0,
            "county" => "Cochise",
            "label" => "Cochise ( AZ )",
            "locale" => "AZ | Cochise"
        ]);
        County::create([
            "state_id" => 3,
            "location_id" => 0,
            "county" => "Coconino",
            "label" => "Coconino ( AZ )",
            "locale" => "AZ | Coconino"
        ]);
        County::create([
            "state_id" => 3,
            "location_id" => 0,
            "county" => "Gila",
            "label" => "Gila ( AZ )",
            "locale" => "AZ | Gila"
        ]);
        County::create([
            "state_id" => 3,
            "location_id" => 0,
            "county" => "Graham",
            "label" => "Graham ( AZ )",
            "locale" => "AZ | Graham"
        ]);
        County::create([
            "state_id" => 3,
            "location_id" => 0,
            "county" => "Greenlee",
            "label" => "Greenlee ( AZ )",
            "locale" => "AZ | Greenlee"
        ]);
        County::create([
            "state_id" => 3,
            "location_id" => 0,
            "county" => "La Paz",
            "label" => "La Paz ( AZ )",
            "locale" => "AZ | La Paz"
        ]);
        County::create([
            "state_id" => 3,
            "location_id" => 0,
            "county" => "Maricopa",
            "label" => "Maricopa ( AZ )",
            "locale" => "AZ | Maricopa"
        ]);
        County::create([
            "state_id" => 3,
            "location_id" => 0,
            "county" => "Mohave",
            "label" => "Mohave ( AZ )",
            "locale" => "AZ | Mohave"
        ]);
        County::create([
            "state_id" => 3,
            "location_id" => 0,
            "county" => "Navajo",
            "label" => "Navajo ( AZ )",
            "locale" => "AZ | Navajo"
        ]);
        County::create([
            "state_id" => 3,
            "location_id" => 0,
            "county" => "Pima",
            "label" => "Pima ( AZ )",
            "locale" => "AZ | Pima"
        ]);
        County::create([
            "state_id" => 3,
            "location_id" => 0,
            "county" => "Pinal",
            "label" => "Pinal ( AZ )",
            "locale" => "AZ | Pinal"
        ]);
        County::create([
            "state_id" => 3,
            "location_id" => 0,
            "county" => "Santa Cruz",
            "label" => "Santa Cruz ( AZ )",
            "locale" => "AZ | Santa Cruz"
        ]);
        County::create([
            "state_id" => 3,
            "location_id" => 0,
            "county" => "Yavapai",
            "label" => "Yavapai ( AZ )",
            "locale" => "AZ | Yavapai"
        ]);
        County::create([
            "state_id" => 3,
            "location_id" => 0,
            "county" => "Yuma",
            "label" => "Yuma ( AZ )",
            "locale" => "AZ | Yuma"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Arkansas",
            "label" => "Arkansas ( AR )",
            "locale" => "AR | Arkansas"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Ashley",
            "label" => "Ashley ( AR )",
            "locale" => "AR | Ashley"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Baxter",
            "label" => "Baxter ( AR )",
            "locale" => "AR | Baxter"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Benton",
            "label" => "Benton ( AR )",
            "locale" => "AR | Benton"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Boone",
            "label" => "Boone ( AR )",
            "locale" => "AR | Boone"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Bradley",
            "label" => "Bradley ( AR )",
            "locale" => "AR | Bradley"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Calhoun",
            "label" => "Calhoun ( AR )",
            "locale" => "AR | Calhoun"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Carroll",
            "label" => "Carroll ( AR )",
            "locale" => "AR | Carroll"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Chicot",
            "label" => "Chicot ( AR )",
            "locale" => "AR | Chicot"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Clark",
            "label" => "Clark ( AR )",
            "locale" => "AR | Clark"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Clay",
            "label" => "Clay ( AR )",
            "locale" => "AR | Clay"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Cleburne",
            "label" => "Cleburne ( AR )",
            "locale" => "AR | Cleburne"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Cleveland",
            "label" => "Cleveland ( AR )",
            "locale" => "AR | Cleveland"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Columbia",
            "label" => "Columbia ( AR )",
            "locale" => "AR | Columbia"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Conway",
            "label" => "Conway ( AR )",
            "locale" => "AR | Conway"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Craighead",
            "label" => "Craighead ( AR )",
            "locale" => "AR | Craighead"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Crawford",
            "label" => "Crawford ( AR )",
            "locale" => "AR | Crawford"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Crittenden",
            "label" => "Crittenden ( AR )",
            "locale" => "AR | Crittenden"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Cross",
            "label" => "Cross ( AR )",
            "locale" => "AR | Cross"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Dallas",
            "label" => "Dallas ( AR )",
            "locale" => "AR | Dallas"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Desha",
            "label" => "Desha ( AR )",
            "locale" => "AR | Desha"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Drew",
            "label" => "Drew ( AR )",
            "locale" => "AR | Drew"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Faulkner",
            "label" => "Faulkner ( AR )",
            "locale" => "AR | Faulkner"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Franklin",
            "label" => "Franklin ( AR )",
            "locale" => "AR | Franklin"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Fulton",
            "label" => "Fulton ( AR )",
            "locale" => "AR | Fulton"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Garland",
            "label" => "Garland ( AR )",
            "locale" => "AR | Garland"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Grant",
            "label" => "Grant ( AR )",
            "locale" => "AR | Grant"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Greene",
            "label" => "Greene ( AR )",
            "locale" => "AR | Greene"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Hempstead",
            "label" => "Hempstead ( AR )",
            "locale" => "AR | Hempstead"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Hot Spring",
            "label" => "Hot Spring ( AR )",
            "locale" => "AR | Hot Spring"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Howard",
            "label" => "Howard ( AR )",
            "locale" => "AR | Howard"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Independence",
            "label" => "Independence ( AR )",
            "locale" => "AR | Independence"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Izard",
            "label" => "Izard ( AR )",
            "locale" => "AR | Izard"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Jackson",
            "label" => "Jackson ( AR )",
            "locale" => "AR | Jackson"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Jefferson",
            "label" => "Jefferson ( AR )",
            "locale" => "AR | Jefferson"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Johnson",
            "label" => "Johnson ( AR )",
            "locale" => "AR | Johnson"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Lafayette",
            "label" => "Lafayette ( AR )",
            "locale" => "AR | Lafayette"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Lawrence",
            "label" => "Lawrence ( AR )",
            "locale" => "AR | Lawrence"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Lee",
            "label" => "Lee ( AR )",
            "locale" => "AR | Lee"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Lincoln",
            "label" => "Lincoln ( AR )",
            "locale" => "AR | Lincoln"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Little River",
            "label" => "Little River ( AR )",
            "locale" => "AR | Little River"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Logan",
            "label" => "Logan ( AR )",
            "locale" => "AR | Logan"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Lonoke",
            "label" => "Lonoke ( AR )",
            "locale" => "AR | Lonoke"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Madison",
            "label" => "Madison ( AR )",
            "locale" => "AR | Madison"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Marion",
            "label" => "Marion ( AR )",
            "locale" => "AR | Marion"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Miller",
            "label" => "Miller ( AR )",
            "locale" => "AR | Miller"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Mississippi",
            "label" => "Mississippi ( AR )",
            "locale" => "AR | Mississippi"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Monroe",
            "label" => "Monroe ( AR )",
            "locale" => "AR | Monroe"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Montgomery",
            "label" => "Montgomery ( AR )",
            "locale" => "AR | Montgomery"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Nevada",
            "label" => "Nevada ( AR )",
            "locale" => "AR | Nevada"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Newton",
            "label" => "Newton ( AR )",
            "locale" => "AR | Newton"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Ouachita",
            "label" => "Ouachita ( AR )",
            "locale" => "AR | Ouachita"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Perry",
            "label" => "Perry ( AR )",
            "locale" => "AR | Perry"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Phillips",
            "label" => "Phillips ( AR )",
            "locale" => "AR | Phillips"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Pike",
            "label" => "Pike ( AR )",
            "locale" => "AR | Pike"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Poinsett",
            "label" => "Poinsett ( AR )",
            "locale" => "AR | Poinsett"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Polk",
            "label" => "Polk ( AR )",
            "locale" => "AR | Polk"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Pope",
            "label" => "Pope ( AR )",
            "locale" => "AR | Pope"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Prairie",
            "label" => "Prairie ( AR )",
            "locale" => "AR | Prairie"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Pulaski",
            "label" => "Pulaski ( AR )",
            "locale" => "AR | Pulaski"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Randolph",
            "label" => "Randolph ( AR )",
            "locale" => "AR | Randolph"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Saline",
            "label" => "Saline ( AR )",
            "locale" => "AR | Saline"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Scott",
            "label" => "Scott ( AR )",
            "locale" => "AR | Scott"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Searcy",
            "label" => "Searcy ( AR )",
            "locale" => "AR | Searcy"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Sebastian",
            "label" => "Sebastian ( AR )",
            "locale" => "AR | Sebastian"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Sevier",
            "label" => "Sevier ( AR )",
            "locale" => "AR | Sevier"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Sharp",
            "label" => "Sharp ( AR )",
            "locale" => "AR | Sharp"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "St. Francis",
            "label" => "St. Francis ( AR )",
            "locale" => "AR | St. Francis"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Stone",
            "label" => "Stone ( AR )",
            "locale" => "AR | Stone"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Union",
            "label" => "Union ( AR )",
            "locale" => "AR | Union"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Van Buren",
            "label" => "Van Buren ( AR )",
            "locale" => "AR | Van Buren"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Washington",
            "label" => "Washington ( AR )",
            "locale" => "AR | Washington"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "White",
            "label" => "White ( AR )",
            "locale" => "AR | White"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Woodruff",
            "label" => "Woodruff ( AR )",
            "locale" => "AR | Woodruff"
        ]);
        County::create([
            "state_id" => 4,
            "location_id" => 4,
            "county" => "Yell",
            "label" => "Yell ( AR )",
            "locale" => "AR | Yell"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Alameda",
            "label" => "Alameda ( CA )",
            "locale" => "CA | Alameda"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Alpine",
            "label" => "Alpine ( CA )",
            "locale" => "CA | Alpine"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Amador",
            "label" => "Amador ( CA )",
            "locale" => "CA | Amador"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Butte",
            "label" => "Butte ( CA )",
            "locale" => "CA | Butte"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Calaveras",
            "label" => "Calaveras ( CA )",
            "locale" => "CA | Calaveras"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Colusa",
            "label" => "Colusa ( CA )",
            "locale" => "CA | Colusa"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Contra Costa",
            "label" => "Contra Costa ( CA )",
            "locale" => "CA | Contra Costa"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Del Norte",
            "label" => "Del Norte ( CA )",
            "locale" => "CA | Del Norte"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "El Dorado",
            "label" => "El Dorado ( CA )",
            "locale" => "CA | El Dorado"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Fresno",
            "label" => "Fresno ( CA )",
            "locale" => "CA | Fresno"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Glenn",
            "label" => "Glenn ( CA )",
            "locale" => "CA | Glenn"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Humboldt",
            "label" => "Humboldt ( CA )",
            "locale" => "CA | Humboldt"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Imperial",
            "label" => "Imperial ( CA )",
            "locale" => "CA | Imperial"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Inyo",
            "label" => "Inyo ( CA )",
            "locale" => "CA | Inyo"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Kern",
            "label" => "Kern ( CA )",
            "locale" => "CA | Kern"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Kings",
            "label" => "Kings ( CA )",
            "locale" => "CA | Kings"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Lake",
            "label" => "Lake ( CA )",
            "locale" => "CA | Lake"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Lassen",
            "label" => "Lassen ( CA )",
            "locale" => "CA | Lassen"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Los Angeles",
            "label" => "Los Angeles ( CA )",
            "locale" => "CA | Los Angeles"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Madera",
            "label" => "Madera ( CA )",
            "locale" => "CA | Madera"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Marin",
            "label" => "Marin ( CA )",
            "locale" => "CA | Marin"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Mariposa",
            "label" => "Mariposa ( CA )",
            "locale" => "CA | Mariposa"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Mendocino",
            "label" => "Mendocino ( CA )",
            "locale" => "CA | Mendocino"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Merced",
            "label" => "Merced ( CA )",
            "locale" => "CA | Merced"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Modoc",
            "label" => "Modoc ( CA )",
            "locale" => "CA | Modoc"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Mono",
            "label" => "Mono ( CA )",
            "locale" => "CA | Mono"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Monterey",
            "label" => "Monterey ( CA )",
            "locale" => "CA | Monterey"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Napa",
            "label" => "Napa ( CA )",
            "locale" => "CA | Napa"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Nevada",
            "label" => "Nevada ( CA )",
            "locale" => "CA | Nevada"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Orange",
            "label" => "Orange ( CA )",
            "locale" => "CA | Orange"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Placer",
            "label" => "Placer ( CA )",
            "locale" => "CA | Placer"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Plumas",
            "label" => "Plumas ( CA )",
            "locale" => "CA | Plumas"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Riverside",
            "label" => "Riverside ( CA )",
            "locale" => "CA | Riverside"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Sacramento",
            "label" => "Sacramento ( CA )",
            "locale" => "CA | Sacramento"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "San Benito",
            "label" => "San Benito ( CA )",
            "locale" => "CA | San Benito"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "San Bernardino",
            "label" => "San Bernardino ( CA )",
            "locale" => "CA | San Bernardino"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "San Diego",
            "label" => "San Diego ( CA )",
            "locale" => "CA | San Diego"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "San Joaquin",
            "label" => "San Joaquin ( CA )",
            "locale" => "CA | San Joaquin"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "San Luis Obispo",
            "label" => "San Luis Obispo ( CA )",
            "locale" => "CA | San Luis Obispo"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "San Mateo",
            "label" => "San Mateo ( CA )",
            "locale" => "CA | San Mateo"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Santa Barbara",
            "label" => "Santa Barbara ( CA )",
            "locale" => "CA | Santa Barbara"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Santa Clara",
            "label" => "Santa Clara ( CA )",
            "locale" => "CA | Santa Clara"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Santa Cruz",
            "label" => "Santa Cruz ( CA )",
            "locale" => "CA | Santa Cruz"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Shasta",
            "label" => "Shasta ( CA )",
            "locale" => "CA | Shasta"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Sierra",
            "label" => "Sierra ( CA )",
            "locale" => "CA | Sierra"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Siskiyou",
            "label" => "Siskiyou ( CA )",
            "locale" => "CA | Siskiyou"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Solano",
            "label" => "Solano ( CA )",
            "locale" => "CA | Solano"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Sonoma",
            "label" => "Sonoma ( CA )",
            "locale" => "CA | Sonoma"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Stanislaus",
            "label" => "Stanislaus ( CA )",
            "locale" => "CA | Stanislaus"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Sutter",
            "label" => "Sutter ( CA )",
            "locale" => "CA | Sutter"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Tehama",
            "label" => "Tehama ( CA )",
            "locale" => "CA | Tehama"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Trinity",
            "label" => "Trinity ( CA )",
            "locale" => "CA | Trinity"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Tulare",
            "label" => "Tulare ( CA )",
            "locale" => "CA | Tulare"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Tuolumne",
            "label" => "Tuolumne ( CA )",
            "locale" => "CA | Tuolumne"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Ventura",
            "label" => "Ventura ( CA )",
            "locale" => "CA | Ventura"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Yolo",
            "label" => "Yolo ( CA )",
            "locale" => "CA | Yolo"
        ]);
        County::create([
            "state_id" => 5,
            "location_id" => 0,
            "county" => "Yuba",
            "label" => "Yuba ( CA )",
            "locale" => "CA | Yuba"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Adams",
            "label" => "Adams ( CO )",
            "locale" => "CO | Adams"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Alamosa",
            "label" => "Alamosa ( CO )",
            "locale" => "CO | Alamosa"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Arapahoe",
            "label" => "Arapahoe ( CO )",
            "locale" => "CO | Arapahoe"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Archuleta",
            "label" => "Archuleta ( CO )",
            "locale" => "CO | Archuleta"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Baca",
            "label" => "Baca ( CO )",
            "locale" => "CO | Baca"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Bent",
            "label" => "Bent ( CO )",
            "locale" => "CO | Bent"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Boulder",
            "label" => "Boulder ( CO )",
            "locale" => "CO | Boulder"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Chaffee",
            "label" => "Chaffee ( CO )",
            "locale" => "CO | Chaffee"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Cheyenne",
            "label" => "Cheyenne ( CO )",
            "locale" => "CO | Cheyenne"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Clear Creek",
            "label" => "Clear Creek ( CO )",
            "locale" => "CO | Clear Creek"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Conejos",
            "label" => "Conejos ( CO )",
            "locale" => "CO | Conejos"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Costilla",
            "label" => "Costilla ( CO )",
            "locale" => "CO | Costilla"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Crowley",
            "label" => "Crowley ( CO )",
            "locale" => "CO | Crowley"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Custer",
            "label" => "Custer ( CO )",
            "locale" => "CO | Custer"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Delta",
            "label" => "Delta ( CO )",
            "locale" => "CO | Delta"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Dolores",
            "label" => "Dolores ( CO )",
            "locale" => "CO | Dolores"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Douglas",
            "label" => "Douglas ( CO )",
            "locale" => "CO | Douglas"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Eagle",
            "label" => "Eagle ( CO )",
            "locale" => "CO | Eagle"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "El Paso",
            "label" => "El Paso ( CO )",
            "locale" => "CO | El Paso"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Elbert",
            "label" => "Elbert ( CO )",
            "locale" => "CO | Elbert"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Fremont",
            "label" => "Fremont ( CO )",
            "locale" => "CO | Fremont"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Garfield",
            "label" => "Garfield ( CO )",
            "locale" => "CO | Garfield"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Gilpin",
            "label" => "Gilpin ( CO )",
            "locale" => "CO | Gilpin"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Grand",
            "label" => "Grand ( CO )",
            "locale" => "CO | Grand"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Gunnison",
            "label" => "Gunnison ( CO )",
            "locale" => "CO | Gunnison"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Hinsdale",
            "label" => "Hinsdale ( CO )",
            "locale" => "CO | Hinsdale"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Huerfano",
            "label" => "Huerfano ( CO )",
            "locale" => "CO | Huerfano"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Jackson",
            "label" => "Jackson ( CO )",
            "locale" => "CO | Jackson"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Jefferson",
            "label" => "Jefferson ( CO )",
            "locale" => "CO | Jefferson"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Kiowa",
            "label" => "Kiowa ( CO )",
            "locale" => "CO | Kiowa"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Kit Carson",
            "label" => "Kit Carson ( CO )",
            "locale" => "CO | Kit Carson"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "La Plata",
            "label" => "La Plata ( CO )",
            "locale" => "CO | La Plata"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Lake",
            "label" => "Lake ( CO )",
            "locale" => "CO | Lake"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Larimer",
            "label" => "Larimer ( CO )",
            "locale" => "CO | Larimer"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Las Animas",
            "label" => "Las Animas ( CO )",
            "locale" => "CO | Las Animas"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Lincoln",
            "label" => "Lincoln ( CO )",
            "locale" => "CO | Lincoln"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Logan",
            "label" => "Logan ( CO )",
            "locale" => "CO | Logan"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Mesa",
            "label" => "Mesa ( CO )",
            "locale" => "CO | Mesa"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Mineral",
            "label" => "Mineral ( CO )",
            "locale" => "CO | Mineral"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Moffat",
            "label" => "Moffat ( CO )",
            "locale" => "CO | Moffat"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Montezuma",
            "label" => "Montezuma ( CO )",
            "locale" => "CO | Montezuma"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Montrose",
            "label" => "Montrose ( CO )",
            "locale" => "CO | Montrose"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Morgan",
            "label" => "Morgan ( CO )",
            "locale" => "CO | Morgan"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Otero",
            "label" => "Otero ( CO )",
            "locale" => "CO | Otero"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Ouray",
            "label" => "Ouray ( CO )",
            "locale" => "CO | Ouray"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Park",
            "label" => "Park ( CO )",
            "locale" => "CO | Park"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Phillips",
            "label" => "Phillips ( CO )",
            "locale" => "CO | Phillips"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Pitkin",
            "label" => "Pitkin ( CO )",
            "locale" => "CO | Pitkin"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Prowers",
            "label" => "Prowers ( CO )",
            "locale" => "CO | Prowers"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Pueblo",
            "label" => "Pueblo ( CO )",
            "locale" => "CO | Pueblo"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Rio Blanco",
            "label" => "Rio Blanco ( CO )",
            "locale" => "CO | Rio Blanco"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Rio Grande",
            "label" => "Rio Grande ( CO )",
            "locale" => "CO | Rio Grande"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Routt",
            "label" => "Routt ( CO )",
            "locale" => "CO | Routt"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Saguache",
            "label" => "Saguache ( CO )",
            "locale" => "CO | Saguache"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "San Juan",
            "label" => "San Juan ( CO )",
            "locale" => "CO | San Juan"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "San Miguel",
            "label" => "San Miguel ( CO )",
            "locale" => "CO | San Miguel"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Sedgwick",
            "label" => "Sedgwick ( CO )",
            "locale" => "CO | Sedgwick"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Summit",
            "label" => "Summit ( CO )",
            "locale" => "CO | Summit"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Teller",
            "label" => "Teller ( CO )",
            "locale" => "CO | Teller"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Washington",
            "label" => "Washington ( CO )",
            "locale" => "CO | Washington"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Weld",
            "label" => "Weld ( CO )",
            "locale" => "CO | Weld"
        ]);
        County::create([
            "state_id" => 6,
            "location_id" => 0,
            "county" => "Yuma",
            "label" => "Yuma ( CO )",
            "locale" => "CO | Yuma"
        ]);
        County::create([
            "state_id" => 8,
            "location_id" => 0,
            "county" => "Kent",
            "label" => "Kent ( DE )",
            "locale" => "DE | Kent"
        ]);
        County::create([
            "state_id" => 8,
            "location_id" => 0,
            "county" => "New Castle",
            "label" => "New Castle ( DE )",
            "locale" => "DE | New Castle"
        ]);
        County::create([
            "state_id" => 8,
            "location_id" => 0,
            "county" => "Sussex",
            "label" => "Sussex ( DE )",
            "locale" => "DE | Sussex"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Alachua",
            "label" => "Alachua ( FL )",
            "locale" => "FL | Alachua"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Baker",
            "label" => "Baker ( FL )",
            "locale" => "FL | Baker"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Bay",
            "label" => "Bay ( FL )",
            "locale" => "FL | Bay"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Bradford",
            "label" => "Bradford ( FL )",
            "locale" => "FL | Bradford"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Brevard",
            "label" => "Brevard ( FL )",
            "locale" => "FL | Brevard"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Broward",
            "label" => "Broward ( FL )",
            "locale" => "FL | Broward"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Calhoun",
            "label" => "Calhoun ( FL )",
            "locale" => "FL | Calhoun"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Charlotte",
            "label" => "Charlotte ( FL )",
            "locale" => "FL | Charlotte"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Citrus",
            "label" => "Citrus ( FL )",
            "locale" => "FL | Citrus"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Clay",
            "label" => "Clay ( FL )",
            "locale" => "FL | Clay"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Collier",
            "label" => "Collier ( FL )",
            "locale" => "FL | Collier"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Columbia",
            "label" => "Columbia ( FL )",
            "locale" => "FL | Columbia"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "DeSoto",
            "label" => "DeSoto ( FL )",
            "locale" => "FL | DeSoto"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Dixie",
            "label" => "Dixie ( FL )",
            "locale" => "FL | Dixie"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Escambia",
            "label" => "Escambia ( FL )",
            "locale" => "FL | Escambia"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Flagler",
            "label" => "Flagler ( FL )",
            "locale" => "FL | Flagler"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Franklin",
            "label" => "Franklin ( FL )",
            "locale" => "FL | Franklin"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Gadsden",
            "label" => "Gadsden ( FL )",
            "locale" => "FL | Gadsden"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Gilchrist",
            "label" => "Gilchrist ( FL )",
            "locale" => "FL | Gilchrist"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Glades",
            "label" => "Glades ( FL )",
            "locale" => "FL | Glades"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Gulf",
            "label" => "Gulf ( FL )",
            "locale" => "FL | Gulf"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Hamilton",
            "label" => "Hamilton ( FL )",
            "locale" => "FL | Hamilton"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Hardee",
            "label" => "Hardee ( FL )",
            "locale" => "FL | Hardee"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Hendry",
            "label" => "Hendry ( FL )",
            "locale" => "FL | Hendry"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Hernando",
            "label" => "Hernando ( FL )",
            "locale" => "FL | Hernando"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Highlands",
            "label" => "Highlands ( FL )",
            "locale" => "FL | Highlands"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Hillsborough",
            "label" => "Hillsborough ( FL )",
            "locale" => "FL | Hillsborough"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Holmes",
            "label" => "Holmes ( FL )",
            "locale" => "FL | Holmes"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Indian River",
            "label" => "Indian River ( FL )",
            "locale" => "FL | Indian River"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Jackson",
            "label" => "Jackson ( FL )",
            "locale" => "FL | Jackson"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Jefferson",
            "label" => "Jefferson ( FL )",
            "locale" => "FL | Jefferson"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Lafayette",
            "label" => "Lafayette ( FL )",
            "locale" => "FL | Lafayette"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Lake",
            "label" => "Lake ( FL )",
            "locale" => "FL | Lake"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Lee",
            "label" => "Lee ( FL )",
            "locale" => "FL | Lee"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Leon",
            "label" => "Leon ( FL )",
            "locale" => "FL | Leon"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Levy",
            "label" => "Levy ( FL )",
            "locale" => "FL | Levy"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Liberty",
            "label" => "Liberty ( FL )",
            "locale" => "FL | Liberty"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Madison",
            "label" => "Madison ( FL )",
            "locale" => "FL | Madison"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Manatee",
            "label" => "Manatee ( FL )",
            "locale" => "FL | Manatee"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Marion",
            "label" => "Marion ( FL )",
            "locale" => "FL | Marion"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Martin",
            "label" => "Martin ( FL )",
            "locale" => "FL | Martin"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Miami-Dade",
            "label" => "Miami-Dade ( FL )",
            "locale" => "FL | Miami-Dade"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Monroe",
            "label" => "Monroe ( FL )",
            "locale" => "FL | Monroe"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Nassau",
            "label" => "Nassau ( FL )",
            "locale" => "FL | Nassau"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Okaloosa",
            "label" => "Okaloosa ( FL )",
            "locale" => "FL | Okaloosa"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Okeechobee",
            "label" => "Okeechobee ( FL )",
            "locale" => "FL | Okeechobee"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Orange",
            "label" => "Orange ( FL )",
            "locale" => "FL | Orange"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Osceola",
            "label" => "Osceola ( FL )",
            "locale" => "FL | Osceola"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Palm Beach",
            "label" => "Palm Beach ( FL )",
            "locale" => "FL | Palm Beach"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Pasco",
            "label" => "Pasco ( FL )",
            "locale" => "FL | Pasco"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Pinellas",
            "label" => "Pinellas ( FL )",
            "locale" => "FL | Pinellas"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Polk",
            "label" => "Polk ( FL )",
            "locale" => "FL | Polk"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Putnam",
            "label" => "Putnam ( FL )",
            "locale" => "FL | Putnam"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Santa Rosa",
            "label" => "Santa Rosa ( FL )",
            "locale" => "FL | Santa Rosa"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Sarasota",
            "label" => "Sarasota ( FL )",
            "locale" => "FL | Sarasota"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Seminole",
            "label" => "Seminole ( FL )",
            "locale" => "FL | Seminole"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "St. Johns",
            "label" => "St. Johns ( FL )",
            "locale" => "FL | St. Johns"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "St. Lucie",
            "label" => "St. Lucie ( FL )",
            "locale" => "FL | St. Lucie"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Sumter",
            "label" => "Sumter ( FL )",
            "locale" => "FL | Sumter"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Suwannee",
            "label" => "Suwannee ( FL )",
            "locale" => "FL | Suwannee"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Taylor",
            "label" => "Taylor ( FL )",
            "locale" => "FL | Taylor"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Union",
            "label" => "Union ( FL )",
            "locale" => "FL | Union"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Volusia",
            "label" => "Volusia ( FL )",
            "locale" => "FL | Volusia"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Wakulla",
            "label" => "Wakulla ( FL )",
            "locale" => "FL | Wakulla"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Walton",
            "label" => "Walton ( FL )",
            "locale" => "FL | Walton"
        ]);
        County::create([
            "state_id" => 10,
            "location_id" => 0,
            "county" => "Washington",
            "label" => "Washington ( FL )",
            "locale" => "FL | Washington"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Appling",
            "label" => "Appling ( GA )",
            "locale" => "GA | Appling"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Atkinson",
            "label" => "Atkinson ( GA )",
            "locale" => "GA | Atkinson"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Bacon",
            "label" => "Bacon ( GA )",
            "locale" => "GA | Bacon"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Baker",
            "label" => "Baker ( GA )",
            "locale" => "GA | Baker"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Baldwin",
            "label" => "Baldwin ( GA )",
            "locale" => "GA | Baldwin"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Banks",
            "label" => "Banks ( GA )",
            "locale" => "GA | Banks"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Barrow",
            "label" => "Barrow ( GA )",
            "locale" => "GA | Barrow"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Bartow",
            "label" => "Bartow ( GA )",
            "locale" => "GA | Bartow"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Ben Hill",
            "label" => "Ben Hill ( GA )",
            "locale" => "GA | Ben Hill"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Berrien",
            "label" => "Berrien ( GA )",
            "locale" => "GA | Berrien"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Bibb",
            "label" => "Bibb ( GA )",
            "locale" => "GA | Bibb"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Bleckley",
            "label" => "Bleckley ( GA )",
            "locale" => "GA | Bleckley"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Brantley",
            "label" => "Brantley ( GA )",
            "locale" => "GA | Brantley"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Brooks",
            "label" => "Brooks ( GA )",
            "locale" => "GA | Brooks"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Bryan",
            "label" => "Bryan ( GA )",
            "locale" => "GA | Bryan"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Bulloch",
            "label" => "Bulloch ( GA )",
            "locale" => "GA | Bulloch"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Burke",
            "label" => "Burke ( GA )",
            "locale" => "GA | Burke"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Butts",
            "label" => "Butts ( GA )",
            "locale" => "GA | Butts"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Calhoun",
            "label" => "Calhoun ( GA )",
            "locale" => "GA | Calhoun"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Camden",
            "label" => "Camden ( GA )",
            "locale" => "GA | Camden"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Candler",
            "label" => "Candler ( GA )",
            "locale" => "GA | Candler"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Carroll",
            "label" => "Carroll ( GA )",
            "locale" => "GA | Carroll"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Catoosa",
            "label" => "Catoosa ( GA )",
            "locale" => "GA | Catoosa"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Charlton",
            "label" => "Charlton ( GA )",
            "locale" => "GA | Charlton"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Chatham",
            "label" => "Chatham ( GA )",
            "locale" => "GA | Chatham"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Chattooga",
            "label" => "Chattooga ( GA )",
            "locale" => "GA | Chattooga"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Cherokee",
            "label" => "Cherokee ( GA )",
            "locale" => "GA | Cherokee"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Clay",
            "label" => "Clay ( GA )",
            "locale" => "GA | Clay"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Clayton",
            "label" => "Clayton ( GA )",
            "locale" => "GA | Clayton"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Clinch",
            "label" => "Clinch ( GA )",
            "locale" => "GA | Clinch"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Cobb",
            "label" => "Cobb ( GA )",
            "locale" => "GA | Cobb"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Coffee",
            "label" => "Coffee ( GA )",
            "locale" => "GA | Coffee"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Colquitt",
            "label" => "Colquitt ( GA )",
            "locale" => "GA | Colquitt"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Columbia",
            "label" => "Columbia ( GA )",
            "locale" => "GA | Columbia"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Cook",
            "label" => "Cook ( GA )",
            "locale" => "GA | Cook"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Coweta",
            "label" => "Coweta ( GA )",
            "locale" => "GA | Coweta"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Crawford",
            "label" => "Crawford ( GA )",
            "locale" => "GA | Crawford"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Crisp",
            "label" => "Crisp ( GA )",
            "locale" => "GA | Crisp"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Dade",
            "label" => "Dade ( GA )",
            "locale" => "GA | Dade"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Dawson",
            "label" => "Dawson ( GA )",
            "locale" => "GA | Dawson"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Decatur",
            "label" => "Decatur ( GA )",
            "locale" => "GA | Decatur"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "DeKalb",
            "label" => "DeKalb ( GA )",
            "locale" => "GA | DeKalb"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Dodge",
            "label" => "Dodge ( GA )",
            "locale" => "GA | Dodge"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Dooly",
            "label" => "Dooly ( GA )",
            "locale" => "GA | Dooly"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Dougherty",
            "label" => "Dougherty ( GA )",
            "locale" => "GA | Dougherty"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Douglas",
            "label" => "Douglas ( GA )",
            "locale" => "GA | Douglas"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Early",
            "label" => "Early ( GA )",
            "locale" => "GA | Early"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Echols",
            "label" => "Echols ( GA )",
            "locale" => "GA | Echols"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Effingham",
            "label" => "Effingham ( GA )",
            "locale" => "GA | Effingham"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Elbert",
            "label" => "Elbert ( GA )",
            "locale" => "GA | Elbert"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Emanuel",
            "label" => "Emanuel ( GA )",
            "locale" => "GA | Emanuel"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Evans",
            "label" => "Evans ( GA )",
            "locale" => "GA | Evans"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Fannin",
            "label" => "Fannin ( GA )",
            "locale" => "GA | Fannin"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Fayette",
            "label" => "Fayette ( GA )",
            "locale" => "GA | Fayette"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Floyd",
            "label" => "Floyd ( GA )",
            "locale" => "GA | Floyd"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Forsyth",
            "label" => "Forsyth ( GA )",
            "locale" => "GA | Forsyth"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Franklin",
            "label" => "Franklin ( GA )",
            "locale" => "GA | Franklin"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Fulton",
            "label" => "Fulton ( GA )",
            "locale" => "GA | Fulton"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Gilmer",
            "label" => "Gilmer ( GA )",
            "locale" => "GA | Gilmer"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Glascock",
            "label" => "Glascock ( GA )",
            "locale" => "GA | Glascock"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Glynn",
            "label" => "Glynn ( GA )",
            "locale" => "GA | Glynn"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Gordon",
            "label" => "Gordon ( GA )",
            "locale" => "GA | Gordon"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Grady",
            "label" => "Grady ( GA )",
            "locale" => "GA | Grady"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Greene",
            "label" => "Greene ( GA )",
            "locale" => "GA | Greene"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Gwinnett",
            "label" => "Gwinnett ( GA )",
            "locale" => "GA | Gwinnett"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Habersham",
            "label" => "Habersham ( GA )",
            "locale" => "GA | Habersham"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Hall",
            "label" => "Hall ( GA )",
            "locale" => "GA | Hall"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Hancock",
            "label" => "Hancock ( GA )",
            "locale" => "GA | Hancock"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Haralson",
            "label" => "Haralson ( GA )",
            "locale" => "GA | Haralson"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Harris",
            "label" => "Harris ( GA )",
            "locale" => "GA | Harris"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Hart",
            "label" => "Hart ( GA )",
            "locale" => "GA | Hart"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Heard",
            "label" => "Heard ( GA )",
            "locale" => "GA | Heard"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Henry",
            "label" => "Henry ( GA )",
            "locale" => "GA | Henry"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Houston",
            "label" => "Houston ( GA )",
            "locale" => "GA | Houston"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Irwin",
            "label" => "Irwin ( GA )",
            "locale" => "GA | Irwin"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Jackson",
            "label" => "Jackson ( GA )",
            "locale" => "GA | Jackson"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Jasper",
            "label" => "Jasper ( GA )",
            "locale" => "GA | Jasper"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Jeff Davis",
            "label" => "Jeff Davis ( GA )",
            "locale" => "GA | Jeff Davis"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Jefferson",
            "label" => "Jefferson ( GA )",
            "locale" => "GA | Jefferson"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Jenkins",
            "label" => "Jenkins ( GA )",
            "locale" => "GA | Jenkins"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Johnson",
            "label" => "Johnson ( GA )",
            "locale" => "GA | Johnson"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Jones",
            "label" => "Jones ( GA )",
            "locale" => "GA | Jones"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Lamar",
            "label" => "Lamar ( GA )",
            "locale" => "GA | Lamar"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Lanier",
            "label" => "Lanier ( GA )",
            "locale" => "GA | Lanier"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Laurens",
            "label" => "Laurens ( GA )",
            "locale" => "GA | Laurens"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Lee",
            "label" => "Lee ( GA )",
            "locale" => "GA | Lee"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Liberty",
            "label" => "Liberty ( GA )",
            "locale" => "GA | Liberty"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Lincoln",
            "label" => "Lincoln ( GA )",
            "locale" => "GA | Lincoln"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Long",
            "label" => "Long ( GA )",
            "locale" => "GA | Long"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Lowndes",
            "label" => "Lowndes ( GA )",
            "locale" => "GA | Lowndes"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Lumpkin",
            "label" => "Lumpkin ( GA )",
            "locale" => "GA | Lumpkin"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Macon",
            "label" => "Macon ( GA )",
            "locale" => "GA | Macon"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Madison",
            "label" => "Madison ( GA )",
            "locale" => "GA | Madison"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Marion",
            "label" => "Marion ( GA )",
            "locale" => "GA | Marion"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "McDuffie",
            "label" => "McDuffie ( GA )",
            "locale" => "GA | McDuffie"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "McIntosh",
            "label" => "McIntosh ( GA )",
            "locale" => "GA | McIntosh"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Meriwether",
            "label" => "Meriwether ( GA )",
            "locale" => "GA | Meriwether"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Miller",
            "label" => "Miller ( GA )",
            "locale" => "GA | Miller"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Mitchell",
            "label" => "Mitchell ( GA )",
            "locale" => "GA | Mitchell"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Monroe",
            "label" => "Monroe ( GA )",
            "locale" => "GA | Monroe"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Montgomery",
            "label" => "Montgomery ( GA )",
            "locale" => "GA | Montgomery"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Morgan",
            "label" => "Morgan ( GA )",
            "locale" => "GA | Morgan"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Murray",
            "label" => "Murray ( GA )",
            "locale" => "GA | Murray"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Newton",
            "label" => "Newton ( GA )",
            "locale" => "GA | Newton"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Oconee",
            "label" => "Oconee ( GA )",
            "locale" => "GA | Oconee"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Oglethorpe",
            "label" => "Oglethorpe ( GA )",
            "locale" => "GA | Oglethorpe"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Paulding",
            "label" => "Paulding ( GA )",
            "locale" => "GA | Paulding"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Peach",
            "label" => "Peach ( GA )",
            "locale" => "GA | Peach"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Pickens",
            "label" => "Pickens ( GA )",
            "locale" => "GA | Pickens"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Pierce",
            "label" => "Pierce ( GA )",
            "locale" => "GA | Pierce"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Pike",
            "label" => "Pike ( GA )",
            "locale" => "GA | Pike"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Polk",
            "label" => "Polk ( GA )",
            "locale" => "GA | Polk"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Pulaski",
            "label" => "Pulaski ( GA )",
            "locale" => "GA | Pulaski"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Putnam",
            "label" => "Putnam ( GA )",
            "locale" => "GA | Putnam"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Rabun",
            "label" => "Rabun ( GA )",
            "locale" => "GA | Rabun"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Randolph",
            "label" => "Randolph ( GA )",
            "locale" => "GA | Randolph"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Rockdale",
            "label" => "Rockdale ( GA )",
            "locale" => "GA | Rockdale"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Schley",
            "label" => "Schley ( GA )",
            "locale" => "GA | Schley"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Screven",
            "label" => "Screven ( GA )",
            "locale" => "GA | Screven"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Seminole",
            "label" => "Seminole ( GA )",
            "locale" => "GA | Seminole"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Spalding",
            "label" => "Spalding ( GA )",
            "locale" => "GA | Spalding"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Stephens",
            "label" => "Stephens ( GA )",
            "locale" => "GA | Stephens"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Stewart",
            "label" => "Stewart ( GA )",
            "locale" => "GA | Stewart"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Sumter",
            "label" => "Sumter ( GA )",
            "locale" => "GA | Sumter"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Talbot",
            "label" => "Talbot ( GA )",
            "locale" => "GA | Talbot"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Taliaferro",
            "label" => "Taliaferro ( GA )",
            "locale" => "GA | Taliaferro"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Tattnall",
            "label" => "Tattnall ( GA )",
            "locale" => "GA | Tattnall"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Taylor",
            "label" => "Taylor ( GA )",
            "locale" => "GA | Taylor"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Telfair",
            "label" => "Telfair ( GA )",
            "locale" => "GA | Telfair"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Terrell",
            "label" => "Terrell ( GA )",
            "locale" => "GA | Terrell"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Thomas",
            "label" => "Thomas ( GA )",
            "locale" => "GA | Thomas"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Tift",
            "label" => "Tift ( GA )",
            "locale" => "GA | Tift"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Toombs",
            "label" => "Toombs ( GA )",
            "locale" => "GA | Toombs"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Towns",
            "label" => "Towns ( GA )",
            "locale" => "GA | Towns"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Treutlen",
            "label" => "Treutlen ( GA )",
            "locale" => "GA | Treutlen"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Troup",
            "label" => "Troup ( GA )",
            "locale" => "GA | Troup"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Turner",
            "label" => "Turner ( GA )",
            "locale" => "GA | Turner"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Twiggs",
            "label" => "Twiggs ( GA )",
            "locale" => "GA | Twiggs"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Union",
            "label" => "Union ( GA )",
            "locale" => "GA | Union"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Upson",
            "label" => "Upson ( GA )",
            "locale" => "GA | Upson"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Walker",
            "label" => "Walker ( GA )",
            "locale" => "GA | Walker"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Walton",
            "label" => "Walton ( GA )",
            "locale" => "GA | Walton"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Ware",
            "label" => "Ware ( GA )",
            "locale" => "GA | Ware"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Warren",
            "label" => "Warren ( GA )",
            "locale" => "GA | Warren"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Washington",
            "label" => "Washington ( GA )",
            "locale" => "GA | Washington"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Wayne",
            "label" => "Wayne ( GA )",
            "locale" => "GA | Wayne"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Wheeler",
            "label" => "Wheeler ( GA )",
            "locale" => "GA | Wheeler"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "White",
            "label" => "White ( GA )",
            "locale" => "GA | White"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Whitfield",
            "label" => "Whitfield ( GA )",
            "locale" => "GA | Whitfield"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Wilcox",
            "label" => "Wilcox ( GA )",
            "locale" => "GA | Wilcox"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Wilkes",
            "label" => "Wilkes ( GA )",
            "locale" => "GA | Wilkes"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Wilkinson",
            "label" => "Wilkinson ( GA )",
            "locale" => "GA | Wilkinson"
        ]);
        County::create([
            "state_id" => 11,
            "location_id" => 0,
            "county" => "Worth",
            "label" => "Worth ( GA )",
            "locale" => "GA | Worth"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Ada",
            "label" => "Ada ( ID )",
            "locale" => "ID | Ada"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Adams",
            "label" => "Adams ( ID )",
            "locale" => "ID | Adams"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Bannock",
            "label" => "Bannock ( ID )",
            "locale" => "ID | Bannock"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Bear Lake",
            "label" => "Bear Lake ( ID )",
            "locale" => "ID | Bear Lake"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Benewah",
            "label" => "Benewah ( ID )",
            "locale" => "ID | Benewah"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Bingham",
            "label" => "Bingham ( ID )",
            "locale" => "ID | Bingham"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Blaine",
            "label" => "Blaine ( ID )",
            "locale" => "ID | Blaine"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Boise",
            "label" => "Boise ( ID )",
            "locale" => "ID | Boise"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Bonner",
            "label" => "Bonner ( ID )",
            "locale" => "ID | Bonner"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Bonneville",
            "label" => "Bonneville ( ID )",
            "locale" => "ID | Bonneville"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Boundary",
            "label" => "Boundary ( ID )",
            "locale" => "ID | Boundary"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Butte",
            "label" => "Butte ( ID )",
            "locale" => "ID | Butte"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Camas",
            "label" => "Camas ( ID )",
            "locale" => "ID | Camas"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Canyon",
            "label" => "Canyon ( ID )",
            "locale" => "ID | Canyon"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Caribou",
            "label" => "Caribou ( ID )",
            "locale" => "ID | Caribou"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Cassia",
            "label" => "Cassia ( ID )",
            "locale" => "ID | Cassia"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Clark",
            "label" => "Clark ( ID )",
            "locale" => "ID | Clark"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Clearwater",
            "label" => "Clearwater ( ID )",
            "locale" => "ID | Clearwater"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Custer",
            "label" => "Custer ( ID )",
            "locale" => "ID | Custer"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Elmore",
            "label" => "Elmore ( ID )",
            "locale" => "ID | Elmore"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Franklin",
            "label" => "Franklin ( ID )",
            "locale" => "ID | Franklin"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Fremont",
            "label" => "Fremont ( ID )",
            "locale" => "ID | Fremont"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Gem",
            "label" => "Gem ( ID )",
            "locale" => "ID | Gem"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Gooding",
            "label" => "Gooding ( ID )",
            "locale" => "ID | Gooding"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Idaho",
            "label" => "Idaho ( ID )",
            "locale" => "ID | Idaho"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Jefferson",
            "label" => "Jefferson ( ID )",
            "locale" => "ID | Jefferson"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Jerome",
            "label" => "Jerome ( ID )",
            "locale" => "ID | Jerome"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Kootenai",
            "label" => "Kootenai ( ID )",
            "locale" => "ID | Kootenai"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Latah",
            "label" => "Latah ( ID )",
            "locale" => "ID | Latah"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Lemhi",
            "label" => "Lemhi ( ID )",
            "locale" => "ID | Lemhi"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Lewis",
            "label" => "Lewis ( ID )",
            "locale" => "ID | Lewis"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Lincoln",
            "label" => "Lincoln ( ID )",
            "locale" => "ID | Lincoln"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Madison",
            "label" => "Madison ( ID )",
            "locale" => "ID | Madison"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Minidoka",
            "label" => "Minidoka ( ID )",
            "locale" => "ID | Minidoka"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Nez Perce",
            "label" => "Nez Perce ( ID )",
            "locale" => "ID | Nez Perce"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Oneida",
            "label" => "Oneida ( ID )",
            "locale" => "ID | Oneida"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Owyhee",
            "label" => "Owyhee ( ID )",
            "locale" => "ID | Owyhee"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Payette",
            "label" => "Payette ( ID )",
            "locale" => "ID | Payette"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Power",
            "label" => "Power ( ID )",
            "locale" => "ID | Power"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Shoshone",
            "label" => "Shoshone ( ID )",
            "locale" => "ID | Shoshone"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Teton",
            "label" => "Teton ( ID )",
            "locale" => "ID | Teton"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Twin Falls",
            "label" => "Twin Falls ( ID )",
            "locale" => "ID | Twin Falls"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Valley",
            "label" => "Valley ( ID )",
            "locale" => "ID | Valley"
        ]);
        County::create([
            "state_id" => 13,
            "location_id" => 0,
            "county" => "Washington",
            "label" => "Washington ( ID )",
            "locale" => "ID | Washington"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Adams",
            "label" => "Adams ( IL )",
            "locale" => "IL | Adams"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Alexander",
            "label" => "Alexander ( IL )",
            "locale" => "IL | Alexander"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Bond",
            "label" => "Bond ( IL )",
            "locale" => "IL | Bond"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Boone",
            "label" => "Boone ( IL )",
            "locale" => "IL | Boone"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Brown",
            "label" => "Brown ( IL )",
            "locale" => "IL | Brown"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Bureau",
            "label" => "Bureau ( IL )",
            "locale" => "IL | Bureau"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Calhoun",
            "label" => "Calhoun ( IL )",
            "locale" => "IL | Calhoun"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Carroll",
            "label" => "Carroll ( IL )",
            "locale" => "IL | Carroll"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Cass",
            "label" => "Cass ( IL )",
            "locale" => "IL | Cass"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Champaign",
            "label" => "Champaign ( IL )",
            "locale" => "IL | Champaign"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Christian",
            "label" => "Christian ( IL )",
            "locale" => "IL | Christian"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Clark",
            "label" => "Clark ( IL )",
            "locale" => "IL | Clark"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Clay",
            "label" => "Clay ( IL )",
            "locale" => "IL | Clay"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Clinton",
            "label" => "Clinton ( IL )",
            "locale" => "IL | Clinton"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Coles",
            "label" => "Coles ( IL )",
            "locale" => "IL | Coles"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Cook",
            "label" => "Cook ( IL )",
            "locale" => "IL | Cook"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Crawford",
            "label" => "Crawford ( IL )",
            "locale" => "IL | Crawford"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Cumberland",
            "label" => "Cumberland ( IL )",
            "locale" => "IL | Cumberland"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "De Witt",
            "label" => "De Witt ( IL )",
            "locale" => "IL | De Witt"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "DeKalb",
            "label" => "DeKalb ( IL )",
            "locale" => "IL | DeKalb"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Douglas",
            "label" => "Douglas ( IL )",
            "locale" => "IL | Douglas"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "DuPage",
            "label" => "DuPage ( IL )",
            "locale" => "IL | DuPage"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Edgar",
            "label" => "Edgar ( IL )",
            "locale" => "IL | Edgar"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Edwards",
            "label" => "Edwards ( IL )",
            "locale" => "IL | Edwards"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Effingham",
            "label" => "Effingham ( IL )",
            "locale" => "IL | Effingham"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Fayette",
            "label" => "Fayette ( IL )",
            "locale" => "IL | Fayette"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Ford",
            "label" => "Ford ( IL )",
            "locale" => "IL | Ford"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Franklin",
            "label" => "Franklin ( IL )",
            "locale" => "IL | Franklin"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Fulton",
            "label" => "Fulton ( IL )",
            "locale" => "IL | Fulton"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Gallatin",
            "label" => "Gallatin ( IL )",
            "locale" => "IL | Gallatin"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Greene",
            "label" => "Greene ( IL )",
            "locale" => "IL | Greene"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Grundy",
            "label" => "Grundy ( IL )",
            "locale" => "IL | Grundy"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Hamilton",
            "label" => "Hamilton ( IL )",
            "locale" => "IL | Hamilton"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Hancock",
            "label" => "Hancock ( IL )",
            "locale" => "IL | Hancock"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Hardin",
            "label" => "Hardin ( IL )",
            "locale" => "IL | Hardin"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Henderson",
            "label" => "Henderson ( IL )",
            "locale" => "IL | Henderson"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Henry",
            "label" => "Henry ( IL )",
            "locale" => "IL | Henry"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Iroquois",
            "label" => "Iroquois ( IL )",
            "locale" => "IL | Iroquois"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Jackson",
            "label" => "Jackson ( IL )",
            "locale" => "IL | Jackson"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Jasper",
            "label" => "Jasper ( IL )",
            "locale" => "IL | Jasper"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Jefferson",
            "label" => "Jefferson ( IL )",
            "locale" => "IL | Jefferson"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Jersey",
            "label" => "Jersey ( IL )",
            "locale" => "IL | Jersey"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Jo Daviess",
            "label" => "Jo Daviess ( IL )",
            "locale" => "IL | Jo Daviess"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Johnson",
            "label" => "Johnson ( IL )",
            "locale" => "IL | Johnson"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Kane",
            "label" => "Kane ( IL )",
            "locale" => "IL | Kane"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Kankakee",
            "label" => "Kankakee ( IL )",
            "locale" => "IL | Kankakee"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Kendall",
            "label" => "Kendall ( IL )",
            "locale" => "IL | Kendall"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Knox",
            "label" => "Knox ( IL )",
            "locale" => "IL | Knox"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Lake",
            "label" => "Lake ( IL )",
            "locale" => "IL | Lake"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "LaSalle",
            "label" => "LaSalle ( IL )",
            "locale" => "IL | LaSalle"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Lawrence",
            "label" => "Lawrence ( IL )",
            "locale" => "IL | Lawrence"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Lee",
            "label" => "Lee ( IL )",
            "locale" => "IL | Lee"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Livingston",
            "label" => "Livingston ( IL )",
            "locale" => "IL | Livingston"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Logan",
            "label" => "Logan ( IL )",
            "locale" => "IL | Logan"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Macon",
            "label" => "Macon ( IL )",
            "locale" => "IL | Macon"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Macoupin",
            "label" => "Macoupin ( IL )",
            "locale" => "IL | Macoupin"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Madison",
            "label" => "Madison ( IL )",
            "locale" => "IL | Madison"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Marion",
            "label" => "Marion ( IL )",
            "locale" => "IL | Marion"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Marshall",
            "label" => "Marshall ( IL )",
            "locale" => "IL | Marshall"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Mason",
            "label" => "Mason ( IL )",
            "locale" => "IL | Mason"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Massac",
            "label" => "Massac ( IL )",
            "locale" => "IL | Massac"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "McDonough",
            "label" => "McDonough ( IL )",
            "locale" => "IL | McDonough"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "McHenry",
            "label" => "McHenry ( IL )",
            "locale" => "IL | McHenry"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "McLean",
            "label" => "McLean ( IL )",
            "locale" => "IL | McLean"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Menard",
            "label" => "Menard ( IL )",
            "locale" => "IL | Menard"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Mercer",
            "label" => "Mercer ( IL )",
            "locale" => "IL | Mercer"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Monroe",
            "label" => "Monroe ( IL )",
            "locale" => "IL | Monroe"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Montgomery",
            "label" => "Montgomery ( IL )",
            "locale" => "IL | Montgomery"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Morgan",
            "label" => "Morgan ( IL )",
            "locale" => "IL | Morgan"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Moultrie",
            "label" => "Moultrie ( IL )",
            "locale" => "IL | Moultrie"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Ogle",
            "label" => "Ogle ( IL )",
            "locale" => "IL | Ogle"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Peoria",
            "label" => "Peoria ( IL )",
            "locale" => "IL | Peoria"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Perry",
            "label" => "Perry ( IL )",
            "locale" => "IL | Perry"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Piatt",
            "label" => "Piatt ( IL )",
            "locale" => "IL | Piatt"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Pike",
            "label" => "Pike ( IL )",
            "locale" => "IL | Pike"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Pope",
            "label" => "Pope ( IL )",
            "locale" => "IL | Pope"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Pulaski",
            "label" => "Pulaski ( IL )",
            "locale" => "IL | Pulaski"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Putnam",
            "label" => "Putnam ( IL )",
            "locale" => "IL | Putnam"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Randolph",
            "label" => "Randolph ( IL )",
            "locale" => "IL | Randolph"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Richland",
            "label" => "Richland ( IL )",
            "locale" => "IL | Richland"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Rock Island",
            "label" => "Rock Island ( IL )",
            "locale" => "IL | Rock Island"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Saline",
            "label" => "Saline ( IL )",
            "locale" => "IL | Saline"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Sangamon",
            "label" => "Sangamon ( IL )",
            "locale" => "IL | Sangamon"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Schuyler",
            "label" => "Schuyler ( IL )",
            "locale" => "IL | Schuyler"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Scott",
            "label" => "Scott ( IL )",
            "locale" => "IL | Scott"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Shelby",
            "label" => "Shelby ( IL )",
            "locale" => "IL | Shelby"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "St. Clair",
            "label" => "St. Clair ( IL )",
            "locale" => "IL | St. Clair"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Stark",
            "label" => "Stark ( IL )",
            "locale" => "IL | Stark"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Stephenson",
            "label" => "Stephenson ( IL )",
            "locale" => "IL | Stephenson"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Tazewell",
            "label" => "Tazewell ( IL )",
            "locale" => "IL | Tazewell"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Union",
            "label" => "Union ( IL )",
            "locale" => "IL | Union"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Vermilion",
            "label" => "Vermilion ( IL )",
            "locale" => "IL | Vermilion"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Wabash",
            "label" => "Wabash ( IL )",
            "locale" => "IL | Wabash"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Warren",
            "label" => "Warren ( IL )",
            "locale" => "IL | Warren"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Washington",
            "label" => "Washington ( IL )",
            "locale" => "IL | Washington"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Wayne",
            "label" => "Wayne ( IL )",
            "locale" => "IL | Wayne"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "White",
            "label" => "White ( IL )",
            "locale" => "IL | White"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Whiteside",
            "label" => "Whiteside ( IL )",
            "locale" => "IL | Whiteside"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Will",
            "label" => "Will ( IL )",
            "locale" => "IL | Will"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Williamson",
            "label" => "Williamson ( IL )",
            "locale" => "IL | Williamson"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Winnebago",
            "label" => "Winnebago ( IL )",
            "locale" => "IL | Winnebago"
        ]);
        County::create([
            "state_id" => 14,
            "location_id" => 0,
            "county" => "Woodford",
            "label" => "Woodford ( IL )",
            "locale" => "IL | Woodford"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Adams",
            "label" => "Adams ( IN )",
            "locale" => "IN | Adams"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Allen",
            "label" => "Allen ( IN )",
            "locale" => "IN | Allen"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Bartholomew",
            "label" => "Bartholomew ( IN )",
            "locale" => "IN | Bartholomew"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Benton",
            "label" => "Benton ( IN )",
            "locale" => "IN | Benton"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Blackford",
            "label" => "Blackford ( IN )",
            "locale" => "IN | Blackford"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Boone",
            "label" => "Boone ( IN )",
            "locale" => "IN | Boone"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Brown",
            "label" => "Brown ( IN )",
            "locale" => "IN | Brown"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Carroll",
            "label" => "Carroll ( IN )",
            "locale" => "IN | Carroll"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Cass",
            "label" => "Cass ( IN )",
            "locale" => "IN | Cass"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Clark",
            "label" => "Clark ( IN )",
            "locale" => "IN | Clark"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Clay",
            "label" => "Clay ( IN )",
            "locale" => "IN | Clay"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Clinton",
            "label" => "Clinton ( IN )",
            "locale" => "IN | Clinton"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Crawford",
            "label" => "Crawford ( IN )",
            "locale" => "IN | Crawford"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Daviess",
            "label" => "Daviess ( IN )",
            "locale" => "IN | Daviess"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Dearborn",
            "label" => "Dearborn ( IN )",
            "locale" => "IN | Dearborn"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Decatur",
            "label" => "Decatur ( IN )",
            "locale" => "IN | Decatur"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "DeKalb",
            "label" => "DeKalb ( IN )",
            "locale" => "IN | DeKalb"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Delaware",
            "label" => "Delaware ( IN )",
            "locale" => "IN | Delaware"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Dubois",
            "label" => "Dubois ( IN )",
            "locale" => "IN | Dubois"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Elkhart",
            "label" => "Elkhart ( IN )",
            "locale" => "IN | Elkhart"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Fayette",
            "label" => "Fayette ( IN )",
            "locale" => "IN | Fayette"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Floyd",
            "label" => "Floyd ( IN )",
            "locale" => "IN | Floyd"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Fountain",
            "label" => "Fountain ( IN )",
            "locale" => "IN | Fountain"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Franklin",
            "label" => "Franklin ( IN )",
            "locale" => "IN | Franklin"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Fulton",
            "label" => "Fulton ( IN )",
            "locale" => "IN | Fulton"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Gibson",
            "label" => "Gibson ( IN )",
            "locale" => "IN | Gibson"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Grant",
            "label" => "Grant ( IN )",
            "locale" => "IN | Grant"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Greene",
            "label" => "Greene ( IN )",
            "locale" => "IN | Greene"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Hamilton",
            "label" => "Hamilton ( IN )",
            "locale" => "IN | Hamilton"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Hancock",
            "label" => "Hancock ( IN )",
            "locale" => "IN | Hancock"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Harrison",
            "label" => "Harrison ( IN )",
            "locale" => "IN | Harrison"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Hendricks",
            "label" => "Hendricks ( IN )",
            "locale" => "IN | Hendricks"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Henry",
            "label" => "Henry ( IN )",
            "locale" => "IN | Henry"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Howard",
            "label" => "Howard ( IN )",
            "locale" => "IN | Howard"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Huntington",
            "label" => "Huntington ( IN )",
            "locale" => "IN | Huntington"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Jackson",
            "label" => "Jackson ( IN )",
            "locale" => "IN | Jackson"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Jasper",
            "label" => "Jasper ( IN )",
            "locale" => "IN | Jasper"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Jay",
            "label" => "Jay ( IN )",
            "locale" => "IN | Jay"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Jefferson",
            "label" => "Jefferson ( IN )",
            "locale" => "IN | Jefferson"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Jennings",
            "label" => "Jennings ( IN )",
            "locale" => "IN | Jennings"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Johnson",
            "label" => "Johnson ( IN )",
            "locale" => "IN | Johnson"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Knox",
            "label" => "Knox ( IN )",
            "locale" => "IN | Knox"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Kosciusko",
            "label" => "Kosciusko ( IN )",
            "locale" => "IN | Kosciusko"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "LaGrange",
            "label" => "LaGrange ( IN )",
            "locale" => "IN | LaGrange"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Lake",
            "label" => "Lake ( IN )",
            "locale" => "IN | Lake"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "LaPorte",
            "label" => "LaPorte ( IN )",
            "locale" => "IN | LaPorte"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Lawrence",
            "label" => "Lawrence ( IN )",
            "locale" => "IN | Lawrence"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Madison",
            "label" => "Madison ( IN )",
            "locale" => "IN | Madison"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Marshall",
            "label" => "Marshall ( IN )",
            "locale" => "IN | Marshall"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Martin",
            "label" => "Martin ( IN )",
            "locale" => "IN | Martin"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Miami",
            "label" => "Miami ( IN )",
            "locale" => "IN | Miami"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Monroe",
            "label" => "Monroe ( IN )",
            "locale" => "IN | Monroe"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Montgomery",
            "label" => "Montgomery ( IN )",
            "locale" => "IN | Montgomery"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Morgan",
            "label" => "Morgan ( IN )",
            "locale" => "IN | Morgan"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Newton",
            "label" => "Newton ( IN )",
            "locale" => "IN | Newton"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Noble",
            "label" => "Noble ( IN )",
            "locale" => "IN | Noble"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Ohio",
            "label" => "Ohio ( IN )",
            "locale" => "IN | Ohio"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Orange",
            "label" => "Orange ( IN )",
            "locale" => "IN | Orange"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Owen",
            "label" => "Owen ( IN )",
            "locale" => "IN | Owen"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Parke",
            "label" => "Parke ( IN )",
            "locale" => "IN | Parke"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Perry",
            "label" => "Perry ( IN )",
            "locale" => "IN | Perry"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Pike",
            "label" => "Pike ( IN )",
            "locale" => "IN | Pike"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Porter",
            "label" => "Porter ( IN )",
            "locale" => "IN | Porter"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Posey",
            "label" => "Posey ( IN )",
            "locale" => "IN | Posey"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Pulaski",
            "label" => "Pulaski ( IN )",
            "locale" => "IN | Pulaski"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Putnam",
            "label" => "Putnam ( IN )",
            "locale" => "IN | Putnam"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Randolph",
            "label" => "Randolph ( IN )",
            "locale" => "IN | Randolph"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Ripley",
            "label" => "Ripley ( IN )",
            "locale" => "IN | Ripley"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Rush",
            "label" => "Rush ( IN )",
            "locale" => "IN | Rush"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Scott",
            "label" => "Scott ( IN )",
            "locale" => "IN | Scott"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Shelby",
            "label" => "Shelby ( IN )",
            "locale" => "IN | Shelby"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Spencer",
            "label" => "Spencer ( IN )",
            "locale" => "IN | Spencer"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "St. Joseph",
            "label" => "St. Joseph ( IN )",
            "locale" => "IN | St. Joseph"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Starke",
            "label" => "Starke ( IN )",
            "locale" => "IN | Starke"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Steuben",
            "label" => "Steuben ( IN )",
            "locale" => "IN | Steuben"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Sullivan",
            "label" => "Sullivan ( IN )",
            "locale" => "IN | Sullivan"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Switzerland",
            "label" => "Switzerland ( IN )",
            "locale" => "IN | Switzerland"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Tippecanoe",
            "label" => "Tippecanoe ( IN )",
            "locale" => "IN | Tippecanoe"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Tipton",
            "label" => "Tipton ( IN )",
            "locale" => "IN | Tipton"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Union",
            "label" => "Union ( IN )",
            "locale" => "IN | Union"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Vanderburgh",
            "label" => "Vanderburgh ( IN )",
            "locale" => "IN | Vanderburgh"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Vermillion",
            "label" => "Vermillion ( IN )",
            "locale" => "IN | Vermillion"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Vigo",
            "label" => "Vigo ( IN )",
            "locale" => "IN | Vigo"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Wabash",
            "label" => "Wabash ( IN )",
            "locale" => "IN | Wabash"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Warren",
            "label" => "Warren ( IN )",
            "locale" => "IN | Warren"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Warrick",
            "label" => "Warrick ( IN )",
            "locale" => "IN | Warrick"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Washington",
            "label" => "Washington ( IN )",
            "locale" => "IN | Washington"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Wayne",
            "label" => "Wayne ( IN )",
            "locale" => "IN | Wayne"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Wells",
            "label" => "Wells ( IN )",
            "locale" => "IN | Wells"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "White",
            "label" => "White ( IN )",
            "locale" => "IN | White"
        ]);
        County::create([
            "state_id" => 15,
            "location_id" => 0,
            "county" => "Whitley",
            "label" => "Whitley ( IN )",
            "locale" => "IN | Whitley"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Adair",
            "label" => "Adair ( IA )",
            "locale" => "IA | Adair"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Adams",
            "label" => "Adams ( IA )",
            "locale" => "IA | Adams"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Allamakee",
            "label" => "Allamakee ( IA )",
            "locale" => "IA | Allamakee"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Appanoose",
            "label" => "Appanoose ( IA )",
            "locale" => "IA | Appanoose"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Audubon",
            "label" => "Audubon ( IA )",
            "locale" => "IA | Audubon"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Benton",
            "label" => "Benton ( IA )",
            "locale" => "IA | Benton"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Black Hawk",
            "label" => "Black Hawk ( IA )",
            "locale" => "IA | Black Hawk"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Boone",
            "label" => "Boone ( IA )",
            "locale" => "IA | Boone"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Bremer",
            "label" => "Bremer ( IA )",
            "locale" => "IA | Bremer"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Buchanan",
            "label" => "Buchanan ( IA )",
            "locale" => "IA | Buchanan"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Buena Vista",
            "label" => "Buena Vista ( IA )",
            "locale" => "IA | Buena Vista"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Butler",
            "label" => "Butler ( IA )",
            "locale" => "IA | Butler"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Calhoun",
            "label" => "Calhoun ( IA )",
            "locale" => "IA | Calhoun"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Carroll",
            "label" => "Carroll ( IA )",
            "locale" => "IA | Carroll"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Cass",
            "label" => "Cass ( IA )",
            "locale" => "IA | Cass"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Cedar",
            "label" => "Cedar ( IA )",
            "locale" => "IA | Cedar"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Cerro Gordo",
            "label" => "Cerro Gordo ( IA )",
            "locale" => "IA | Cerro Gordo"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Cherokee",
            "label" => "Cherokee ( IA )",
            "locale" => "IA | Cherokee"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Chickasaw",
            "label" => "Chickasaw ( IA )",
            "locale" => "IA | Chickasaw"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Clarke",
            "label" => "Clarke ( IA )",
            "locale" => "IA | Clarke"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Clay",
            "label" => "Clay ( IA )",
            "locale" => "IA | Clay"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Clayton",
            "label" => "Clayton ( IA )",
            "locale" => "IA | Clayton"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Clinton",
            "label" => "Clinton ( IA )",
            "locale" => "IA | Clinton"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Crawford",
            "label" => "Crawford ( IA )",
            "locale" => "IA | Crawford"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Dallas",
            "label" => "Dallas ( IA )",
            "locale" => "IA | Dallas"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Davis",
            "label" => "Davis ( IA )",
            "locale" => "IA | Davis"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Decatur",
            "label" => "Decatur ( IA )",
            "locale" => "IA | Decatur"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Delaware",
            "label" => "Delaware ( IA )",
            "locale" => "IA | Delaware"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Des Moines",
            "label" => "Des Moines ( IA )",
            "locale" => "IA | Des Moines"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Dickinson",
            "label" => "Dickinson ( IA )",
            "locale" => "IA | Dickinson"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Dubuque",
            "label" => "Dubuque ( IA )",
            "locale" => "IA | Dubuque"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Emmet",
            "label" => "Emmet ( IA )",
            "locale" => "IA | Emmet"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Fayette",
            "label" => "Fayette ( IA )",
            "locale" => "IA | Fayette"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Floyd",
            "label" => "Floyd ( IA )",
            "locale" => "IA | Floyd"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Franklin",
            "label" => "Franklin ( IA )",
            "locale" => "IA | Franklin"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Fremont",
            "label" => "Fremont ( IA )",
            "locale" => "IA | Fremont"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Greene",
            "label" => "Greene ( IA )",
            "locale" => "IA | Greene"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Grundy",
            "label" => "Grundy ( IA )",
            "locale" => "IA | Grundy"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Guthrie",
            "label" => "Guthrie ( IA )",
            "locale" => "IA | Guthrie"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Hamilton",
            "label" => "Hamilton ( IA )",
            "locale" => "IA | Hamilton"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Hancock",
            "label" => "Hancock ( IA )",
            "locale" => "IA | Hancock"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Hardin",
            "label" => "Hardin ( IA )",
            "locale" => "IA | Hardin"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Harrison",
            "label" => "Harrison ( IA )",
            "locale" => "IA | Harrison"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Henry",
            "label" => "Henry ( IA )",
            "locale" => "IA | Henry"
        ]);
        County::create([
            "state_id" => 16,
            "location_id" => 0,
            "county" => "Howard",
            "label" => "Howard ( IA )",
            "locale" => "IA | Howard"
        ]);

        County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Humboldt",
			"label" => "Humboldt ( IA )",
			"locale" => "IA | Humboldt"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Ida",
			"label" => "Ida ( IA )",
			"locale" => "IA | Ida"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Iowa",
			"label" => "Iowa ( IA )",
			"locale" => "IA | Iowa"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Jackson",
			"label" => "Jackson ( IA )",
			"locale" => "IA | Jackson"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Jasper",
			"label" => "Jasper ( IA )",
			"locale" => "IA | Jasper"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Jefferson",
			"label" => "Jefferson ( IA )",
			"locale" => "IA | Jefferson"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Johnson",
			"label" => "Johnson ( IA )",
			"locale" => "IA | Johnson"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Jones",
			"label" => "Jones ( IA )",
			"locale" => "IA | Jones"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Keokuk",
			"label" => "Keokuk ( IA )",
			"locale" => "IA | Keokuk"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Kossuth",
			"label" => "Kossuth ( IA )",
			"locale" => "IA | Kossuth"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Lee",
			"label" => "Lee ( IA )",
			"locale" => "IA | Lee"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Linn",
			"label" => "Linn ( IA )",
			"locale" => "IA | Linn"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Louisa",
			"label" => "Louisa ( IA )",
			"locale" => "IA | Louisa"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Lucas",
			"label" => "Lucas ( IA )",
			"locale" => "IA | Lucas"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Lyon",
			"label" => "Lyon ( IA )",
			"locale" => "IA | Lyon"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Madison",
			"label" => "Madison ( IA )",
			"locale" => "IA | Madison"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Mahaska",
			"label" => "Mahaska ( IA )",
			"locale" => "IA | Mahaska"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Marion",
			"label" => "Marion ( IA )",
			"locale" => "IA | Marion"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Marshall",
			"label" => "Marshall ( IA )",
			"locale" => "IA | Marshall"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Mills",
			"label" => "Mills ( IA )",
			"locale" => "IA | Mills"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Mitchell",
			"label" => "Mitchell ( IA )",
			"locale" => "IA | Mitchell"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Monona",
			"label" => "Monona ( IA )",
			"locale" => "IA | Monona"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Monroe",
			"label" => "Monroe ( IA )",
			"locale" => "IA | Monroe"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Montgomery",
			"label" => "Montgomery ( IA )",
			"locale" => "IA | Montgomery"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Muscatine",
			"label" => "Muscatine ( IA )",
			"locale" => "IA | Muscatine"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "O'Brien",
			"label" => "O'Brien ( IA )",
			"locale" => "IA | O'Brien"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Osceola",
			"label" => "Osceola ( IA )",
			"locale" => "IA | Osceola"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Page",
			"label" => "Page ( IA )",
			"locale" => "IA | Page"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Palo Alto",
			"label" => "Palo Alto ( IA )",
			"locale" => "IA | Palo Alto"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Plymouth",
			"label" => "Plymouth ( IA )",
			"locale" => "IA | Plymouth"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Pocahontas",
			"label" => "Pocahontas ( IA )",
			"locale" => "IA | Pocahontas"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Polk",
			"label" => "Polk ( IA )",
			"locale" => "IA | Polk"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Pottawattamie",
			"label" => "Pottawattamie ( IA )",
			"locale" => "IA | Pottawattamie"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Poweshiek",
			"label" => "Poweshiek ( IA )",
			"locale" => "IA | Poweshiek"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Ringgold",
			"label" => "Ringgold ( IA )",
			"locale" => "IA | Ringgold"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Sac",
			"label" => "Sac ( IA )",
			"locale" => "IA | Sac"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Scott",
			"label" => "Scott ( IA )",
			"locale" => "IA | Scott"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Shelby",
			"label" => "Shelby ( IA )",
			"locale" => "IA | Shelby"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Sioux",
			"label" => "Sioux ( IA )",
			"locale" => "IA | Sioux"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Story",
			"label" => "Story ( IA )",
			"locale" => "IA | Story"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Tama",
			"label" => "Tama ( IA )",
			"locale" => "IA | Tama"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Taylor",
			"label" => "Taylor ( IA )",
			"locale" => "IA | Taylor"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Union",
			"label" => "Union ( IA )",
			"locale" => "IA | Union"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Van Buren",
			"label" => "Van Buren ( IA )",
			"locale" => "IA | Van Buren"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Wapello",
			"label" => "Wapello ( IA )",
			"locale" => "IA | Wapello"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Warren",
			"label" => "Warren ( IA )",
			"locale" => "IA | Warren"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( IA )",
			"locale" => "IA | Washington"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Wayne",
			"label" => "Wayne ( IA )",
			"locale" => "IA | Wayne"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Webster",
			"label" => "Webster ( IA )",
			"locale" => "IA | Webster"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Winnebago",
			"label" => "Winnebago ( IA )",
			"locale" => "IA | Winnebago"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Winneshiek",
			"label" => "Winneshiek ( IA )",
			"locale" => "IA | Winneshiek"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Woodbury",
			"label" => "Woodbury ( IA )",
			"locale" => "IA | Woodbury"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Worth",
			"label" => "Worth ( IA )",
			"locale" => "IA | Worth"
		]);
		County::create([
			"state_id" => 16,
			"location_id" => 0,
			"county" => "Wright",
			"label" => "Wright ( IA )",
			"locale" => "IA | Wright"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Allen",
			"label" => "Allen ( KS )",
			"locale" => "KS | Allen"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Anderson",
			"label" => "Anderson ( KS )",
			"locale" => "KS | Anderson"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Atchison",
			"label" => "Atchison ( KS )",
			"locale" => "KS | Atchison"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Barber",
			"label" => "Barber ( KS )",
			"locale" => "KS | Barber"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Barton",
			"label" => "Barton ( KS )",
			"locale" => "KS | Barton"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Bourbon",
			"label" => "Bourbon ( KS )",
			"locale" => "KS | Bourbon"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Brown",
			"label" => "Brown ( KS )",
			"locale" => "KS | Brown"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Butler",
			"label" => "Butler ( KS )",
			"locale" => "KS | Butler"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Chase",
			"label" => "Chase ( KS )",
			"locale" => "KS | Chase"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Chautauqua",
			"label" => "Chautauqua ( KS )",
			"locale" => "KS | Chautauqua"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Cherokee",
			"label" => "Cherokee ( KS )",
			"locale" => "KS | Cherokee"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Cheyenne",
			"label" => "Cheyenne ( KS )",
			"locale" => "KS | Cheyenne"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Clark",
			"label" => "Clark ( KS )",
			"locale" => "KS | Clark"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Clay",
			"label" => "Clay ( KS )",
			"locale" => "KS | Clay"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Cloud",
			"label" => "Cloud ( KS )",
			"locale" => "KS | Cloud"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Coffey",
			"label" => "Coffey ( KS )",
			"locale" => "KS | Coffey"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Comanche",
			"label" => "Comanche ( KS )",
			"locale" => "KS | Comanche"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Cowley",
			"label" => "Cowley ( KS )",
			"locale" => "KS | Cowley"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Crawford",
			"label" => "Crawford ( KS )",
			"locale" => "KS | Crawford"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Decatur",
			"label" => "Decatur ( KS )",
			"locale" => "KS | Decatur"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Dickinson",
			"label" => "Dickinson ( KS )",
			"locale" => "KS | Dickinson"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Doniphan",
			"label" => "Doniphan ( KS )",
			"locale" => "KS | Doniphan"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Douglas",
			"label" => "Douglas ( KS )",
			"locale" => "KS | Douglas"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Edwards",
			"label" => "Edwards ( KS )",
			"locale" => "KS | Edwards"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Elk",
			"label" => "Elk ( KS )",
			"locale" => "KS | Elk"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Ellis",
			"label" => "Ellis ( KS )",
			"locale" => "KS | Ellis"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Ellsworth",
			"label" => "Ellsworth ( KS )",
			"locale" => "KS | Ellsworth"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Finney",
			"label" => "Finney ( KS )",
			"locale" => "KS | Finney"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Ford",
			"label" => "Ford ( KS )",
			"locale" => "KS | Ford"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Franklin",
			"label" => "Franklin ( KS )",
			"locale" => "KS | Franklin"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Geary",
			"label" => "Geary ( KS )",
			"locale" => "KS | Geary"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Gove",
			"label" => "Gove ( KS )",
			"locale" => "KS | Gove"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Graham",
			"label" => "Graham ( KS )",
			"locale" => "KS | Graham"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Grant",
			"label" => "Grant ( KS )",
			"locale" => "KS | Grant"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Gray",
			"label" => "Gray ( KS )",
			"locale" => "KS | Gray"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Greeley",
			"label" => "Greeley ( KS )",
			"locale" => "KS | Greeley"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Greenwood",
			"label" => "Greenwood ( KS )",
			"locale" => "KS | Greenwood"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Hamilton",
			"label" => "Hamilton ( KS )",
			"locale" => "KS | Hamilton"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Harper",
			"label" => "Harper ( KS )",
			"locale" => "KS | Harper"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Harvey",
			"label" => "Harvey ( KS )",
			"locale" => "KS | Harvey"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Haskell",
			"label" => "Haskell ( KS )",
			"locale" => "KS | Haskell"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Hodgeman",
			"label" => "Hodgeman ( KS )",
			"locale" => "KS | Hodgeman"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Jackson",
			"label" => "Jackson ( KS )",
			"locale" => "KS | Jackson"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Jefferson",
			"label" => "Jefferson ( KS )",
			"locale" => "KS | Jefferson"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Jewell",
			"label" => "Jewell ( KS )",
			"locale" => "KS | Jewell"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Johnson",
			"label" => "Johnson ( KS )",
			"locale" => "KS | Johnson"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Kearny",
			"label" => "Kearny ( KS )",
			"locale" => "KS | Kearny"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Kingman",
			"label" => "Kingman ( KS )",
			"locale" => "KS | Kingman"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Kiowa",
			"label" => "Kiowa ( KS )",
			"locale" => "KS | Kiowa"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Labette",
			"label" => "Labette ( KS )",
			"locale" => "KS | Labette"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Lane",
			"label" => "Lane ( KS )",
			"locale" => "KS | Lane"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Leavenworth",
			"label" => "Leavenworth ( KS )",
			"locale" => "KS | Leavenworth"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Lincoln",
			"label" => "Lincoln ( KS )",
			"locale" => "KS | Lincoln"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Linn",
			"label" => "Linn ( KS )",
			"locale" => "KS | Linn"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Logan",
			"label" => "Logan ( KS )",
			"locale" => "KS | Logan"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Lyon",
			"label" => "Lyon ( KS )",
			"locale" => "KS | Lyon"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Marion",
			"label" => "Marion ( KS )",
			"locale" => "KS | Marion"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Marshall",
			"label" => "Marshall ( KS )",
			"locale" => "KS | Marshall"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "McPherson",
			"label" => "McPherson ( KS )",
			"locale" => "KS | McPherson"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Meade",
			"label" => "Meade ( KS )",
			"locale" => "KS | Meade"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Miami",
			"label" => "Miami ( KS )",
			"locale" => "KS | Miami"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Mitchell",
			"label" => "Mitchell ( KS )",
			"locale" => "KS | Mitchell"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Montgomery",
			"label" => "Montgomery ( KS )",
			"locale" => "KS | Montgomery"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Morris",
			"label" => "Morris ( KS )",
			"locale" => "KS | Morris"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Morton",
			"label" => "Morton ( KS )",
			"locale" => "KS | Morton"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Nemaha",
			"label" => "Nemaha ( KS )",
			"locale" => "KS | Nemaha"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Neosho",
			"label" => "Neosho ( KS )",
			"locale" => "KS | Neosho"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Ness",
			"label" => "Ness ( KS )",
			"locale" => "KS | Ness"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Norton",
			"label" => "Norton ( KS )",
			"locale" => "KS | Norton"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Osage",
			"label" => "Osage ( KS )",
			"locale" => "KS | Osage"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Osborne",
			"label" => "Osborne ( KS )",
			"locale" => "KS | Osborne"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Ottawa",
			"label" => "Ottawa ( KS )",
			"locale" => "KS | Ottawa"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Pawnee",
			"label" => "Pawnee ( KS )",
			"locale" => "KS | Pawnee"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Phillips",
			"label" => "Phillips ( KS )",
			"locale" => "KS | Phillips"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Pottawatomie",
			"label" => "Pottawatomie ( KS )",
			"locale" => "KS | Pottawatomie"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Pratt",
			"label" => "Pratt ( KS )",
			"locale" => "KS | Pratt"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Rawlins",
			"label" => "Rawlins ( KS )",
			"locale" => "KS | Rawlins"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Reno",
			"label" => "Reno ( KS )",
			"locale" => "KS | Reno"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Republic",
			"label" => "Republic ( KS )",
			"locale" => "KS | Republic"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Rice",
			"label" => "Rice ( KS )",
			"locale" => "KS | Rice"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Riley",
			"label" => "Riley ( KS )",
			"locale" => "KS | Riley"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Rooks",
			"label" => "Rooks ( KS )",
			"locale" => "KS | Rooks"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Rush",
			"label" => "Rush ( KS )",
			"locale" => "KS | Rush"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Russell",
			"label" => "Russell ( KS )",
			"locale" => "KS | Russell"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Saline",
			"label" => "Saline ( KS )",
			"locale" => "KS | Saline"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Scott",
			"label" => "Scott ( KS )",
			"locale" => "KS | Scott"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Sedgwick",
			"label" => "Sedgwick ( KS )",
			"locale" => "KS | Sedgwick"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Seward",
			"label" => "Seward ( KS )",
			"locale" => "KS | Seward"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Shawnee",
			"label" => "Shawnee ( KS )",
			"locale" => "KS | Shawnee"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Sheridan",
			"label" => "Sheridan ( KS )",
			"locale" => "KS | Sheridan"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Sherman",
			"label" => "Sherman ( KS )",
			"locale" => "KS | Sherman"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Smith",
			"label" => "Smith ( KS )",
			"locale" => "KS | Smith"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Stafford",
			"label" => "Stafford ( KS )",
			"locale" => "KS | Stafford"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Stanton",
			"label" => "Stanton ( KS )",
			"locale" => "KS | Stanton"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Stevens",
			"label" => "Stevens ( KS )",
			"locale" => "KS | Stevens"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Sumner",
			"label" => "Sumner ( KS )",
			"locale" => "KS | Sumner"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Thomas",
			"label" => "Thomas ( KS )",
			"locale" => "KS | Thomas"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Trego",
			"label" => "Trego ( KS )",
			"locale" => "KS | Trego"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Wabaunsee",
			"label" => "Wabaunsee ( KS )",
			"locale" => "KS | Wabaunsee"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Wallace",
			"label" => "Wallace ( KS )",
			"locale" => "KS | Wallace"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( KS )",
			"locale" => "KS | Washington"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Wichita",
			"label" => "Wichita ( KS )",
			"locale" => "KS | Wichita"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Wilson",
			"label" => "Wilson ( KS )",
			"locale" => "KS | Wilson"
		]);
		County::create([
			"state_id" => 17,
			"location_id" => 0,
			"county" => "Woodson",
			"label" => "Woodson ( KS )",
			"locale" => "KS | Woodson"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Adair",
			"label" => "Adair ( KY )",
			"locale" => "KY | Adair"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Allen",
			"label" => "Allen ( KY )",
			"locale" => "KY | Allen"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Anderson",
			"label" => "Anderson ( KY )",
			"locale" => "KY | Anderson"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Ballard",
			"label" => "Ballard ( KY )",
			"locale" => "KY | Ballard"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Barren",
			"label" => "Barren ( KY )",
			"locale" => "KY | Barren"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Bath",
			"label" => "Bath ( KY )",
			"locale" => "KY | Bath"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Bell",
			"label" => "Bell ( KY )",
			"locale" => "KY | Bell"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Boone",
			"label" => "Boone ( KY )",
			"locale" => "KY | Boone"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Bourbon",
			"label" => "Bourbon ( KY )",
			"locale" => "KY | Bourbon"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Boyd",
			"label" => "Boyd ( KY )",
			"locale" => "KY | Boyd"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Boyle",
			"label" => "Boyle ( KY )",
			"locale" => "KY | Boyle"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Bracken",
			"label" => "Bracken ( KY )",
			"locale" => "KY | Bracken"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Breathitt",
			"label" => "Breathitt ( KY )",
			"locale" => "KY | Breathitt"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Breckinridge",
			"label" => "Breckinridge ( KY )",
			"locale" => "KY | Breckinridge"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Bullitt",
			"label" => "Bullitt ( KY )",
			"locale" => "KY | Bullitt"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Butler",
			"label" => "Butler ( KY )",
			"locale" => "KY | Butler"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Caldwell",
			"label" => "Caldwell ( KY )",
			"locale" => "KY | Caldwell"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Calloway",
			"label" => "Calloway ( KY )",
			"locale" => "KY | Calloway"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Campbell",
			"label" => "Campbell ( KY )",
			"locale" => "KY | Campbell"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Carlisle",
			"label" => "Carlisle ( KY )",
			"locale" => "KY | Carlisle"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Carroll",
			"label" => "Carroll ( KY )",
			"locale" => "KY | Carroll"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Carter",
			"label" => "Carter ( KY )",
			"locale" => "KY | Carter"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Casey",
			"label" => "Casey ( KY )",
			"locale" => "KY | Casey"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Christian",
			"label" => "Christian ( KY )",
			"locale" => "KY | Christian"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Clark",
			"label" => "Clark ( KY )",
			"locale" => "KY | Clark"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Clay",
			"label" => "Clay ( KY )",
			"locale" => "KY | Clay"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Clinton",
			"label" => "Clinton ( KY )",
			"locale" => "KY | Clinton"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Crittenden",
			"label" => "Crittenden ( KY )",
			"locale" => "KY | Crittenden"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Cumberland",
			"label" => "Cumberland ( KY )",
			"locale" => "KY | Cumberland"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Daviess",
			"label" => "Daviess ( KY )",
			"locale" => "KY | Daviess"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Edmonson",
			"label" => "Edmonson ( KY )",
			"locale" => "KY | Edmonson"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Elliott",
			"label" => "Elliott ( KY )",
			"locale" => "KY | Elliott"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Estill",
			"label" => "Estill ( KY )",
			"locale" => "KY | Estill"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Fleming",
			"label" => "Fleming ( KY )",
			"locale" => "KY | Fleming"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Floyd",
			"label" => "Floyd ( KY )",
			"locale" => "KY | Floyd"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Franklin",
			"label" => "Franklin ( KY )",
			"locale" => "KY | Franklin"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Fulton",
			"label" => "Fulton ( KY )",
			"locale" => "KY | Fulton"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Gallatin",
			"label" => "Gallatin ( KY )",
			"locale" => "KY | Gallatin"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Garrard",
			"label" => "Garrard ( KY )",
			"locale" => "KY | Garrard"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Grant",
			"label" => "Grant ( KY )",
			"locale" => "KY | Grant"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Graves",
			"label" => "Graves ( KY )",
			"locale" => "KY | Graves"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Grayson",
			"label" => "Grayson ( KY )",
			"locale" => "KY | Grayson"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Green",
			"label" => "Green ( KY )",
			"locale" => "KY | Green"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Greenup",
			"label" => "Greenup ( KY )",
			"locale" => "KY | Greenup"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Hancock",
			"label" => "Hancock ( KY )",
			"locale" => "KY | Hancock"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Hardin",
			"label" => "Hardin ( KY )",
			"locale" => "KY | Hardin"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Harlan",
			"label" => "Harlan ( KY )",
			"locale" => "KY | Harlan"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Harrison",
			"label" => "Harrison ( KY )",
			"locale" => "KY | Harrison"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Hart",
			"label" => "Hart ( KY )",
			"locale" => "KY | Hart"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Henderson",
			"label" => "Henderson ( KY )",
			"locale" => "KY | Henderson"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Henry",
			"label" => "Henry ( KY )",
			"locale" => "KY | Henry"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Hickman",
			"label" => "Hickman ( KY )",
			"locale" => "KY | Hickman"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Hopkins",
			"label" => "Hopkins ( KY )",
			"locale" => "KY | Hopkins"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Jackson",
			"label" => "Jackson ( KY )",
			"locale" => "KY | Jackson"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Jessamine",
			"label" => "Jessamine ( KY )",
			"locale" => "KY | Jessamine"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Johnson",
			"label" => "Johnson ( KY )",
			"locale" => "KY | Johnson"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Kenton",
			"label" => "Kenton ( KY )",
			"locale" => "KY | Kenton"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Knott",
			"label" => "Knott ( KY )",
			"locale" => "KY | Knott"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Knox",
			"label" => "Knox ( KY )",
			"locale" => "KY | Knox"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Larue",
			"label" => "Larue ( KY )",
			"locale" => "KY | Larue"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Laurel",
			"label" => "Laurel ( KY )",
			"locale" => "KY | Laurel"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Lawrence",
			"label" => "Lawrence ( KY )",
			"locale" => "KY | Lawrence"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Lee",
			"label" => "Lee ( KY )",
			"locale" => "KY | Lee"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Leslie",
			"label" => "Leslie ( KY )",
			"locale" => "KY | Leslie"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Letcher",
			"label" => "Letcher ( KY )",
			"locale" => "KY | Letcher"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Lewis",
			"label" => "Lewis ( KY )",
			"locale" => "KY | Lewis"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Lincoln",
			"label" => "Lincoln ( KY )",
			"locale" => "KY | Lincoln"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Livingston",
			"label" => "Livingston ( KY )",
			"locale" => "KY | Livingston"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Logan",
			"label" => "Logan ( KY )",
			"locale" => "KY | Logan"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Lyon",
			"label" => "Lyon ( KY )",
			"locale" => "KY | Lyon"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Madison",
			"label" => "Madison ( KY )",
			"locale" => "KY | Madison"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Magoffin",
			"label" => "Magoffin ( KY )",
			"locale" => "KY | Magoffin"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Marion",
			"label" => "Marion ( KY )",
			"locale" => "KY | Marion"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Marshall",
			"label" => "Marshall ( KY )",
			"locale" => "KY | Marshall"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Martin",
			"label" => "Martin ( KY )",
			"locale" => "KY | Martin"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Mason",
			"label" => "Mason ( KY )",
			"locale" => "KY | Mason"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "McCracken",
			"label" => "McCracken ( KY )",
			"locale" => "KY | McCracken"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "McCreary",
			"label" => "McCreary ( KY )",
			"locale" => "KY | McCreary"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "McLean",
			"label" => "McLean ( KY )",
			"locale" => "KY | McLean"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Meade",
			"label" => "Meade ( KY )",
			"locale" => "KY | Meade"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Menifee",
			"label" => "Menifee ( KY )",
			"locale" => "KY | Menifee"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Mercer",
			"label" => "Mercer ( KY )",
			"locale" => "KY | Mercer"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Metcalfe",
			"label" => "Metcalfe ( KY )",
			"locale" => "KY | Metcalfe"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Monroe",
			"label" => "Monroe ( KY )",
			"locale" => "KY | Monroe"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Montgomery",
			"label" => "Montgomery ( KY )",
			"locale" => "KY | Montgomery"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Morgan",
			"label" => "Morgan ( KY )",
			"locale" => "KY | Morgan"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Muhlenberg",
			"label" => "Muhlenberg ( KY )",
			"locale" => "KY | Muhlenberg"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Nelson",
			"label" => "Nelson ( KY )",
			"locale" => "KY | Nelson"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Nicholas",
			"label" => "Nicholas ( KY )",
			"locale" => "KY | Nicholas"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Ohio",
			"label" => "Ohio ( KY )",
			"locale" => "KY | Ohio"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Oldham",
			"label" => "Oldham ( KY )",
			"locale" => "KY | Oldham"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Owen",
			"label" => "Owen ( KY )",
			"locale" => "KY | Owen"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Owsley",
			"label" => "Owsley ( KY )",
			"locale" => "KY | Owsley"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Pendleton",
			"label" => "Pendleton ( KY )",
			"locale" => "KY | Pendleton"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Perry",
			"label" => "Perry ( KY )",
			"locale" => "KY | Perry"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Pike",
			"label" => "Pike ( KY )",
			"locale" => "KY | Pike"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Powell",
			"label" => "Powell ( KY )",
			"locale" => "KY | Powell"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Pulaski",
			"label" => "Pulaski ( KY )",
			"locale" => "KY | Pulaski"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Robertson",
			"label" => "Robertson ( KY )",
			"locale" => "KY | Robertson"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Rockcastle",
			"label" => "Rockcastle ( KY )",
			"locale" => "KY | Rockcastle"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Rowan",
			"label" => "Rowan ( KY )",
			"locale" => "KY | Rowan"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Russell",
			"label" => "Russell ( KY )",
			"locale" => "KY | Russell"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Scott",
			"label" => "Scott ( KY )",
			"locale" => "KY | Scott"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Shelby",
			"label" => "Shelby ( KY )",
			"locale" => "KY | Shelby"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Simpson",
			"label" => "Simpson ( KY )",
			"locale" => "KY | Simpson"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Spencer",
			"label" => "Spencer ( KY )",
			"locale" => "KY | Spencer"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Taylor",
			"label" => "Taylor ( KY )",
			"locale" => "KY | Taylor"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Todd",
			"label" => "Todd ( KY )",
			"locale" => "KY | Todd"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Trigg",
			"label" => "Trigg ( KY )",
			"locale" => "KY | Trigg"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Trimble",
			"label" => "Trimble ( KY )",
			"locale" => "KY | Trimble"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Union",
			"label" => "Union ( KY )",
			"locale" => "KY | Union"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Warren",
			"label" => "Warren ( KY )",
			"locale" => "KY | Warren"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( KY )",
			"locale" => "KY | Washington"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Wayne",
			"label" => "Wayne ( KY )",
			"locale" => "KY | Wayne"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Webster",
			"label" => "Webster ( KY )",
			"locale" => "KY | Webster"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Whitley",
			"label" => "Whitley ( KY )",
			"locale" => "KY | Whitley"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Wolfe",
			"label" => "Wolfe ( KY )",
			"locale" => "KY | Wolfe"
		]);
		County::create([
			"state_id" => 18,
			"location_id" => 0,
			"county" => "Woodford",
			"label" => "Woodford ( KY )",
			"locale" => "KY | Woodford"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 0,
			"county" => "Acadia",
			"label" => "Acadia ( LA )",
			"locale" => "LA | Acadia"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Allen",
			"label" => "Allen ( LA )",
			"locale" => "LA | Allen"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 3,
			"county" => "Ascension",
			"label" => "Ascension ( LA )",
			"locale" => "LA | Ascension"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 3,
			"county" => "Assumption",
			"label" => "Assumption ( LA )",
			"locale" => "LA | Assumption"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Avoyelles",
			"label" => "Avoyelles ( LA )",
			"locale" => "LA | Avoyelles"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Beauregard",
			"label" => "Beauregard ( LA )",
			"locale" => "LA | Beauregard"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Bienville",
			"label" => "Bienville ( LA )",
			"locale" => "LA | Bienville"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 3,
			"county" => "Bossier",
			"label" => "Bossier ( LA )",
			"locale" => "LA | Bossier"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 3,
			"county" => "Caddo",
			"label" => "Caddo ( LA )",
			"locale" => "LA | Caddo"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 3,
			"county" => "Calcasieu",
			"label" => "Calcasieu ( LA )",
			"locale" => "LA | Calcasieu"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Caldwell",
			"label" => "Caldwell ( LA )",
			"locale" => "LA | Caldwell"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Cameron",
			"label" => "Cameron ( LA )",
			"locale" => "LA | Cameron"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Catahoula",
			"label" => "Catahoula ( LA )",
			"locale" => "LA | Catahoula"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Claiborne",
			"label" => "Claiborne ( LA )",
			"locale" => "LA | Claiborne"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Concordia",
			"label" => "Concordia ( LA )",
			"locale" => "LA | Concordia"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "De Soto",
			"label" => "De Soto ( LA )",
			"locale" => "LA | De Soto"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 3,
			"county" => "East Baton Rouge",
			"label" => "East Baton Rouge ( LA )",
			"locale" => "LA | East Baton Rouge"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "East Carroll",
			"label" => "East Carroll ( LA )",
			"locale" => "LA | East Carroll"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "East Feliciana",
			"label" => "East Feliciana ( LA )",
			"locale" => "LA | East Feliciana"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 3,
			"county" => "Evangeline",
			"label" => "Evangeline ( LA )",
			"locale" => "LA | Evangeline"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Franklin",
			"label" => "Franklin ( LA )",
			"locale" => "LA | Franklin"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Grant",
			"label" => "Grant ( LA )",
			"locale" => "LA | Grant"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 3,
			"county" => "Iberia",
			"label" => "Iberia ( LA )",
			"locale" => "LA | Iberia"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 3,
			"county" => "Iberville",
			"label" => "Iberville ( LA )",
			"locale" => "LA | Iberville"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Jackson",
			"label" => "Jackson ( LA )",
			"locale" => "LA | Jackson"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Jefferson",
			"label" => "Jefferson ( LA )",
			"locale" => "LA | Jefferson"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Jefferson Davis",
			"label" => "Jefferson Davis ( LA )",
			"locale" => "LA | Jefferson Davis"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "La Salle",
			"label" => "La Salle ( LA )",
			"locale" => "LA | La Salle"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 3,
			"county" => "Lafayette",
			"label" => "Lafayette ( LA )",
			"locale" => "LA | Lafayette"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 3,
			"county" => "Lafourche",
			"label" => "Lafourche ( LA )",
			"locale" => "LA | Lafourche"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Lincoln",
			"label" => "Lincoln ( LA )",
			"locale" => "LA | Lincoln"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Livingston",
			"label" => "Livingston ( LA )",
			"locale" => "LA | Livingston"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Madison",
			"label" => "Madison ( LA )",
			"locale" => "LA | Madison"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Morehouse",
			"label" => "Morehouse ( LA )",
			"locale" => "LA | Morehouse"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Natchitoches",
			"label" => "Natchitoches ( LA )",
			"locale" => "LA | Natchitoches"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Ouachita",
			"label" => "Ouachita ( LA )",
			"locale" => "LA | Ouachita"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 3,
			"county" => "Plaquemines",
			"label" => "Plaquemines ( LA )",
			"locale" => "LA | Plaquemines"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Pointe Coupee",
			"label" => "Pointe Coupee ( LA )",
			"locale" => "LA | Pointe Coupee"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Rapides",
			"label" => "Rapides ( LA )",
			"locale" => "LA | Rapides"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Red River",
			"label" => "Red River ( LA )",
			"locale" => "LA | Red River"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Richland",
			"label" => "Richland ( LA )",
			"locale" => "LA | Richland"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Sabine",
			"label" => "Sabine ( LA )",
			"locale" => "LA | Sabine"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 3,
			"county" => "St. Bernard",
			"label" => "St. Bernard ( LA )",
			"locale" => "LA | St. Bernard"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "St. Charles",
			"label" => "St. Charles ( LA )",
			"locale" => "LA | St. Charles"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "St. Helena",
			"label" => "St. Helena ( LA )",
			"locale" => "LA | St. Helena"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "St. James",
			"label" => "St. James ( LA )",
			"locale" => "LA | St. James"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "St. John the Baptist",
			"label" => "St. John the Baptist ( LA )",
			"locale" => "LA | St. John the Baptist"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "St. Landry",
			"label" => "St. Landry ( LA )",
			"locale" => "LA | St. Landry"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "St. Martin",
			"label" => "St. Martin ( LA )",
			"locale" => "LA | St. Martin"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "St. Mary",
			"label" => "St. Mary ( LA )",
			"locale" => "LA | St. Mary"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 3,
			"county" => "St. Tammany",
			"label" => "St. Tammany ( LA )",
			"locale" => "LA | St. Tammany"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 3,
			"county" => "Tangipahoa",
			"label" => "Tangipahoa ( LA )",
			"locale" => "LA | Tangipahoa"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Tensas",
			"label" => "Tensas ( LA )",
			"locale" => "LA | Tensas"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Terrebonne",
			"label" => "Terrebonne ( LA )",
			"locale" => "LA | Terrebonne"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Union",
			"label" => "Union ( LA )",
			"locale" => "LA | Union"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 3,
			"county" => "Vermilion",
			"label" => "Vermilion ( LA )",
			"locale" => "LA | Vermilion"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Vernon",
			"label" => "Vernon ( LA )",
			"locale" => "LA | Vernon"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Washington",
			"label" => "Washington ( LA )",
			"locale" => "LA | Washington"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Webster",
			"label" => "Webster ( LA )",
			"locale" => "LA | Webster"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 3,
			"county" => "West Baton Rouge",
			"label" => "West Baton Rouge ( LA )",
			"locale" => "LA | West Baton Rouge"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "West Carroll",
			"label" => "West Carroll ( LA )",
			"locale" => "LA | West Carroll"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "West Feliciana",
			"label" => "West Feliciana ( LA )",
			"locale" => "LA | West Feliciana"
		]);
		County::create([
			"state_id" => 19,
			"location_id" => 5,
			"county" => "Winn",
			"label" => "Winn ( LA )",
			"locale" => "LA | Winn"
		]);
		County::create([
			"state_id" => 20,
			"location_id" => 0,
			"county" => "Androscoggin",
			"label" => "Androscoggin ( ME )",
			"locale" => "ME | Androscoggin"
		]);
		County::create([
			"state_id" => 20,
			"location_id" => 0,
			"county" => "Aroostook",
			"label" => "Aroostook ( ME )",
			"locale" => "ME | Aroostook"
		]);
		County::create([
			"state_id" => 20,
			"location_id" => 0,
			"county" => "Cumberland",
			"label" => "Cumberland ( ME )",
			"locale" => "ME | Cumberland"
		]);
		County::create([
			"state_id" => 20,
			"location_id" => 0,
			"county" => "Franklin",
			"label" => "Franklin ( ME )",
			"locale" => "ME | Franklin"
		]);
		County::create([
			"state_id" => 20,
			"location_id" => 0,
			"county" => "Hancock",
			"label" => "Hancock ( ME )",
			"locale" => "ME | Hancock"
		]);
		County::create([
			"state_id" => 20,
			"location_id" => 0,
			"county" => "Kennebec",
			"label" => "Kennebec ( ME )",
			"locale" => "ME | Kennebec"
		]);
		County::create([
			"state_id" => 20,
			"location_id" => 0,
			"county" => "Knox",
			"label" => "Knox ( ME )",
			"locale" => "ME | Knox"
		]);
		County::create([
			"state_id" => 20,
			"location_id" => 0,
			"county" => "Lincoln",
			"label" => "Lincoln ( ME )",
			"locale" => "ME | Lincoln"
		]);
		County::create([
			"state_id" => 20,
			"location_id" => 0,
			"county" => "Oxford",
			"label" => "Oxford ( ME )",
			"locale" => "ME | Oxford"
		]);
		County::create([
			"state_id" => 20,
			"location_id" => 0,
			"county" => "Penobscot",
			"label" => "Penobscot ( ME )",
			"locale" => "ME | Penobscot"
		]);
		County::create([
			"state_id" => 20,
			"location_id" => 0,
			"county" => "Piscataquis",
			"label" => "Piscataquis ( ME )",
			"locale" => "ME | Piscataquis"
		]);
		County::create([
			"state_id" => 20,
			"location_id" => 0,
			"county" => "Sagadahoc",
			"label" => "Sagadahoc ( ME )",
			"locale" => "ME | Sagadahoc"
		]);
		County::create([
			"state_id" => 20,
			"location_id" => 0,
			"county" => "Somerset",
			"label" => "Somerset ( ME )",
			"locale" => "ME | Somerset"
		]);
		County::create([
			"state_id" => 20,
			"location_id" => 0,
			"county" => "Waldo",
			"label" => "Waldo ( ME )",
			"locale" => "ME | Waldo"
		]);
		County::create([
			"state_id" => 20,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( ME )",
			"locale" => "ME | Washington"
		]);
		County::create([
			"state_id" => 20,
			"location_id" => 0,
			"county" => "York",
			"label" => "York ( ME )",
			"locale" => "ME | York"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Allegany",
			"label" => "Allegany ( MD )",
			"locale" => "MD | Allegany"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Anne Arundel",
			"label" => "Anne Arundel ( MD )",
			"locale" => "MD | Anne Arundel"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Baltimore",
			"label" => "Baltimore ( MD )",
			"locale" => "MD | Baltimore"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Calvert",
			"label" => "Calvert ( MD )",
			"locale" => "MD | Calvert"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Caroline",
			"label" => "Caroline ( MD )",
			"locale" => "MD | Caroline"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Carroll",
			"label" => "Carroll ( MD )",
			"locale" => "MD | Carroll"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Cecil",
			"label" => "Cecil ( MD )",
			"locale" => "MD | Cecil"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Charles",
			"label" => "Charles ( MD )",
			"locale" => "MD | Charles"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Dorchester",
			"label" => "Dorchester ( MD )",
			"locale" => "MD | Dorchester"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Frederick",
			"label" => "Frederick ( MD )",
			"locale" => "MD | Frederick"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Garrett",
			"label" => "Garrett ( MD )",
			"locale" => "MD | Garrett"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Harford",
			"label" => "Harford ( MD )",
			"locale" => "MD | Harford"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Howard",
			"label" => "Howard ( MD )",
			"locale" => "MD | Howard"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Kent",
			"label" => "Kent ( MD )",
			"locale" => "MD | Kent"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Montgomery",
			"label" => "Montgomery ( MD )",
			"locale" => "MD | Montgomery"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Prince George's",
			"label" => "Prince George's ( MD )",
			"locale" => "MD | Prince George's"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Queen Anne's",
			"label" => "Queen Anne's ( MD )",
			"locale" => "MD | Queen Anne's"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Somerset",
			"label" => "Somerset ( MD )",
			"locale" => "MD | Somerset"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "St. Mary's",
			"label" => "St. Mary's ( MD )",
			"locale" => "MD | St. Mary's"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Talbot",
			"label" => "Talbot ( MD )",
			"locale" => "MD | Talbot"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( MD )",
			"locale" => "MD | Washington"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Wicomico",
			"label" => "Wicomico ( MD )",
			"locale" => "MD | Wicomico"
		]);
		County::create([
			"state_id" => 21,
			"location_id" => 0,
			"county" => "Worcester",
			"label" => "Worcester ( MD )",
			"locale" => "MD | Worcester"
		]);
		County::create([
			"state_id" => 22,
			"location_id" => 0,
			"county" => "Barnstable",
			"label" => "Barnstable ( MA )",
			"locale" => "MA | Barnstable"
		]);
		County::create([
			"state_id" => 22,
			"location_id" => 0,
			"county" => "Bristol",
			"label" => "Bristol ( MA )",
			"locale" => "MA | Bristol"
		]);
		County::create([
			"state_id" => 22,
			"location_id" => 0,
			"county" => "Dukes",
			"label" => "Dukes ( MA )",
			"locale" => "MA | Dukes"
		]);
		County::create([
			"state_id" => 22,
			"location_id" => 0,
			"county" => "Norfolk",
			"label" => "Norfolk ( MA )",
			"locale" => "MA | Norfolk"
		]);
		County::create([
			"state_id" => 22,
			"location_id" => 0,
			"county" => "Plymouth",
			"label" => "Plymouth ( MA )",
			"locale" => "MA | Plymouth"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Alcona",
			"label" => "Alcona ( MI )",
			"locale" => "MI | Alcona"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Alger",
			"label" => "Alger ( MI )",
			"locale" => "MI | Alger"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Allegan",
			"label" => "Allegan ( MI )",
			"locale" => "MI | Allegan"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Alpena",
			"label" => "Alpena ( MI )",
			"locale" => "MI | Alpena"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Antrim",
			"label" => "Antrim ( MI )",
			"locale" => "MI | Antrim"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Arenac",
			"label" => "Arenac ( MI )",
			"locale" => "MI | Arenac"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Baraga",
			"label" => "Baraga ( MI )",
			"locale" => "MI | Baraga"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Barry",
			"label" => "Barry ( MI )",
			"locale" => "MI | Barry"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Bay",
			"label" => "Bay ( MI )",
			"locale" => "MI | Bay"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Benzie",
			"label" => "Benzie ( MI )",
			"locale" => "MI | Benzie"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Berrien",
			"label" => "Berrien ( MI )",
			"locale" => "MI | Berrien"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Branch",
			"label" => "Branch ( MI )",
			"locale" => "MI | Branch"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Calhoun",
			"label" => "Calhoun ( MI )",
			"locale" => "MI | Calhoun"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Cass",
			"label" => "Cass ( MI )",
			"locale" => "MI | Cass"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Charlevoix",
			"label" => "Charlevoix ( MI )",
			"locale" => "MI | Charlevoix"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Cheboygan",
			"label" => "Cheboygan ( MI )",
			"locale" => "MI | Cheboygan"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Chippewa",
			"label" => "Chippewa ( MI )",
			"locale" => "MI | Chippewa"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Clare",
			"label" => "Clare ( MI )",
			"locale" => "MI | Clare"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Clinton",
			"label" => "Clinton ( MI )",
			"locale" => "MI | Clinton"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Crawford",
			"label" => "Crawford ( MI )",
			"locale" => "MI | Crawford"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Delta",
			"label" => "Delta ( MI )",
			"locale" => "MI | Delta"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Dickinson",
			"label" => "Dickinson ( MI )",
			"locale" => "MI | Dickinson"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Eaton",
			"label" => "Eaton ( MI )",
			"locale" => "MI | Eaton"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Emmet",
			"label" => "Emmet ( MI )",
			"locale" => "MI | Emmet"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Genesee",
			"label" => "Genesee ( MI )",
			"locale" => "MI | Genesee"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Gladwin",
			"label" => "Gladwin ( MI )",
			"locale" => "MI | Gladwin"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Gogebic",
			"label" => "Gogebic ( MI )",
			"locale" => "MI | Gogebic"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Grand Traverse",
			"label" => "Grand Traverse ( MI )",
			"locale" => "MI | Grand Traverse"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Gratiot",
			"label" => "Gratiot ( MI )",
			"locale" => "MI | Gratiot"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Hillsdale",
			"label" => "Hillsdale ( MI )",
			"locale" => "MI | Hillsdale"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Houghton",
			"label" => "Houghton ( MI )",
			"locale" => "MI | Houghton"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Huron",
			"label" => "Huron ( MI )",
			"locale" => "MI | Huron"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Ingham",
			"label" => "Ingham ( MI )",
			"locale" => "MI | Ingham"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Ionia",
			"label" => "Ionia ( MI )",
			"locale" => "MI | Ionia"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Iosco",
			"label" => "Iosco ( MI )",
			"locale" => "MI | Iosco"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Iron",
			"label" => "Iron ( MI )",
			"locale" => "MI | Iron"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Isabella",
			"label" => "Isabella ( MI )",
			"locale" => "MI | Isabella"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Jackson",
			"label" => "Jackson ( MI )",
			"locale" => "MI | Jackson"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Kalamazoo",
			"label" => "Kalamazoo ( MI )",
			"locale" => "MI | Kalamazoo"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Kalkaska",
			"label" => "Kalkaska ( MI )",
			"locale" => "MI | Kalkaska"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Kent",
			"label" => "Kent ( MI )",
			"locale" => "MI | Kent"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Keweenaw",
			"label" => "Keweenaw ( MI )",
			"locale" => "MI | Keweenaw"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Lake",
			"label" => "Lake ( MI )",
			"locale" => "MI | Lake"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Lapeer",
			"label" => "Lapeer ( MI )",
			"locale" => "MI | Lapeer"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Leelanau",
			"label" => "Leelanau ( MI )",
			"locale" => "MI | Leelanau"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Lenawee",
			"label" => "Lenawee ( MI )",
			"locale" => "MI | Lenawee"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Livingston",
			"label" => "Livingston ( MI )",
			"locale" => "MI | Livingston"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Luce",
			"label" => "Luce ( MI )",
			"locale" => "MI | Luce"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Mackinac",
			"label" => "Mackinac ( MI )",
			"locale" => "MI | Mackinac"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Macomb",
			"label" => "Macomb ( MI )",
			"locale" => "MI | Macomb"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Manistee",
			"label" => "Manistee ( MI )",
			"locale" => "MI | Manistee"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Marquette",
			"label" => "Marquette ( MI )",
			"locale" => "MI | Marquette"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Mason",
			"label" => "Mason ( MI )",
			"locale" => "MI | Mason"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Mecosta",
			"label" => "Mecosta ( MI )",
			"locale" => "MI | Mecosta"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Menominee",
			"label" => "Menominee ( MI )",
			"locale" => "MI | Menominee"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Midland",
			"label" => "Midland ( MI )",
			"locale" => "MI | Midland"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Missaukee",
			"label" => "Missaukee ( MI )",
			"locale" => "MI | Missaukee"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Monroe",
			"label" => "Monroe ( MI )",
			"locale" => "MI | Monroe"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Montcalm",
			"label" => "Montcalm ( MI )",
			"locale" => "MI | Montcalm"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Montmorency",
			"label" => "Montmorency ( MI )",
			"locale" => "MI | Montmorency"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Muskegon",
			"label" => "Muskegon ( MI )",
			"locale" => "MI | Muskegon"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Newaygo",
			"label" => "Newaygo ( MI )",
			"locale" => "MI | Newaygo"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Oakland",
			"label" => "Oakland ( MI )",
			"locale" => "MI | Oakland"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Oceana",
			"label" => "Oceana ( MI )",
			"locale" => "MI | Oceana"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Ogemaw",
			"label" => "Ogemaw ( MI )",
			"locale" => "MI | Ogemaw"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Ontonagon",
			"label" => "Ontonagon ( MI )",
			"locale" => "MI | Ontonagon"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Osceola",
			"label" => "Osceola ( MI )",
			"locale" => "MI | Osceola"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Oscoda",
			"label" => "Oscoda ( MI )",
			"locale" => "MI | Oscoda"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Otsego",
			"label" => "Otsego ( MI )",
			"locale" => "MI | Otsego"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Ottawa",
			"label" => "Ottawa ( MI )",
			"locale" => "MI | Ottawa"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Presque Isle",
			"label" => "Presque Isle ( MI )",
			"locale" => "MI | Presque Isle"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Roscommon",
			"label" => "Roscommon ( MI )",
			"locale" => "MI | Roscommon"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Saginaw",
			"label" => "Saginaw ( MI )",
			"locale" => "MI | Saginaw"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Sanilac",
			"label" => "Sanilac ( MI )",
			"locale" => "MI | Sanilac"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Schoolcraft",
			"label" => "Schoolcraft ( MI )",
			"locale" => "MI | Schoolcraft"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Shiawassee",
			"label" => "Shiawassee ( MI )",
			"locale" => "MI | Shiawassee"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "St. Clair",
			"label" => "St. Clair ( MI )",
			"locale" => "MI | St. Clair"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "St. Joseph",
			"label" => "St. Joseph ( MI )",
			"locale" => "MI | St. Joseph"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Tuscola",
			"label" => "Tuscola ( MI )",
			"locale" => "MI | Tuscola"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Van Buren",
			"label" => "Van Buren ( MI )",
			"locale" => "MI | Van Buren"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Washtenaw",
			"label" => "Washtenaw ( MI )",
			"locale" => "MI | Washtenaw"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Wayne",
			"label" => "Wayne ( MI )",
			"locale" => "MI | Wayne"
		]);
		County::create([
			"state_id" => 23,
			"location_id" => 0,
			"county" => "Wexford",
			"label" => "Wexford ( MI )",
			"locale" => "MI | Wexford"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Aitkin",
			"label" => "Aitkin ( MN )",
			"locale" => "MN | Aitkin"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Anoka",
			"label" => "Anoka ( MN )",
			"locale" => "MN | Anoka"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Becker",
			"label" => "Becker ( MN )",
			"locale" => "MN | Becker"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Beltrami",
			"label" => "Beltrami ( MN )",
			"locale" => "MN | Beltrami"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Benton",
			"label" => "Benton ( MN )",
			"locale" => "MN | Benton"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Big Stone",
			"label" => "Big Stone ( MN )",
			"locale" => "MN | Big Stone"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Blue Earth",
			"label" => "Blue Earth ( MN )",
			"locale" => "MN | Blue Earth"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Brown",
			"label" => "Brown ( MN )",
			"locale" => "MN | Brown"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Carlton",
			"label" => "Carlton ( MN )",
			"locale" => "MN | Carlton"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Carver",
			"label" => "Carver ( MN )",
			"locale" => "MN | Carver"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Cass",
			"label" => "Cass ( MN )",
			"locale" => "MN | Cass"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Chippewa",
			"label" => "Chippewa ( MN )",
			"locale" => "MN | Chippewa"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Chisago",
			"label" => "Chisago ( MN )",
			"locale" => "MN | Chisago"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Clay",
			"label" => "Clay ( MN )",
			"locale" => "MN | Clay"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Clearwater",
			"label" => "Clearwater ( MN )",
			"locale" => "MN | Clearwater"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Cook",
			"label" => "Cook ( MN )",
			"locale" => "MN | Cook"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Cottonwood",
			"label" => "Cottonwood ( MN )",
			"locale" => "MN | Cottonwood"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Crow Wing",
			"label" => "Crow Wing ( MN )",
			"locale" => "MN | Crow Wing"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Dakota",
			"label" => "Dakota ( MN )",
			"locale" => "MN | Dakota"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Dodge",
			"label" => "Dodge ( MN )",
			"locale" => "MN | Dodge"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Douglas",
			"label" => "Douglas ( MN )",
			"locale" => "MN | Douglas"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Faribault",
			"label" => "Faribault ( MN )",
			"locale" => "MN | Faribault"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Fillmore",
			"label" => "Fillmore ( MN )",
			"locale" => "MN | Fillmore"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Freeborn",
			"label" => "Freeborn ( MN )",
			"locale" => "MN | Freeborn"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Goodhue",
			"label" => "Goodhue ( MN )",
			"locale" => "MN | Goodhue"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Grant",
			"label" => "Grant ( MN )",
			"locale" => "MN | Grant"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Hennepin",
			"label" => "Hennepin ( MN )",
			"locale" => "MN | Hennepin"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Houston",
			"label" => "Houston ( MN )",
			"locale" => "MN | Houston"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Hubbard",
			"label" => "Hubbard ( MN )",
			"locale" => "MN | Hubbard"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Isanti",
			"label" => "Isanti ( MN )",
			"locale" => "MN | Isanti"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Itasca",
			"label" => "Itasca ( MN )",
			"locale" => "MN | Itasca"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Jackson",
			"label" => "Jackson ( MN )",
			"locale" => "MN | Jackson"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Kanabec",
			"label" => "Kanabec ( MN )",
			"locale" => "MN | Kanabec"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Kandiyohi",
			"label" => "Kandiyohi ( MN )",
			"locale" => "MN | Kandiyohi"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Kittson",
			"label" => "Kittson ( MN )",
			"locale" => "MN | Kittson"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Koochiching",
			"label" => "Koochiching ( MN )",
			"locale" => "MN | Koochiching"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Lac qui Parle",
			"label" => "Lac qui Parle ( MN )",
			"locale" => "MN | Lac qui Parle"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Lake",
			"label" => "Lake ( MN )",
			"locale" => "MN | Lake"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Lake of the Woods",
			"label" => "Lake of the Woods ( MN )",
			"locale" => "MN | Lake of the Woods"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Le Sueur",
			"label" => "Le Sueur ( MN )",
			"locale" => "MN | Le Sueur"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Lincoln",
			"label" => "Lincoln ( MN )",
			"locale" => "MN | Lincoln"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Lyon",
			"label" => "Lyon ( MN )",
			"locale" => "MN | Lyon"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Mahnomen",
			"label" => "Mahnomen ( MN )",
			"locale" => "MN | Mahnomen"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Marshall",
			"label" => "Marshall ( MN )",
			"locale" => "MN | Marshall"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Martin",
			"label" => "Martin ( MN )",
			"locale" => "MN | Martin"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "McLeod",
			"label" => "McLeod ( MN )",
			"locale" => "MN | McLeod"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Meeker",
			"label" => "Meeker ( MN )",
			"locale" => "MN | Meeker"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Mille Lacs",
			"label" => "Mille Lacs ( MN )",
			"locale" => "MN | Mille Lacs"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Morrison",
			"label" => "Morrison ( MN )",
			"locale" => "MN | Morrison"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Mower",
			"label" => "Mower ( MN )",
			"locale" => "MN | Mower"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Murray",
			"label" => "Murray ( MN )",
			"locale" => "MN | Murray"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Nicollet",
			"label" => "Nicollet ( MN )",
			"locale" => "MN | Nicollet"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Nobles",
			"label" => "Nobles ( MN )",
			"locale" => "MN | Nobles"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Norman",
			"label" => "Norman ( MN )",
			"locale" => "MN | Norman"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Olmsted",
			"label" => "Olmsted ( MN )",
			"locale" => "MN | Olmsted"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Otter Tail",
			"label" => "Otter Tail ( MN )",
			"locale" => "MN | Otter Tail"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Pennington",
			"label" => "Pennington ( MN )",
			"locale" => "MN | Pennington"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Pine",
			"label" => "Pine ( MN )",
			"locale" => "MN | Pine"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Pipestone",
			"label" => "Pipestone ( MN )",
			"locale" => "MN | Pipestone"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Polk",
			"label" => "Polk ( MN )",
			"locale" => "MN | Polk"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Pope",
			"label" => "Pope ( MN )",
			"locale" => "MN | Pope"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Ramsey",
			"label" => "Ramsey ( MN )",
			"locale" => "MN | Ramsey"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Red Lake",
			"label" => "Red Lake ( MN )",
			"locale" => "MN | Red Lake"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Redwood",
			"label" => "Redwood ( MN )",
			"locale" => "MN | Redwood"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Renville",
			"label" => "Renville ( MN )",
			"locale" => "MN | Renville"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Rice",
			"label" => "Rice ( MN )",
			"locale" => "MN | Rice"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Rock",
			"label" => "Rock ( MN )",
			"locale" => "MN | Rock"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Roseau",
			"label" => "Roseau ( MN )",
			"locale" => "MN | Roseau"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Scott",
			"label" => "Scott ( MN )",
			"locale" => "MN | Scott"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Sherburne",
			"label" => "Sherburne ( MN )",
			"locale" => "MN | Sherburne"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Sibley",
			"label" => "Sibley ( MN )",
			"locale" => "MN | Sibley"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "St. Louis",
			"label" => "St. Louis ( MN )",
			"locale" => "MN | St. Louis"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Stearns",
			"label" => "Stearns ( MN )",
			"locale" => "MN | Stearns"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Steele",
			"label" => "Steele ( MN )",
			"locale" => "MN | Steele"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Stevens",
			"label" => "Stevens ( MN )",
			"locale" => "MN | Stevens"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Swift",
			"label" => "Swift ( MN )",
			"locale" => "MN | Swift"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Todd",
			"label" => "Todd ( MN )",
			"locale" => "MN | Todd"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Traverse",
			"label" => "Traverse ( MN )",
			"locale" => "MN | Traverse"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Wabasha",
			"label" => "Wabasha ( MN )",
			"locale" => "MN | Wabasha"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Wadena",
			"label" => "Wadena ( MN )",
			"locale" => "MN | Wadena"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Waseca",
			"label" => "Waseca ( MN )",
			"locale" => "MN | Waseca"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( MN )",
			"locale" => "MN | Washington"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Watonwan",
			"label" => "Watonwan ( MN )",
			"locale" => "MN | Watonwan"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Wilkin",
			"label" => "Wilkin ( MN )",
			"locale" => "MN | Wilkin"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Winona",
			"label" => "Winona ( MN )",
			"locale" => "MN | Winona"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Wright",
			"label" => "Wright ( MN )",
			"locale" => "MN | Wright"
		]);
		County::create([
			"state_id" => 24,
			"location_id" => 0,
			"county" => "Yellow Medicine",
			"label" => "Yellow Medicine ( MN )",
			"locale" => "MN | Yellow Medicine"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Adams",
			"label" => "Adams ( MS )",
			"locale" => "MS | Adams"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Alcorn",
			"label" => "Alcorn ( MS )",
			"locale" => "MS | Alcorn"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Amite",
			"label" => "Amite ( MS )",
			"locale" => "MS | Amite"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Attala",
			"label" => "Attala ( MS )",
			"locale" => "MS | Attala"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Benton",
			"label" => "Benton ( MS )",
			"locale" => "MS | Benton"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Bolivar",
			"label" => "Bolivar ( MS )",
			"locale" => "MS | Bolivar"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Calhoun",
			"label" => "Calhoun ( MS )",
			"locale" => "MS | Calhoun"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Carroll",
			"label" => "Carroll ( MS )",
			"locale" => "MS | Carroll"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Chickasaw",
			"label" => "Chickasaw ( MS )",
			"locale" => "MS | Chickasaw"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Choctaw",
			"label" => "Choctaw ( MS )",
			"locale" => "MS | Choctaw"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Claiborne",
			"label" => "Claiborne ( MS )",
			"locale" => "MS | Claiborne"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Clarke",
			"label" => "Clarke ( MS )",
			"locale" => "MS | Clarke"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Clay",
			"label" => "Clay ( MS )",
			"locale" => "MS | Clay"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Coahoma",
			"label" => "Coahoma ( MS )",
			"locale" => "MS | Coahoma"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Copiah",
			"label" => "Copiah ( MS )",
			"locale" => "MS | Copiah"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Covington",
			"label" => "Covington ( MS )",
			"locale" => "MS | Covington"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "DeSoto",
			"label" => "DeSoto ( MS )",
			"locale" => "MS | DeSoto"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Forrest",
			"label" => "Forrest ( MS )",
			"locale" => "MS | Forrest"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Franklin",
			"label" => "Franklin ( MS )",
			"locale" => "MS | Franklin"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "George",
			"label" => "George ( MS )",
			"locale" => "MS | George"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Greene",
			"label" => "Greene ( MS )",
			"locale" => "MS | Greene"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Grenada",
			"label" => "Grenada ( MS )",
			"locale" => "MS | Grenada"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Hancock",
			"label" => "Hancock ( MS )",
			"locale" => "MS | Hancock"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Harrison",
			"label" => "Harrison ( MS )",
			"locale" => "MS | Harrison"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Hinds",
			"label" => "Hinds ( MS )",
			"locale" => "MS | Hinds"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Holmes",
			"label" => "Holmes ( MS )",
			"locale" => "MS | Holmes"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Humphreys",
			"label" => "Humphreys ( MS )",
			"locale" => "MS | Humphreys"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Issaquena",
			"label" => "Issaquena ( MS )",
			"locale" => "MS | Issaquena"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Itawamba",
			"label" => "Itawamba ( MS )",
			"locale" => "MS | Itawamba"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Jackson",
			"label" => "Jackson ( MS )",
			"locale" => "MS | Jackson"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Jasper",
			"label" => "Jasper ( MS )",
			"locale" => "MS | Jasper"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Jefferson",
			"label" => "Jefferson ( MS )",
			"locale" => "MS | Jefferson"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Jefferson Davis",
			"label" => "Jefferson Davis ( MS )",
			"locale" => "MS | Jefferson Davis"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Jones",
			"label" => "Jones ( MS )",
			"locale" => "MS | Jones"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Kemper",
			"label" => "Kemper ( MS )",
			"locale" => "MS | Kemper"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Lafayette",
			"label" => "Lafayette ( MS )",
			"locale" => "MS | Lafayette"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Lamar",
			"label" => "Lamar ( MS )",
			"locale" => "MS | Lamar"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Lauderdale",
			"label" => "Lauderdale ( MS )",
			"locale" => "MS | Lauderdale"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Lawrence",
			"label" => "Lawrence ( MS )",
			"locale" => "MS | Lawrence"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Leake",
			"label" => "Leake ( MS )",
			"locale" => "MS | Leake"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Lee",
			"label" => "Lee ( MS )",
			"locale" => "MS | Lee"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Leflore",
			"label" => "Leflore ( MS )",
			"locale" => "MS | Leflore"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Lincoln",
			"label" => "Lincoln ( MS )",
			"locale" => "MS | Lincoln"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Lowndes",
			"label" => "Lowndes ( MS )",
			"locale" => "MS | Lowndes"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Madison",
			"label" => "Madison ( MS )",
			"locale" => "MS | Madison"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Marion",
			"label" => "Marion ( MS )",
			"locale" => "MS | Marion"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Marshall",
			"label" => "Marshall ( MS )",
			"locale" => "MS | Marshall"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Monroe",
			"label" => "Monroe ( MS )",
			"locale" => "MS | Monroe"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Montgomery",
			"label" => "Montgomery ( MS )",
			"locale" => "MS | Montgomery"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Neshoba",
			"label" => "Neshoba ( MS )",
			"locale" => "MS | Neshoba"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Newton",
			"label" => "Newton ( MS )",
			"locale" => "MS | Newton"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Noxubee",
			"label" => "Noxubee ( MS )",
			"locale" => "MS | Noxubee"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Oktibbeha",
			"label" => "Oktibbeha ( MS )",
			"locale" => "MS | Oktibbeha"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Panola",
			"label" => "Panola ( MS )",
			"locale" => "MS | Panola"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Pearl River",
			"label" => "Pearl River ( MS )",
			"locale" => "MS | Pearl River"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Perry",
			"label" => "Perry ( MS )",
			"locale" => "MS | Perry"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Pike",
			"label" => "Pike ( MS )",
			"locale" => "MS | Pike"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Pontotoc",
			"label" => "Pontotoc ( MS )",
			"locale" => "MS | Pontotoc"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Prentiss",
			"label" => "Prentiss ( MS )",
			"locale" => "MS | Prentiss"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Quitman",
			"label" => "Quitman ( MS )",
			"locale" => "MS | Quitman"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Rankin",
			"label" => "Rankin ( MS )",
			"locale" => "MS | Rankin"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Scott",
			"label" => "Scott ( MS )",
			"locale" => "MS | Scott"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Sharkey",
			"label" => "Sharkey ( MS )",
			"locale" => "MS | Sharkey"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Simpson",
			"label" => "Simpson ( MS )",
			"locale" => "MS | Simpson"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Smith",
			"label" => "Smith ( MS )",
			"locale" => "MS | Smith"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Stone",
			"label" => "Stone ( MS )",
			"locale" => "MS | Stone"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Sunflower",
			"label" => "Sunflower ( MS )",
			"locale" => "MS | Sunflower"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Tallahatchie",
			"label" => "Tallahatchie ( MS )",
			"locale" => "MS | Tallahatchie"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Tate",
			"label" => "Tate ( MS )",
			"locale" => "MS | Tate"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Tippah",
			"label" => "Tippah ( MS )",
			"locale" => "MS | Tippah"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Tishomingo",
			"label" => "Tishomingo ( MS )",
			"locale" => "MS | Tishomingo"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Tunica",
			"label" => "Tunica ( MS )",
			"locale" => "MS | Tunica"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Union",
			"label" => "Union ( MS )",
			"locale" => "MS | Union"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Walthall",
			"label" => "Walthall ( MS )",
			"locale" => "MS | Walthall"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Warren",
			"label" => "Warren ( MS )",
			"locale" => "MS | Warren"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Washington",
			"label" => "Washington ( MS )",
			"locale" => "MS | Washington"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Wayne",
			"label" => "Wayne ( MS )",
			"locale" => "MS | Wayne"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Webster",
			"label" => "Webster ( MS )",
			"locale" => "MS | Webster"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Wilkinson",
			"label" => "Wilkinson ( MS )",
			"locale" => "MS | Wilkinson"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Winston",
			"label" => "Winston ( MS )",
			"locale" => "MS | Winston"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Yalobusha",
			"label" => "Yalobusha ( MS )",
			"locale" => "MS | Yalobusha"
		]);
		County::create([
			"state_id" => 25,
			"location_id" => 2,
			"county" => "Yazoo",
			"label" => "Yazoo ( MS )",
			"locale" => "MS | Yazoo"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Adair",
			"label" => "Adair ( MO )",
			"locale" => "MO | Adair"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Andrew",
			"label" => "Andrew ( MO )",
			"locale" => "MO | Andrew"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Atchison",
			"label" => "Atchison ( MO )",
			"locale" => "MO | Atchison"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Audrain",
			"label" => "Audrain ( MO )",
			"locale" => "MO | Audrain"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Barry",
			"label" => "Barry ( MO )",
			"locale" => "MO | Barry"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Barton",
			"label" => "Barton ( MO )",
			"locale" => "MO | Barton"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Bates",
			"label" => "Bates ( MO )",
			"locale" => "MO | Bates"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Benton",
			"label" => "Benton ( MO )",
			"locale" => "MO | Benton"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Bollinger",
			"label" => "Bollinger ( MO )",
			"locale" => "MO | Bollinger"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Boone",
			"label" => "Boone ( MO )",
			"locale" => "MO | Boone"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Buchanan",
			"label" => "Buchanan ( MO )",
			"locale" => "MO | Buchanan"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Butler",
			"label" => "Butler ( MO )",
			"locale" => "MO | Butler"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Caldwell",
			"label" => "Caldwell ( MO )",
			"locale" => "MO | Caldwell"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Callaway",
			"label" => "Callaway ( MO )",
			"locale" => "MO | Callaway"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Camden",
			"label" => "Camden ( MO )",
			"locale" => "MO | Camden"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Cape Girardeau",
			"label" => "Cape Girardeau ( MO )",
			"locale" => "MO | Cape Girardeau"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Carroll",
			"label" => "Carroll ( MO )",
			"locale" => "MO | Carroll"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Carter",
			"label" => "Carter ( MO )",
			"locale" => "MO | Carter"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Cass",
			"label" => "Cass ( MO )",
			"locale" => "MO | Cass"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Cedar",
			"label" => "Cedar ( MO )",
			"locale" => "MO | Cedar"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Chariton",
			"label" => "Chariton ( MO )",
			"locale" => "MO | Chariton"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Christian",
			"label" => "Christian ( MO )",
			"locale" => "MO | Christian"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Clark",
			"label" => "Clark ( MO )",
			"locale" => "MO | Clark"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Clay",
			"label" => "Clay ( MO )",
			"locale" => "MO | Clay"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Clinton",
			"label" => "Clinton ( MO )",
			"locale" => "MO | Clinton"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Cole",
			"label" => "Cole ( MO )",
			"locale" => "MO | Cole"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Cooper",
			"label" => "Cooper ( MO )",
			"locale" => "MO | Cooper"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Crawford",
			"label" => "Crawford ( MO )",
			"locale" => "MO | Crawford"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Dade",
			"label" => "Dade ( MO )",
			"locale" => "MO | Dade"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Dallas",
			"label" => "Dallas ( MO )",
			"locale" => "MO | Dallas"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Daviess",
			"label" => "Daviess ( MO )",
			"locale" => "MO | Daviess"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "DeKalb",
			"label" => "DeKalb ( MO )",
			"locale" => "MO | DeKalb"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Dent",
			"label" => "Dent ( MO )",
			"locale" => "MO | Dent"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Douglas",
			"label" => "Douglas ( MO )",
			"locale" => "MO | Douglas"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Dunklin",
			"label" => "Dunklin ( MO )",
			"locale" => "MO | Dunklin"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Franklin",
			"label" => "Franklin ( MO )",
			"locale" => "MO | Franklin"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Gasconade",
			"label" => "Gasconade ( MO )",
			"locale" => "MO | Gasconade"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Gentry",
			"label" => "Gentry ( MO )",
			"locale" => "MO | Gentry"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Greene",
			"label" => "Greene ( MO )",
			"locale" => "MO | Greene"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Grundy",
			"label" => "Grundy ( MO )",
			"locale" => "MO | Grundy"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Harrison",
			"label" => "Harrison ( MO )",
			"locale" => "MO | Harrison"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Henry",
			"label" => "Henry ( MO )",
			"locale" => "MO | Henry"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Hickory",
			"label" => "Hickory ( MO )",
			"locale" => "MO | Hickory"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Holt",
			"label" => "Holt ( MO )",
			"locale" => "MO | Holt"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Howard",
			"label" => "Howard ( MO )",
			"locale" => "MO | Howard"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Howell",
			"label" => "Howell ( MO )",
			"locale" => "MO | Howell"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Iron",
			"label" => "Iron ( MO )",
			"locale" => "MO | Iron"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Jackson",
			"label" => "Jackson ( MO )",
			"locale" => "MO | Jackson"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Jasper",
			"label" => "Jasper ( MO )",
			"locale" => "MO | Jasper"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Jefferson",
			"label" => "Jefferson ( MO )",
			"locale" => "MO | Jefferson"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Johnson",
			"label" => "Johnson ( MO )",
			"locale" => "MO | Johnson"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Knox",
			"label" => "Knox ( MO )",
			"locale" => "MO | Knox"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Laclede",
			"label" => "Laclede ( MO )",
			"locale" => "MO | Laclede"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Lafayette",
			"label" => "Lafayette ( MO )",
			"locale" => "MO | Lafayette"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Lawrence",
			"label" => "Lawrence ( MO )",
			"locale" => "MO | Lawrence"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Lewis",
			"label" => "Lewis ( MO )",
			"locale" => "MO | Lewis"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Lincoln",
			"label" => "Lincoln ( MO )",
			"locale" => "MO | Lincoln"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Linn",
			"label" => "Linn ( MO )",
			"locale" => "MO | Linn"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Livingston",
			"label" => "Livingston ( MO )",
			"locale" => "MO | Livingston"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Macon",
			"label" => "Macon ( MO )",
			"locale" => "MO | Macon"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Madison",
			"label" => "Madison ( MO )",
			"locale" => "MO | Madison"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Maries",
			"label" => "Maries ( MO )",
			"locale" => "MO | Maries"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Marion",
			"label" => "Marion ( MO )",
			"locale" => "MO | Marion"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "McDonald",
			"label" => "McDonald ( MO )",
			"locale" => "MO | McDonald"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Mercer",
			"label" => "Mercer ( MO )",
			"locale" => "MO | Mercer"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Miller",
			"label" => "Miller ( MO )",
			"locale" => "MO | Miller"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Mississippi",
			"label" => "Mississippi ( MO )",
			"locale" => "MO | Mississippi"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Moniteau",
			"label" => "Moniteau ( MO )",
			"locale" => "MO | Moniteau"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Monroe",
			"label" => "Monroe ( MO )",
			"locale" => "MO | Monroe"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Montgomery",
			"label" => "Montgomery ( MO )",
			"locale" => "MO | Montgomery"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Morgan",
			"label" => "Morgan ( MO )",
			"locale" => "MO | Morgan"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "New Madrid",
			"label" => "New Madrid ( MO )",
			"locale" => "MO | New Madrid"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Newton",
			"label" => "Newton ( MO )",
			"locale" => "MO | Newton"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Nodaway",
			"label" => "Nodaway ( MO )",
			"locale" => "MO | Nodaway"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Oregon",
			"label" => "Oregon ( MO )",
			"locale" => "MO | Oregon"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Osage",
			"label" => "Osage ( MO )",
			"locale" => "MO | Osage"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Ozark",
			"label" => "Ozark ( MO )",
			"locale" => "MO | Ozark"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Pemiscot",
			"label" => "Pemiscot ( MO )",
			"locale" => "MO | Pemiscot"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Perry",
			"label" => "Perry ( MO )",
			"locale" => "MO | Perry"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Pettis",
			"label" => "Pettis ( MO )",
			"locale" => "MO | Pettis"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Phelps",
			"label" => "Phelps ( MO )",
			"locale" => "MO | Phelps"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Pike",
			"label" => "Pike ( MO )",
			"locale" => "MO | Pike"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Platte",
			"label" => "Platte ( MO )",
			"locale" => "MO | Platte"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Polk",
			"label" => "Polk ( MO )",
			"locale" => "MO | Polk"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Pulaski",
			"label" => "Pulaski ( MO )",
			"locale" => "MO | Pulaski"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Putnam",
			"label" => "Putnam ( MO )",
			"locale" => "MO | Putnam"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Ralls",
			"label" => "Ralls ( MO )",
			"locale" => "MO | Ralls"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Randolph",
			"label" => "Randolph ( MO )",
			"locale" => "MO | Randolph"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Ray",
			"label" => "Ray ( MO )",
			"locale" => "MO | Ray"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Reynolds",
			"label" => "Reynolds ( MO )",
			"locale" => "MO | Reynolds"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Ripley",
			"label" => "Ripley ( MO )",
			"locale" => "MO | Ripley"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Saline",
			"label" => "Saline ( MO )",
			"locale" => "MO | Saline"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Schuyler",
			"label" => "Schuyler ( MO )",
			"locale" => "MO | Schuyler"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Scotland",
			"label" => "Scotland ( MO )",
			"locale" => "MO | Scotland"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Scott",
			"label" => "Scott ( MO )",
			"locale" => "MO | Scott"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Shannon",
			"label" => "Shannon ( MO )",
			"locale" => "MO | Shannon"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Shelby",
			"label" => "Shelby ( MO )",
			"locale" => "MO | Shelby"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "St. Charles",
			"label" => "St. Charles ( MO )",
			"locale" => "MO | St. Charles"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "St. Clair",
			"label" => "St. Clair ( MO )",
			"locale" => "MO | St. Clair"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "St. Francois",
			"label" => "St. Francois ( MO )",
			"locale" => "MO | St. Francois"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "St. Louis",
			"label" => "St. Louis ( MO )",
			"locale" => "MO | St. Louis"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Ste. Genevieve",
			"label" => "Ste. Genevieve ( MO )",
			"locale" => "MO | Ste. Genevieve"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Stoddard",
			"label" => "Stoddard ( MO )",
			"locale" => "MO | Stoddard"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Stone",
			"label" => "Stone ( MO )",
			"locale" => "MO | Stone"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Sullivan",
			"label" => "Sullivan ( MO )",
			"locale" => "MO | Sullivan"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Taney",
			"label" => "Taney ( MO )",
			"locale" => "MO | Taney"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Texas",
			"label" => "Texas ( MO )",
			"locale" => "MO | Texas"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Vernon",
			"label" => "Vernon ( MO )",
			"locale" => "MO | Vernon"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Warren",
			"label" => "Warren ( MO )",
			"locale" => "MO | Warren"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( MO )",
			"locale" => "MO | Washington"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Wayne",
			"label" => "Wayne ( MO )",
			"locale" => "MO | Wayne"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Webster",
			"label" => "Webster ( MO )",
			"locale" => "MO | Webster"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Worth",
			"label" => "Worth ( MO )",
			"locale" => "MO | Worth"
		]);
		County::create([
			"state_id" => 26,
			"location_id" => 0,
			"county" => "Wright",
			"label" => "Wright ( MO )",
			"locale" => "MO | Wright"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Beaverhead",
			"label" => "Beaverhead ( MT )",
			"locale" => "MT | Beaverhead"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Big Horn",
			"label" => "Big Horn ( MT )",
			"locale" => "MT | Big Horn"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Blaine",
			"label" => "Blaine ( MT )",
			"locale" => "MT | Blaine"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Broadwater",
			"label" => "Broadwater ( MT )",
			"locale" => "MT | Broadwater"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Carbon",
			"label" => "Carbon ( MT )",
			"locale" => "MT | Carbon"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Carter",
			"label" => "Carter ( MT )",
			"locale" => "MT | Carter"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Cascade",
			"label" => "Cascade ( MT )",
			"locale" => "MT | Cascade"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Chouteau",
			"label" => "Chouteau ( MT )",
			"locale" => "MT | Chouteau"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Custer",
			"label" => "Custer ( MT )",
			"locale" => "MT | Custer"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Daniels",
			"label" => "Daniels ( MT )",
			"locale" => "MT | Daniels"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Dawson",
			"label" => "Dawson ( MT )",
			"locale" => "MT | Dawson"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Fallon",
			"label" => "Fallon ( MT )",
			"locale" => "MT | Fallon"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Fergus",
			"label" => "Fergus ( MT )",
			"locale" => "MT | Fergus"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Flathead",
			"label" => "Flathead ( MT )",
			"locale" => "MT | Flathead"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Gallatin",
			"label" => "Gallatin ( MT )",
			"locale" => "MT | Gallatin"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Garfield",
			"label" => "Garfield ( MT )",
			"locale" => "MT | Garfield"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Glacier",
			"label" => "Glacier ( MT )",
			"locale" => "MT | Glacier"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Golden Valley",
			"label" => "Golden Valley ( MT )",
			"locale" => "MT | Golden Valley"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Granite",
			"label" => "Granite ( MT )",
			"locale" => "MT | Granite"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Hill",
			"label" => "Hill ( MT )",
			"locale" => "MT | Hill"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Jefferson",
			"label" => "Jefferson ( MT )",
			"locale" => "MT | Jefferson"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Judith Basin",
			"label" => "Judith Basin ( MT )",
			"locale" => "MT | Judith Basin"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Lake",
			"label" => "Lake ( MT )",
			"locale" => "MT | Lake"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Lewis and Clark",
			"label" => "Lewis and Clark ( MT )",
			"locale" => "MT | Lewis and Clark"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Liberty",
			"label" => "Liberty ( MT )",
			"locale" => "MT | Liberty"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Lincoln",
			"label" => "Lincoln ( MT )",
			"locale" => "MT | Lincoln"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Madison",
			"label" => "Madison ( MT )",
			"locale" => "MT | Madison"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "McCone",
			"label" => "McCone ( MT )",
			"locale" => "MT | McCone"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Meagher",
			"label" => "Meagher ( MT )",
			"locale" => "MT | Meagher"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Mineral",
			"label" => "Mineral ( MT )",
			"locale" => "MT | Mineral"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Missoula",
			"label" => "Missoula ( MT )",
			"locale" => "MT | Missoula"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Musselshell",
			"label" => "Musselshell ( MT )",
			"locale" => "MT | Musselshell"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Park",
			"label" => "Park ( MT )",
			"locale" => "MT | Park"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Petroleum",
			"label" => "Petroleum ( MT )",
			"locale" => "MT | Petroleum"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Phillips",
			"label" => "Phillips ( MT )",
			"locale" => "MT | Phillips"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Pondera",
			"label" => "Pondera ( MT )",
			"locale" => "MT | Pondera"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Powder River",
			"label" => "Powder River ( MT )",
			"locale" => "MT | Powder River"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Powell",
			"label" => "Powell ( MT )",
			"locale" => "MT | Powell"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Prairie",
			"label" => "Prairie ( MT )",
			"locale" => "MT | Prairie"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Ravalli",
			"label" => "Ravalli ( MT )",
			"locale" => "MT | Ravalli"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Richland",
			"label" => "Richland ( MT )",
			"locale" => "MT | Richland"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Roosevelt",
			"label" => "Roosevelt ( MT )",
			"locale" => "MT | Roosevelt"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Rosebud",
			"label" => "Rosebud ( MT )",
			"locale" => "MT | Rosebud"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Sanders",
			"label" => "Sanders ( MT )",
			"locale" => "MT | Sanders"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Sheridan",
			"label" => "Sheridan ( MT )",
			"locale" => "MT | Sheridan"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Stillwater",
			"label" => "Stillwater ( MT )",
			"locale" => "MT | Stillwater"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Sweet Grass",
			"label" => "Sweet Grass ( MT )",
			"locale" => "MT | Sweet Grass"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Teton",
			"label" => "Teton ( MT )",
			"locale" => "MT | Teton"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Toole",
			"label" => "Toole ( MT )",
			"locale" => "MT | Toole"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Treasure",
			"label" => "Treasure ( MT )",
			"locale" => "MT | Treasure"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Valley",
			"label" => "Valley ( MT )",
			"locale" => "MT | Valley"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Wheatland",
			"label" => "Wheatland ( MT )",
			"locale" => "MT | Wheatland"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Wibaux",
			"label" => "Wibaux ( MT )",
			"locale" => "MT | Wibaux"
		]);
		County::create([
			"state_id" => 27,
			"location_id" => 0,
			"county" => "Yellowstone",
			"label" => "Yellowstone ( MT )",
			"locale" => "MT | Yellowstone"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Adams",
			"label" => "Adams ( NE )",
			"locale" => "NE | Adams"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Antelope",
			"label" => "Antelope ( NE )",
			"locale" => "NE | Antelope"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Arthur",
			"label" => "Arthur ( NE )",
			"locale" => "NE | Arthur"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Banner",
			"label" => "Banner ( NE )",
			"locale" => "NE | Banner"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Blaine",
			"label" => "Blaine ( NE )",
			"locale" => "NE | Blaine"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Boone",
			"label" => "Boone ( NE )",
			"locale" => "NE | Boone"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Box Butte",
			"label" => "Box Butte ( NE )",
			"locale" => "NE | Box Butte"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Boyd",
			"label" => "Boyd ( NE )",
			"locale" => "NE | Boyd"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Brown",
			"label" => "Brown ( NE )",
			"locale" => "NE | Brown"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Buffalo",
			"label" => "Buffalo ( NE )",
			"locale" => "NE | Buffalo"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Burt",
			"label" => "Burt ( NE )",
			"locale" => "NE | Burt"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Butler",
			"label" => "Butler ( NE )",
			"locale" => "NE | Butler"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Cass",
			"label" => "Cass ( NE )",
			"locale" => "NE | Cass"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Cedar",
			"label" => "Cedar ( NE )",
			"locale" => "NE | Cedar"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Chase",
			"label" => "Chase ( NE )",
			"locale" => "NE | Chase"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Cherry",
			"label" => "Cherry ( NE )",
			"locale" => "NE | Cherry"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Cheyenne",
			"label" => "Cheyenne ( NE )",
			"locale" => "NE | Cheyenne"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Clay",
			"label" => "Clay ( NE )",
			"locale" => "NE | Clay"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Colfax",
			"label" => "Colfax ( NE )",
			"locale" => "NE | Colfax"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Cuming",
			"label" => "Cuming ( NE )",
			"locale" => "NE | Cuming"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Custer",
			"label" => "Custer ( NE )",
			"locale" => "NE | Custer"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Dakota",
			"label" => "Dakota ( NE )",
			"locale" => "NE | Dakota"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Dawes",
			"label" => "Dawes ( NE )",
			"locale" => "NE | Dawes"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Dawson",
			"label" => "Dawson ( NE )",
			"locale" => "NE | Dawson"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Deuel",
			"label" => "Deuel ( NE )",
			"locale" => "NE | Deuel"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Dixon",
			"label" => "Dixon ( NE )",
			"locale" => "NE | Dixon"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Dodge",
			"label" => "Dodge ( NE )",
			"locale" => "NE | Dodge"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Douglas",
			"label" => "Douglas ( NE )",
			"locale" => "NE | Douglas"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Dundy",
			"label" => "Dundy ( NE )",
			"locale" => "NE | Dundy"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Fillmore",
			"label" => "Fillmore ( NE )",
			"locale" => "NE | Fillmore"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Franklin",
			"label" => "Franklin ( NE )",
			"locale" => "NE | Franklin"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Frontier",
			"label" => "Frontier ( NE )",
			"locale" => "NE | Frontier"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Furnas",
			"label" => "Furnas ( NE )",
			"locale" => "NE | Furnas"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Gage",
			"label" => "Gage ( NE )",
			"locale" => "NE | Gage"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Garden",
			"label" => "Garden ( NE )",
			"locale" => "NE | Garden"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Garfield",
			"label" => "Garfield ( NE )",
			"locale" => "NE | Garfield"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Gosper",
			"label" => "Gosper ( NE )",
			"locale" => "NE | Gosper"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Grant",
			"label" => "Grant ( NE )",
			"locale" => "NE | Grant"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Greeley",
			"label" => "Greeley ( NE )",
			"locale" => "NE | Greeley"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Hall",
			"label" => "Hall ( NE )",
			"locale" => "NE | Hall"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Hamilton",
			"label" => "Hamilton ( NE )",
			"locale" => "NE | Hamilton"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Harlan",
			"label" => "Harlan ( NE )",
			"locale" => "NE | Harlan"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Hayes",
			"label" => "Hayes ( NE )",
			"locale" => "NE | Hayes"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Hitchcock",
			"label" => "Hitchcock ( NE )",
			"locale" => "NE | Hitchcock"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Holt",
			"label" => "Holt ( NE )",
			"locale" => "NE | Holt"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Hooker",
			"label" => "Hooker ( NE )",
			"locale" => "NE | Hooker"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Howard",
			"label" => "Howard ( NE )",
			"locale" => "NE | Howard"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Jefferson",
			"label" => "Jefferson ( NE )",
			"locale" => "NE | Jefferson"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Johnson",
			"label" => "Johnson ( NE )",
			"locale" => "NE | Johnson"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Kearney",
			"label" => "Kearney ( NE )",
			"locale" => "NE | Kearney"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Keith",
			"label" => "Keith ( NE )",
			"locale" => "NE | Keith"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Keya Paha",
			"label" => "Keya Paha ( NE )",
			"locale" => "NE | Keya Paha"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Kimball",
			"label" => "Kimball ( NE )",
			"locale" => "NE | Kimball"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Knox",
			"label" => "Knox ( NE )",
			"locale" => "NE | Knox"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Lancaster",
			"label" => "Lancaster ( NE )",
			"locale" => "NE | Lancaster"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Lincoln",
			"label" => "Lincoln ( NE )",
			"locale" => "NE | Lincoln"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Logan",
			"label" => "Logan ( NE )",
			"locale" => "NE | Logan"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Loup",
			"label" => "Loup ( NE )",
			"locale" => "NE | Loup"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Madison",
			"label" => "Madison ( NE )",
			"locale" => "NE | Madison"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "McPherson",
			"label" => "McPherson ( NE )",
			"locale" => "NE | McPherson"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Merrick",
			"label" => "Merrick ( NE )",
			"locale" => "NE | Merrick"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Morrill",
			"label" => "Morrill ( NE )",
			"locale" => "NE | Morrill"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Nance",
			"label" => "Nance ( NE )",
			"locale" => "NE | Nance"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Nemaha",
			"label" => "Nemaha ( NE )",
			"locale" => "NE | Nemaha"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Nuckolls",
			"label" => "Nuckolls ( NE )",
			"locale" => "NE | Nuckolls"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Otoe",
			"label" => "Otoe ( NE )",
			"locale" => "NE | Otoe"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Pawnee",
			"label" => "Pawnee ( NE )",
			"locale" => "NE | Pawnee"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Perkins",
			"label" => "Perkins ( NE )",
			"locale" => "NE | Perkins"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Phelps",
			"label" => "Phelps ( NE )",
			"locale" => "NE | Phelps"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Pierce",
			"label" => "Pierce ( NE )",
			"locale" => "NE | Pierce"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Platte",
			"label" => "Platte ( NE )",
			"locale" => "NE | Platte"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Polk",
			"label" => "Polk ( NE )",
			"locale" => "NE | Polk"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Red Willow",
			"label" => "Red Willow ( NE )",
			"locale" => "NE | Red Willow"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Richardson",
			"label" => "Richardson ( NE )",
			"locale" => "NE | Richardson"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Rock",
			"label" => "Rock ( NE )",
			"locale" => "NE | Rock"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Saline",
			"label" => "Saline ( NE )",
			"locale" => "NE | Saline"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Sarpy",
			"label" => "Sarpy ( NE )",
			"locale" => "NE | Sarpy"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Saunders",
			"label" => "Saunders ( NE )",
			"locale" => "NE | Saunders"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Scotts Bluff",
			"label" => "Scotts Bluff ( NE )",
			"locale" => "NE | Scotts Bluff"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Seward",
			"label" => "Seward ( NE )",
			"locale" => "NE | Seward"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Sheridan",
			"label" => "Sheridan ( NE )",
			"locale" => "NE | Sheridan"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Sherman",
			"label" => "Sherman ( NE )",
			"locale" => "NE | Sherman"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Sioux",
			"label" => "Sioux ( NE )",
			"locale" => "NE | Sioux"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Stanton",
			"label" => "Stanton ( NE )",
			"locale" => "NE | Stanton"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Thayer",
			"label" => "Thayer ( NE )",
			"locale" => "NE | Thayer"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Thomas",
			"label" => "Thomas ( NE )",
			"locale" => "NE | Thomas"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Thurston",
			"label" => "Thurston ( NE )",
			"locale" => "NE | Thurston"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Valley",
			"label" => "Valley ( NE )",
			"locale" => "NE | Valley"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( NE )",
			"locale" => "NE | Washington"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Wayne",
			"label" => "Wayne ( NE )",
			"locale" => "NE | Wayne"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Webster",
			"label" => "Webster ( NE )",
			"locale" => "NE | Webster"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "Wheeler",
			"label" => "Wheeler ( NE )",
			"locale" => "NE | Wheeler"
		]);
		County::create([
			"state_id" => 28,
			"location_id" => 0,
			"county" => "York",
			"label" => "York ( NE )",
			"locale" => "NE | York"
		]);
		County::create([
			"state_id" => 29,
			"location_id" => 0,
			"county" => "Churchill",
			"label" => "Churchill ( NV )",
			"locale" => "NV | Churchill"
		]);
		County::create([
			"state_id" => 29,
			"location_id" => 0,
			"county" => "Clark",
			"label" => "Clark ( NV )",
			"locale" => "NV | Clark"
		]);
		County::create([
			"state_id" => 29,
			"location_id" => 0,
			"county" => "Douglas",
			"label" => "Douglas ( NV )",
			"locale" => "NV | Douglas"
		]);
		County::create([
			"state_id" => 29,
			"location_id" => 0,
			"county" => "Elko",
			"label" => "Elko ( NV )",
			"locale" => "NV | Elko"
		]);
		County::create([
			"state_id" => 29,
			"location_id" => 0,
			"county" => "Esmeralda",
			"label" => "Esmeralda ( NV )",
			"locale" => "NV | Esmeralda"
		]);
		County::create([
			"state_id" => 29,
			"location_id" => 0,
			"county" => "Eureka",
			"label" => "Eureka ( NV )",
			"locale" => "NV | Eureka"
		]);
		County::create([
			"state_id" => 29,
			"location_id" => 0,
			"county" => "Humboldt",
			"label" => "Humboldt ( NV )",
			"locale" => "NV | Humboldt"
		]);
		County::create([
			"state_id" => 29,
			"location_id" => 0,
			"county" => "Lander",
			"label" => "Lander ( NV )",
			"locale" => "NV | Lander"
		]);
		County::create([
			"state_id" => 29,
			"location_id" => 0,
			"county" => "Lincoln",
			"label" => "Lincoln ( NV )",
			"locale" => "NV | Lincoln"
		]);
		County::create([
			"state_id" => 29,
			"location_id" => 0,
			"county" => "Lyon",
			"label" => "Lyon ( NV )",
			"locale" => "NV | Lyon"
		]);
		County::create([
			"state_id" => 29,
			"location_id" => 0,
			"county" => "Mineral",
			"label" => "Mineral ( NV )",
			"locale" => "NV | Mineral"
		]);
		County::create([
			"state_id" => 29,
			"location_id" => 0,
			"county" => "Nye",
			"label" => "Nye ( NV )",
			"locale" => "NV | Nye"
		]);
		County::create([
			"state_id" => 29,
			"location_id" => 0,
			"county" => "Pershing",
			"label" => "Pershing ( NV )",
			"locale" => "NV | Pershing"
		]);
		County::create([
			"state_id" => 29,
			"location_id" => 0,
			"county" => "Storey",
			"label" => "Storey ( NV )",
			"locale" => "NV | Storey"
		]);
		County::create([
			"state_id" => 29,
			"location_id" => 0,
			"county" => "Washoe",
			"label" => "Washoe ( NV )",
			"locale" => "NV | Washoe"
		]);
		County::create([
			"state_id" => 29,
			"location_id" => 0,
			"county" => "White Pine",
			"label" => "White Pine ( NV )",
			"locale" => "NV | White Pine"
		]);
		County::create([
			"state_id" => 30,
			"location_id" => 0,
			"county" => "Belknap",
			"label" => "Belknap ( NH )",
			"locale" => "NH | Belknap"
		]);
		County::create([
			"state_id" => 30,
			"location_id" => 0,
			"county" => "Carroll",
			"label" => "Carroll ( NH )",
			"locale" => "NH | Carroll"
		]);
		County::create([
			"state_id" => 30,
			"location_id" => 0,
			"county" => "Cheshire",
			"label" => "Cheshire ( NH )",
			"locale" => "NH | Cheshire"
		]);
		County::create([
			"state_id" => 30,
			"location_id" => 0,
			"county" => "Coos",
			"label" => "Coos ( NH )",
			"locale" => "NH | Coos"
		]);
		County::create([
			"state_id" => 30,
			"location_id" => 0,
			"county" => "Grafton",
			"label" => "Grafton ( NH )",
			"locale" => "NH | Grafton"
		]);
		County::create([
			"state_id" => 30,
			"location_id" => 0,
			"county" => "Hillsborough",
			"label" => "Hillsborough ( NH )",
			"locale" => "NH | Hillsborough"
		]);
		County::create([
			"state_id" => 30,
			"location_id" => 0,
			"county" => "Merrimack",
			"label" => "Merrimack ( NH )",
			"locale" => "NH | Merrimack"
		]);
		County::create([
			"state_id" => 30,
			"location_id" => 0,
			"county" => "Rockingham",
			"label" => "Rockingham ( NH )",
			"locale" => "NH | Rockingham"
		]);
		County::create([
			"state_id" => 30,
			"location_id" => 0,
			"county" => "Strafford",
			"label" => "Strafford ( NH )",
			"locale" => "NH | Strafford"
		]);
		County::create([
			"state_id" => 30,
			"location_id" => 0,
			"county" => "Sullivan",
			"label" => "Sullivan ( NH )",
			"locale" => "NH | Sullivan"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Atlantic",
			"label" => "Atlantic ( NJ )",
			"locale" => "NJ | Atlantic"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Bergen",
			"label" => "Bergen ( NJ )",
			"locale" => "NJ | Bergen"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Burlington",
			"label" => "Burlington ( NJ )",
			"locale" => "NJ | Burlington"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Camden",
			"label" => "Camden ( NJ )",
			"locale" => "NJ | Camden"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Cape May",
			"label" => "Cape May ( NJ )",
			"locale" => "NJ | Cape May"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Cumberland",
			"label" => "Cumberland ( NJ )",
			"locale" => "NJ | Cumberland"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Essex",
			"label" => "Essex ( NJ )",
			"locale" => "NJ | Essex"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Gloucester",
			"label" => "Gloucester ( NJ )",
			"locale" => "NJ | Gloucester"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Hudson",
			"label" => "Hudson ( NJ )",
			"locale" => "NJ | Hudson"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Hunterdon",
			"label" => "Hunterdon ( NJ )",
			"locale" => "NJ | Hunterdon"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Mercer",
			"label" => "Mercer ( NJ )",
			"locale" => "NJ | Mercer"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Middlesex",
			"label" => "Middlesex ( NJ )",
			"locale" => "NJ | Middlesex"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Monmouth",
			"label" => "Monmouth ( NJ )",
			"locale" => "NJ | Monmouth"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Morris",
			"label" => "Morris ( NJ )",
			"locale" => "NJ | Morris"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Ocean",
			"label" => "Ocean ( NJ )",
			"locale" => "NJ | Ocean"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Passaic",
			"label" => "Passaic ( NJ )",
			"locale" => "NJ | Passaic"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Salem",
			"label" => "Salem ( NJ )",
			"locale" => "NJ | Salem"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Somerset",
			"label" => "Somerset ( NJ )",
			"locale" => "NJ | Somerset"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Sussex",
			"label" => "Sussex ( NJ )",
			"locale" => "NJ | Sussex"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Union",
			"label" => "Union ( NJ )",
			"locale" => "NJ | Union"
		]);
		County::create([
			"state_id" => 31,
			"location_id" => 0,
			"county" => "Warren",
			"label" => "Warren ( NJ )",
			"locale" => "NJ | Warren"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Bernalillo",
			"label" => "Bernalillo ( NM )",
			"locale" => "NM | Bernalillo"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Catron",
			"label" => "Catron ( NM )",
			"locale" => "NM | Catron"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Chaves",
			"label" => "Chaves ( NM )",
			"locale" => "NM | Chaves"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Cibola",
			"label" => "Cibola ( NM )",
			"locale" => "NM | Cibola"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Colfax",
			"label" => "Colfax ( NM )",
			"locale" => "NM | Colfax"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Curry",
			"label" => "Curry ( NM )",
			"locale" => "NM | Curry"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "De Baca",
			"label" => "De Baca ( NM )",
			"locale" => "NM | De Baca"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Do±a Ana",
			"label" => "Do±a Ana ( NM )",
			"locale" => "NM | Do±a Ana"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Eddy",
			"label" => "Eddy ( NM )",
			"locale" => "NM | Eddy"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Grant",
			"label" => "Grant ( NM )",
			"locale" => "NM | Grant"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Guadalupe",
			"label" => "Guadalupe ( NM )",
			"locale" => "NM | Guadalupe"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Harding",
			"label" => "Harding ( NM )",
			"locale" => "NM | Harding"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Hidalgo",
			"label" => "Hidalgo ( NM )",
			"locale" => "NM | Hidalgo"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Lea",
			"label" => "Lea ( NM )",
			"locale" => "NM | Lea"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Lincoln",
			"label" => "Lincoln ( NM )",
			"locale" => "NM | Lincoln"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Los Alamos",
			"label" => "Los Alamos ( NM )",
			"locale" => "NM | Los Alamos"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Luna",
			"label" => "Luna ( NM )",
			"locale" => "NM | Luna"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "McKinley",
			"label" => "McKinley ( NM )",
			"locale" => "NM | McKinley"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Mora",
			"label" => "Mora ( NM )",
			"locale" => "NM | Mora"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Otero",
			"label" => "Otero ( NM )",
			"locale" => "NM | Otero"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Quay",
			"label" => "Quay ( NM )",
			"locale" => "NM | Quay"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Rio Arriba",
			"label" => "Rio Arriba ( NM )",
			"locale" => "NM | Rio Arriba"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Roosevelt",
			"label" => "Roosevelt ( NM )",
			"locale" => "NM | Roosevelt"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "San Juan",
			"label" => "San Juan ( NM )",
			"locale" => "NM | San Juan"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "San Miguel",
			"label" => "San Miguel ( NM )",
			"locale" => "NM | San Miguel"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Sandoval",
			"label" => "Sandoval ( NM )",
			"locale" => "NM | Sandoval"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Santa Fe",
			"label" => "Santa Fe ( NM )",
			"locale" => "NM | Santa Fe"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Sierra",
			"label" => "Sierra ( NM )",
			"locale" => "NM | Sierra"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Socorro",
			"label" => "Socorro ( NM )",
			"locale" => "NM | Socorro"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Taos",
			"label" => "Taos ( NM )",
			"locale" => "NM | Taos"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Torrance",
			"label" => "Torrance ( NM )",
			"locale" => "NM | Torrance"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Union",
			"label" => "Union ( NM )",
			"locale" => "NM | Union"
		]);
		County::create([
			"state_id" => 32,
			"location_id" => 0,
			"county" => "Valencia",
			"label" => "Valencia ( NM )",
			"locale" => "NM | Valencia"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Albany",
			"label" => "Albany ( NY )",
			"locale" => "NY | Albany"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Allegany",
			"label" => "Allegany ( NY )",
			"locale" => "NY | Allegany"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Broome",
			"label" => "Broome ( NY )",
			"locale" => "NY | Broome"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Cattaraugus",
			"label" => "Cattaraugus ( NY )",
			"locale" => "NY | Cattaraugus"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Cayuga",
			"label" => "Cayuga ( NY )",
			"locale" => "NY | Cayuga"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Chautauqua",
			"label" => "Chautauqua ( NY )",
			"locale" => "NY | Chautauqua"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Chemung",
			"label" => "Chemung ( NY )",
			"locale" => "NY | Chemung"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Chenango",
			"label" => "Chenango ( NY )",
			"locale" => "NY | Chenango"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Clinton",
			"label" => "Clinton ( NY )",
			"locale" => "NY | Clinton"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Columbia",
			"label" => "Columbia ( NY )",
			"locale" => "NY | Columbia"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Cortland",
			"label" => "Cortland ( NY )",
			"locale" => "NY | Cortland"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Delaware",
			"label" => "Delaware ( NY )",
			"locale" => "NY | Delaware"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Dutchess",
			"label" => "Dutchess ( NY )",
			"locale" => "NY | Dutchess"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Erie",
			"label" => "Erie ( NY )",
			"locale" => "NY | Erie"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Essex",
			"label" => "Essex ( NY )",
			"locale" => "NY | Essex"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Franklin",
			"label" => "Franklin ( NY )",
			"locale" => "NY | Franklin"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Fulton",
			"label" => "Fulton ( NY )",
			"locale" => "NY | Fulton"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Genesee",
			"label" => "Genesee ( NY )",
			"locale" => "NY | Genesee"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Greene",
			"label" => "Greene ( NY )",
			"locale" => "NY | Greene"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Hamilton",
			"label" => "Hamilton ( NY )",
			"locale" => "NY | Hamilton"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Herkimer",
			"label" => "Herkimer ( NY )",
			"locale" => "NY | Herkimer"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Jefferson",
			"label" => "Jefferson ( NY )",
			"locale" => "NY | Jefferson"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Lewis",
			"label" => "Lewis ( NY )",
			"locale" => "NY | Lewis"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Livingston",
			"label" => "Livingston ( NY )",
			"locale" => "NY | Livingston"
		]);
        County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Madison",
			"label" => "Madison ( NY )",
			"locale" => "NY | Madison"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Monroe",
			"label" => "Monroe ( NY )",
			"locale" => "NY | Monroe"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Montgomery",
			"label" => "Montgomery ( NY )",
			"locale" => "NY | Montgomery"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Nassau",
			"label" => "Nassau ( NY )",
			"locale" => "NY | Nassau"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Niagara",
			"label" => "Niagara ( NY )",
			"locale" => "NY | Niagara"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Oneida",
			"label" => "Oneida ( NY )",
			"locale" => "NY | Oneida"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Onondaga",
			"label" => "Onondaga ( NY )",
			"locale" => "NY | Onondaga"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Ontario",
			"label" => "Ontario ( NY )",
			"locale" => "NY | Ontario"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Orange",
			"label" => "Orange ( NY )",
			"locale" => "NY | Orange"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Orleans",
			"label" => "Orleans ( NY )",
			"locale" => "NY | Orleans"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Oswego",
			"label" => "Oswego ( NY )",
			"locale" => "NY | Oswego"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Otsego",
			"label" => "Otsego ( NY )",
			"locale" => "NY | Otsego"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Putnam",
			"label" => "Putnam ( NY )",
			"locale" => "NY | Putnam"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Rensselaer",
			"label" => "Rensselaer ( NY )",
			"locale" => "NY | Rensselaer"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Rockland",
			"label" => "Rockland ( NY )",
			"locale" => "NY | Rockland"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Saratoga",
			"label" => "Saratoga ( NY )",
			"locale" => "NY | Saratoga"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Schenectady",
			"label" => "Schenectady ( NY )",
			"locale" => "NY | Schenectady"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Schoharie",
			"label" => "Schoharie ( NY )",
			"locale" => "NY | Schoharie"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Schuyler",
			"label" => "Schuyler ( NY )",
			"locale" => "NY | Schuyler"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Seneca",
			"label" => "Seneca ( NY )",
			"locale" => "NY | Seneca"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "St. Lawrence",
			"label" => "St. Lawrence ( NY )",
			"locale" => "NY | St. Lawrence"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Steuben",
			"label" => "Steuben ( NY )",
			"locale" => "NY | Steuben"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Suffolk",
			"label" => "Suffolk ( NY )",
			"locale" => "NY | Suffolk"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Sullivan",
			"label" => "Sullivan ( NY )",
			"locale" => "NY | Sullivan"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Tioga",
			"label" => "Tioga ( NY )",
			"locale" => "NY | Tioga"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Tompkins",
			"label" => "Tompkins ( NY )",
			"locale" => "NY | Tompkins"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Ulster",
			"label" => "Ulster ( NY )",
			"locale" => "NY | Ulster"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Warren",
			"label" => "Warren ( NY )",
			"locale" => "NY | Warren"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( NY )",
			"locale" => "NY | Washington"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Wayne",
			"label" => "Wayne ( NY )",
			"locale" => "NY | Wayne"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Westchester",
			"label" => "Westchester ( NY )",
			"locale" => "NY | Westchester"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Wyoming",
			"label" => "Wyoming ( NY )",
			"locale" => "NY | Wyoming"
		]);
		County::create([
			"state_id" => 33,
			"location_id" => 0,
			"county" => "Yates",
			"label" => "Yates ( NY )",
			"locale" => "NY | Yates"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Alamance",
			"label" => "Alamance ( NC )",
			"locale" => "NC | Alamance"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Alexander",
			"label" => "Alexander ( NC )",
			"locale" => "NC | Alexander"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Alleghany",
			"label" => "Alleghany ( NC )",
			"locale" => "NC | Alleghany"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Anson",
			"label" => "Anson ( NC )",
			"locale" => "NC | Anson"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Ashe",
			"label" => "Ashe ( NC )",
			"locale" => "NC | Ashe"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Avery",
			"label" => "Avery ( NC )",
			"locale" => "NC | Avery"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Beaufort",
			"label" => "Beaufort ( NC )",
			"locale" => "NC | Beaufort"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Bertie",
			"label" => "Bertie ( NC )",
			"locale" => "NC | Bertie"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Bladen",
			"label" => "Bladen ( NC )",
			"locale" => "NC | Bladen"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Brunswick",
			"label" => "Brunswick ( NC )",
			"locale" => "NC | Brunswick"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Buncombe",
			"label" => "Buncombe ( NC )",
			"locale" => "NC | Buncombe"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Burke",
			"label" => "Burke ( NC )",
			"locale" => "NC | Burke"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Cabarrus",
			"label" => "Cabarrus ( NC )",
			"locale" => "NC | Cabarrus"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Caldwell",
			"label" => "Caldwell ( NC )",
			"locale" => "NC | Caldwell"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Camden",
			"label" => "Camden ( NC )",
			"locale" => "NC | Camden"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Carteret",
			"label" => "Carteret ( NC )",
			"locale" => "NC | Carteret"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Caswell",
			"label" => "Caswell ( NC )",
			"locale" => "NC | Caswell"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Catawba",
			"label" => "Catawba ( NC )",
			"locale" => "NC | Catawba"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Chatham",
			"label" => "Chatham ( NC )",
			"locale" => "NC | Chatham"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Cherokee",
			"label" => "Cherokee ( NC )",
			"locale" => "NC | Cherokee"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Chowan",
			"label" => "Chowan ( NC )",
			"locale" => "NC | Chowan"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Clay",
			"label" => "Clay ( NC )",
			"locale" => "NC | Clay"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Cleveland",
			"label" => "Cleveland ( NC )",
			"locale" => "NC | Cleveland"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Columbus",
			"label" => "Columbus ( NC )",
			"locale" => "NC | Columbus"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Craven",
			"label" => "Craven ( NC )",
			"locale" => "NC | Craven"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Cumberland",
			"label" => "Cumberland ( NC )",
			"locale" => "NC | Cumberland"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Currituck",
			"label" => "Currituck ( NC )",
			"locale" => "NC | Currituck"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Dare",
			"label" => "Dare ( NC )",
			"locale" => "NC | Dare"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Davidson",
			"label" => "Davidson ( NC )",
			"locale" => "NC | Davidson"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Davie",
			"label" => "Davie ( NC )",
			"locale" => "NC | Davie"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Duplin",
			"label" => "Duplin ( NC )",
			"locale" => "NC | Duplin"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Durham",
			"label" => "Durham ( NC )",
			"locale" => "NC | Durham"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Edgecombe",
			"label" => "Edgecombe ( NC )",
			"locale" => "NC | Edgecombe"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Forsyth",
			"label" => "Forsyth ( NC )",
			"locale" => "NC | Forsyth"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Franklin",
			"label" => "Franklin ( NC )",
			"locale" => "NC | Franklin"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Gaston",
			"label" => "Gaston ( NC )",
			"locale" => "NC | Gaston"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Gates",
			"label" => "Gates ( NC )",
			"locale" => "NC | Gates"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Graham",
			"label" => "Graham ( NC )",
			"locale" => "NC | Graham"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Granville",
			"label" => "Granville ( NC )",
			"locale" => "NC | Granville"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Greene",
			"label" => "Greene ( NC )",
			"locale" => "NC | Greene"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Guilford",
			"label" => "Guilford ( NC )",
			"locale" => "NC | Guilford"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Halifax",
			"label" => "Halifax ( NC )",
			"locale" => "NC | Halifax"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Harnett",
			"label" => "Harnett ( NC )",
			"locale" => "NC | Harnett"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Haywood",
			"label" => "Haywood ( NC )",
			"locale" => "NC | Haywood"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Henderson",
			"label" => "Henderson ( NC )",
			"locale" => "NC | Henderson"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Hertford",
			"label" => "Hertford ( NC )",
			"locale" => "NC | Hertford"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Hoke",
			"label" => "Hoke ( NC )",
			"locale" => "NC | Hoke"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Hyde",
			"label" => "Hyde ( NC )",
			"locale" => "NC | Hyde"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Iredell",
			"label" => "Iredell ( NC )",
			"locale" => "NC | Iredell"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Jackson",
			"label" => "Jackson ( NC )",
			"locale" => "NC | Jackson"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Johnston",
			"label" => "Johnston ( NC )",
			"locale" => "NC | Johnston"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Jones",
			"label" => "Jones ( NC )",
			"locale" => "NC | Jones"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Lee",
			"label" => "Lee ( NC )",
			"locale" => "NC | Lee"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Lenoir",
			"label" => "Lenoir ( NC )",
			"locale" => "NC | Lenoir"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Lincoln",
			"label" => "Lincoln ( NC )",
			"locale" => "NC | Lincoln"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Macon",
			"label" => "Macon ( NC )",
			"locale" => "NC | Macon"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Madison",
			"label" => "Madison ( NC )",
			"locale" => "NC | Madison"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Martin",
			"label" => "Martin ( NC )",
			"locale" => "NC | Martin"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "McDowell",
			"label" => "McDowell ( NC )",
			"locale" => "NC | McDowell"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Mecklenburg",
			"label" => "Mecklenburg ( NC )",
			"locale" => "NC | Mecklenburg"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Mitchell",
			"label" => "Mitchell ( NC )",
			"locale" => "NC | Mitchell"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Montgomery",
			"label" => "Montgomery ( NC )",
			"locale" => "NC | Montgomery"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Moore",
			"label" => "Moore ( NC )",
			"locale" => "NC | Moore"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Nash",
			"label" => "Nash ( NC )",
			"locale" => "NC | Nash"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "New Hanover",
			"label" => "New Hanover ( NC )",
			"locale" => "NC | New Hanover"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Northampton",
			"label" => "Northampton ( NC )",
			"locale" => "NC | Northampton"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Onslow",
			"label" => "Onslow ( NC )",
			"locale" => "NC | Onslow"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Orange",
			"label" => "Orange ( NC )",
			"locale" => "NC | Orange"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Pamlico",
			"label" => "Pamlico ( NC )",
			"locale" => "NC | Pamlico"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Pasquotank",
			"label" => "Pasquotank ( NC )",
			"locale" => "NC | Pasquotank"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Pender",
			"label" => "Pender ( NC )",
			"locale" => "NC | Pender"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Perquimans",
			"label" => "Perquimans ( NC )",
			"locale" => "NC | Perquimans"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Person",
			"label" => "Person ( NC )",
			"locale" => "NC | Person"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Pitt",
			"label" => "Pitt ( NC )",
			"locale" => "NC | Pitt"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Polk",
			"label" => "Polk ( NC )",
			"locale" => "NC | Polk"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Randolph",
			"label" => "Randolph ( NC )",
			"locale" => "NC | Randolph"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Richmond",
			"label" => "Richmond ( NC )",
			"locale" => "NC | Richmond"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Robeson",
			"label" => "Robeson ( NC )",
			"locale" => "NC | Robeson"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Rockingham",
			"label" => "Rockingham ( NC )",
			"locale" => "NC | Rockingham"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Rowan",
			"label" => "Rowan ( NC )",
			"locale" => "NC | Rowan"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Rutherford",
			"label" => "Rutherford ( NC )",
			"locale" => "NC | Rutherford"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Sampson",
			"label" => "Sampson ( NC )",
			"locale" => "NC | Sampson"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Scotland",
			"label" => "Scotland ( NC )",
			"locale" => "NC | Scotland"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Stanly",
			"label" => "Stanly ( NC )",
			"locale" => "NC | Stanly"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Stokes",
			"label" => "Stokes ( NC )",
			"locale" => "NC | Stokes"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Surry",
			"label" => "Surry ( NC )",
			"locale" => "NC | Surry"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Swain",
			"label" => "Swain ( NC )",
			"locale" => "NC | Swain"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Transylvania",
			"label" => "Transylvania ( NC )",
			"locale" => "NC | Transylvania"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Tyrrell",
			"label" => "Tyrrell ( NC )",
			"locale" => "NC | Tyrrell"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Union",
			"label" => "Union ( NC )",
			"locale" => "NC | Union"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Vance",
			"label" => "Vance ( NC )",
			"locale" => "NC | Vance"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Wake",
			"label" => "Wake ( NC )",
			"locale" => "NC | Wake"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Warren",
			"label" => "Warren ( NC )",
			"locale" => "NC | Warren"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( NC )",
			"locale" => "NC | Washington"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Watauga",
			"label" => "Watauga ( NC )",
			"locale" => "NC | Watauga"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Wayne",
			"label" => "Wayne ( NC )",
			"locale" => "NC | Wayne"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Wilkes",
			"label" => "Wilkes ( NC )",
			"locale" => "NC | Wilkes"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Wilson",
			"label" => "Wilson ( NC )",
			"locale" => "NC | Wilson"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Yadkin",
			"label" => "Yadkin ( NC )",
			"locale" => "NC | Yadkin"
		]);
		County::create([
			"state_id" => 34,
			"location_id" => 0,
			"county" => "Yancey",
			"label" => "Yancey ( NC )",
			"locale" => "NC | Yancey"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Adams",
			"label" => "Adams ( ND )",
			"locale" => "ND | Adams"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Barnes",
			"label" => "Barnes ( ND )",
			"locale" => "ND | Barnes"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Benson",
			"label" => "Benson ( ND )",
			"locale" => "ND | Benson"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Billings",
			"label" => "Billings ( ND )",
			"locale" => "ND | Billings"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Bottineau",
			"label" => "Bottineau ( ND )",
			"locale" => "ND | Bottineau"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Bowman",
			"label" => "Bowman ( ND )",
			"locale" => "ND | Bowman"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Burke",
			"label" => "Burke ( ND )",
			"locale" => "ND | Burke"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Burleigh",
			"label" => "Burleigh ( ND )",
			"locale" => "ND | Burleigh"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Cass",
			"label" => "Cass ( ND )",
			"locale" => "ND | Cass"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Cavalier",
			"label" => "Cavalier ( ND )",
			"locale" => "ND | Cavalier"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Dickey",
			"label" => "Dickey ( ND )",
			"locale" => "ND | Dickey"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Divide",
			"label" => "Divide ( ND )",
			"locale" => "ND | Divide"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Dunn",
			"label" => "Dunn ( ND )",
			"locale" => "ND | Dunn"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Eddy",
			"label" => "Eddy ( ND )",
			"locale" => "ND | Eddy"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Emmons",
			"label" => "Emmons ( ND )",
			"locale" => "ND | Emmons"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Foster",
			"label" => "Foster ( ND )",
			"locale" => "ND | Foster"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Golden Valley",
			"label" => "Golden Valley ( ND )",
			"locale" => "ND | Golden Valley"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Grand Forks",
			"label" => "Grand Forks ( ND )",
			"locale" => "ND | Grand Forks"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Grant",
			"label" => "Grant ( ND )",
			"locale" => "ND | Grant"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Griggs",
			"label" => "Griggs ( ND )",
			"locale" => "ND | Griggs"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Hettinger",
			"label" => "Hettinger ( ND )",
			"locale" => "ND | Hettinger"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Kidder",
			"label" => "Kidder ( ND )",
			"locale" => "ND | Kidder"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "LaMoure",
			"label" => "LaMoure ( ND )",
			"locale" => "ND | LaMoure"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Logan",
			"label" => "Logan ( ND )",
			"locale" => "ND | Logan"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "McHenry",
			"label" => "McHenry ( ND )",
			"locale" => "ND | McHenry"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "McIntosh",
			"label" => "McIntosh ( ND )",
			"locale" => "ND | McIntosh"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "McKenzie",
			"label" => "McKenzie ( ND )",
			"locale" => "ND | McKenzie"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "McLean",
			"label" => "McLean ( ND )",
			"locale" => "ND | McLean"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Mercer",
			"label" => "Mercer ( ND )",
			"locale" => "ND | Mercer"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Morton",
			"label" => "Morton ( ND )",
			"locale" => "ND | Morton"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Mountrail",
			"label" => "Mountrail ( ND )",
			"locale" => "ND | Mountrail"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Nelson",
			"label" => "Nelson ( ND )",
			"locale" => "ND | Nelson"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Oliver",
			"label" => "Oliver ( ND )",
			"locale" => "ND | Oliver"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Pembina",
			"label" => "Pembina ( ND )",
			"locale" => "ND | Pembina"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Pierce",
			"label" => "Pierce ( ND )",
			"locale" => "ND | Pierce"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Ramsey",
			"label" => "Ramsey ( ND )",
			"locale" => "ND | Ramsey"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Ransom",
			"label" => "Ransom ( ND )",
			"locale" => "ND | Ransom"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Renville",
			"label" => "Renville ( ND )",
			"locale" => "ND | Renville"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Richland",
			"label" => "Richland ( ND )",
			"locale" => "ND | Richland"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Rolette",
			"label" => "Rolette ( ND )",
			"locale" => "ND | Rolette"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Sargent",
			"label" => "Sargent ( ND )",
			"locale" => "ND | Sargent"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Sheridan",
			"label" => "Sheridan ( ND )",
			"locale" => "ND | Sheridan"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Sioux",
			"label" => "Sioux ( ND )",
			"locale" => "ND | Sioux"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Slope",
			"label" => "Slope ( ND )",
			"locale" => "ND | Slope"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Stark",
			"label" => "Stark ( ND )",
			"locale" => "ND | Stark"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Steele",
			"label" => "Steele ( ND )",
			"locale" => "ND | Steele"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Stutsman",
			"label" => "Stutsman ( ND )",
			"locale" => "ND | Stutsman"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Towner",
			"label" => "Towner ( ND )",
			"locale" => "ND | Towner"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Traill",
			"label" => "Traill ( ND )",
			"locale" => "ND | Traill"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Walsh",
			"label" => "Walsh ( ND )",
			"locale" => "ND | Walsh"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Ward",
			"label" => "Ward ( ND )",
			"locale" => "ND | Ward"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Wells",
			"label" => "Wells ( ND )",
			"locale" => "ND | Wells"
		]);
		County::create([
			"state_id" => 35,
			"location_id" => 0,
			"county" => "Williams",
			"label" => "Williams ( ND )",
			"locale" => "ND | Williams"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Adams",
			"label" => "Adams ( OH )",
			"locale" => "OH | Adams"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Allen",
			"label" => "Allen ( OH )",
			"locale" => "OH | Allen"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Ashland",
			"label" => "Ashland ( OH )",
			"locale" => "OH | Ashland"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Ashtabula",
			"label" => "Ashtabula ( OH )",
			"locale" => "OH | Ashtabula"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Athens",
			"label" => "Athens ( OH )",
			"locale" => "OH | Athens"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Auglaize",
			"label" => "Auglaize ( OH )",
			"locale" => "OH | Auglaize"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Belmont",
			"label" => "Belmont ( OH )",
			"locale" => "OH | Belmont"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Brown",
			"label" => "Brown ( OH )",
			"locale" => "OH | Brown"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Butler",
			"label" => "Butler ( OH )",
			"locale" => "OH | Butler"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Carroll",
			"label" => "Carroll ( OH )",
			"locale" => "OH | Carroll"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Champaign",
			"label" => "Champaign ( OH )",
			"locale" => "OH | Champaign"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Clark",
			"label" => "Clark ( OH )",
			"locale" => "OH | Clark"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Clermont",
			"label" => "Clermont ( OH )",
			"locale" => "OH | Clermont"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Clinton",
			"label" => "Clinton ( OH )",
			"locale" => "OH | Clinton"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Columbiana",
			"label" => "Columbiana ( OH )",
			"locale" => "OH | Columbiana"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Coshocton",
			"label" => "Coshocton ( OH )",
			"locale" => "OH | Coshocton"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Crawford",
			"label" => "Crawford ( OH )",
			"locale" => "OH | Crawford"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Cuyahoga",
			"label" => "Cuyahoga ( OH )",
			"locale" => "OH | Cuyahoga"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Darke",
			"label" => "Darke ( OH )",
			"locale" => "OH | Darke"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Defiance",
			"label" => "Defiance ( OH )",
			"locale" => "OH | Defiance"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Delaware",
			"label" => "Delaware ( OH )",
			"locale" => "OH | Delaware"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Erie",
			"label" => "Erie ( OH )",
			"locale" => "OH | Erie"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Fairfield",
			"label" => "Fairfield ( OH )",
			"locale" => "OH | Fairfield"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Fayette",
			"label" => "Fayette ( OH )",
			"locale" => "OH | Fayette"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Franklin",
			"label" => "Franklin ( OH )",
			"locale" => "OH | Franklin"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Fulton",
			"label" => "Fulton ( OH )",
			"locale" => "OH | Fulton"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Gallia",
			"label" => "Gallia ( OH )",
			"locale" => "OH | Gallia"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Geauga",
			"label" => "Geauga ( OH )",
			"locale" => "OH | Geauga"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Greene",
			"label" => "Greene ( OH )",
			"locale" => "OH | Greene"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Guernsey",
			"label" => "Guernsey ( OH )",
			"locale" => "OH | Guernsey"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Hamilton",
			"label" => "Hamilton ( OH )",
			"locale" => "OH | Hamilton"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Hancock",
			"label" => "Hancock ( OH )",
			"locale" => "OH | Hancock"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Hardin",
			"label" => "Hardin ( OH )",
			"locale" => "OH | Hardin"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Harrison",
			"label" => "Harrison ( OH )",
			"locale" => "OH | Harrison"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Henry",
			"label" => "Henry ( OH )",
			"locale" => "OH | Henry"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Highland",
			"label" => "Highland ( OH )",
			"locale" => "OH | Highland"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Hocking",
			"label" => "Hocking ( OH )",
			"locale" => "OH | Hocking"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Holmes",
			"label" => "Holmes ( OH )",
			"locale" => "OH | Holmes"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Huron",
			"label" => "Huron ( OH )",
			"locale" => "OH | Huron"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Jackson",
			"label" => "Jackson ( OH )",
			"locale" => "OH | Jackson"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Jefferson",
			"label" => "Jefferson ( OH )",
			"locale" => "OH | Jefferson"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Knox",
			"label" => "Knox ( OH )",
			"locale" => "OH | Knox"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Lake",
			"label" => "Lake ( OH )",
			"locale" => "OH | Lake"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Lawrence",
			"label" => "Lawrence ( OH )",
			"locale" => "OH | Lawrence"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Licking",
			"label" => "Licking ( OH )",
			"locale" => "OH | Licking"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Logan",
			"label" => "Logan ( OH )",
			"locale" => "OH | Logan"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Lorain",
			"label" => "Lorain ( OH )",
			"locale" => "OH | Lorain"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Lucas",
			"label" => "Lucas ( OH )",
			"locale" => "OH | Lucas"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Madison",
			"label" => "Madison ( OH )",
			"locale" => "OH | Madison"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Mahoning",
			"label" => "Mahoning ( OH )",
			"locale" => "OH | Mahoning"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Marion",
			"label" => "Marion ( OH )",
			"locale" => "OH | Marion"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Medina",
			"label" => "Medina ( OH )",
			"locale" => "OH | Medina"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Meigs",
			"label" => "Meigs ( OH )",
			"locale" => "OH | Meigs"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Mercer",
			"label" => "Mercer ( OH )",
			"locale" => "OH | Mercer"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Miami",
			"label" => "Miami ( OH )",
			"locale" => "OH | Miami"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Monroe",
			"label" => "Monroe ( OH )",
			"locale" => "OH | Monroe"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Montgomery",
			"label" => "Montgomery ( OH )",
			"locale" => "OH | Montgomery"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Morgan",
			"label" => "Morgan ( OH )",
			"locale" => "OH | Morgan"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Morrow",
			"label" => "Morrow ( OH )",
			"locale" => "OH | Morrow"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Muskingum",
			"label" => "Muskingum ( OH )",
			"locale" => "OH | Muskingum"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Noble",
			"label" => "Noble ( OH )",
			"locale" => "OH | Noble"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Ottawa",
			"label" => "Ottawa ( OH )",
			"locale" => "OH | Ottawa"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Paulding",
			"label" => "Paulding ( OH )",
			"locale" => "OH | Paulding"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Perry",
			"label" => "Perry ( OH )",
			"locale" => "OH | Perry"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Pickaway",
			"label" => "Pickaway ( OH )",
			"locale" => "OH | Pickaway"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Pike",
			"label" => "Pike ( OH )",
			"locale" => "OH | Pike"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Portage",
			"label" => "Portage ( OH )",
			"locale" => "OH | Portage"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Preble",
			"label" => "Preble ( OH )",
			"locale" => "OH | Preble"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Putnam",
			"label" => "Putnam ( OH )",
			"locale" => "OH | Putnam"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Richland",
			"label" => "Richland ( OH )",
			"locale" => "OH | Richland"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Ross",
			"label" => "Ross ( OH )",
			"locale" => "OH | Ross"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Sandusky",
			"label" => "Sandusky ( OH )",
			"locale" => "OH | Sandusky"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Scioto",
			"label" => "Scioto ( OH )",
			"locale" => "OH | Scioto"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Seneca",
			"label" => "Seneca ( OH )",
			"locale" => "OH | Seneca"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Shelby",
			"label" => "Shelby ( OH )",
			"locale" => "OH | Shelby"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Stark",
			"label" => "Stark ( OH )",
			"locale" => "OH | Stark"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Summit",
			"label" => "Summit ( OH )",
			"locale" => "OH | Summit"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Trumbull",
			"label" => "Trumbull ( OH )",
			"locale" => "OH | Trumbull"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Tuscarawas",
			"label" => "Tuscarawas ( OH )",
			"locale" => "OH | Tuscarawas"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Union",
			"label" => "Union ( OH )",
			"locale" => "OH | Union"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Van Wert",
			"label" => "Van Wert ( OH )",
			"locale" => "OH | Van Wert"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Vinton",
			"label" => "Vinton ( OH )",
			"locale" => "OH | Vinton"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Warren",
			"label" => "Warren ( OH )",
			"locale" => "OH | Warren"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( OH )",
			"locale" => "OH | Washington"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Wayne",
			"label" => "Wayne ( OH )",
			"locale" => "OH | Wayne"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Williams",
			"label" => "Williams ( OH )",
			"locale" => "OH | Williams"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Wood",
			"label" => "Wood ( OH )",
			"locale" => "OH | Wood"
		]);
		County::create([
			"state_id" => 36,
			"location_id" => 0,
			"county" => "Wyandot",
			"label" => "Wyandot ( OH )",
			"locale" => "OH | Wyandot"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Adair",
			"label" => "Adair ( OK )",
			"locale" => "OK | Adair"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Alfalfa",
			"label" => "Alfalfa ( OK )",
			"locale" => "OK | Alfalfa"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Atoka",
			"label" => "Atoka ( OK )",
			"locale" => "OK | Atoka"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Beaver",
			"label" => "Beaver ( OK )",
			"locale" => "OK | Beaver"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Beckham",
			"label" => "Beckham ( OK )",
			"locale" => "OK | Beckham"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Blaine",
			"label" => "Blaine ( OK )",
			"locale" => "OK | Blaine"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Bryan",
			"label" => "Bryan ( OK )",
			"locale" => "OK | Bryan"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Caddo",
			"label" => "Caddo ( OK )",
			"locale" => "OK | Caddo"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Canadian",
			"label" => "Canadian ( OK )",
			"locale" => "OK | Canadian"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Carter",
			"label" => "Carter ( OK )",
			"locale" => "OK | Carter"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Cherokee",
			"label" => "Cherokee ( OK )",
			"locale" => "OK | Cherokee"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Choctaw",
			"label" => "Choctaw ( OK )",
			"locale" => "OK | Choctaw"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Cimarron",
			"label" => "Cimarron ( OK )",
			"locale" => "OK | Cimarron"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Cleveland",
			"label" => "Cleveland ( OK )",
			"locale" => "OK | Cleveland"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Coal",
			"label" => "Coal ( OK )",
			"locale" => "OK | Coal"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Comanche",
			"label" => "Comanche ( OK )",
			"locale" => "OK | Comanche"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Cotton",
			"label" => "Cotton ( OK )",
			"locale" => "OK | Cotton"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Craig",
			"label" => "Craig ( OK )",
			"locale" => "OK | Craig"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Creek",
			"label" => "Creek ( OK )",
			"locale" => "OK | Creek"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Custer",
			"label" => "Custer ( OK )",
			"locale" => "OK | Custer"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Delaware",
			"label" => "Delaware ( OK )",
			"locale" => "OK | Delaware"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Dewey",
			"label" => "Dewey ( OK )",
			"locale" => "OK | Dewey"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Ellis",
			"label" => "Ellis ( OK )",
			"locale" => "OK | Ellis"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Garfield",
			"label" => "Garfield ( OK )",
			"locale" => "OK | Garfield"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Garvin",
			"label" => "Garvin ( OK )",
			"locale" => "OK | Garvin"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Grady",
			"label" => "Grady ( OK )",
			"locale" => "OK | Grady"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Grant",
			"label" => "Grant ( OK )",
			"locale" => "OK | Grant"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Greer",
			"label" => "Greer ( OK )",
			"locale" => "OK | Greer"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Harmon",
			"label" => "Harmon ( OK )",
			"locale" => "OK | Harmon"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Harper",
			"label" => "Harper ( OK )",
			"locale" => "OK | Harper"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Haskell",
			"label" => "Haskell ( OK )",
			"locale" => "OK | Haskell"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Hughes",
			"label" => "Hughes ( OK )",
			"locale" => "OK | Hughes"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Jackson",
			"label" => "Jackson ( OK )",
			"locale" => "OK | Jackson"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Jefferson",
			"label" => "Jefferson ( OK )",
			"locale" => "OK | Jefferson"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Johnston",
			"label" => "Johnston ( OK )",
			"locale" => "OK | Johnston"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Kay",
			"label" => "Kay ( OK )",
			"locale" => "OK | Kay"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Kingfisher",
			"label" => "Kingfisher ( OK )",
			"locale" => "OK | Kingfisher"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Kiowa",
			"label" => "Kiowa ( OK )",
			"locale" => "OK | Kiowa"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Latimer",
			"label" => "Latimer ( OK )",
			"locale" => "OK | Latimer"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Le Flore",
			"label" => "Le Flore ( OK )",
			"locale" => "OK | Le Flore"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Lincoln",
			"label" => "Lincoln ( OK )",
			"locale" => "OK | Lincoln"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Logan",
			"label" => "Logan ( OK )",
			"locale" => "OK | Logan"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Love",
			"label" => "Love ( OK )",
			"locale" => "OK | Love"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Major",
			"label" => "Major ( OK )",
			"locale" => "OK | Major"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Marshall",
			"label" => "Marshall ( OK )",
			"locale" => "OK | Marshall"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Mayes",
			"label" => "Mayes ( OK )",
			"locale" => "OK | Mayes"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "McClain",
			"label" => "McClain ( OK )",
			"locale" => "OK | McClain"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "McCurtain",
			"label" => "McCurtain ( OK )",
			"locale" => "OK | McCurtain"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "McIntosh",
			"label" => "McIntosh ( OK )",
			"locale" => "OK | McIntosh"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Murray",
			"label" => "Murray ( OK )",
			"locale" => "OK | Murray"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Muskogee",
			"label" => "Muskogee ( OK )",
			"locale" => "OK | Muskogee"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Noble",
			"label" => "Noble ( OK )",
			"locale" => "OK | Noble"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Nowata",
			"label" => "Nowata ( OK )",
			"locale" => "OK | Nowata"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Okfuskee",
			"label" => "Okfuskee ( OK )",
			"locale" => "OK | Okfuskee"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Oklahoma",
			"label" => "Oklahoma ( OK )",
			"locale" => "OK | Oklahoma"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Okmulgee",
			"label" => "Okmulgee ( OK )",
			"locale" => "OK | Okmulgee"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Osage",
			"label" => "Osage ( OK )",
			"locale" => "OK | Osage"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Ottawa",
			"label" => "Ottawa ( OK )",
			"locale" => "OK | Ottawa"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Pawnee",
			"label" => "Pawnee ( OK )",
			"locale" => "OK | Pawnee"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Payne",
			"label" => "Payne ( OK )",
			"locale" => "OK | Payne"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Pittsburg",
			"label" => "Pittsburg ( OK )",
			"locale" => "OK | Pittsburg"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Pontotoc",
			"label" => "Pontotoc ( OK )",
			"locale" => "OK | Pontotoc"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Pottawatomie",
			"label" => "Pottawatomie ( OK )",
			"locale" => "OK | Pottawatomie"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Pushmataha",
			"label" => "Pushmataha ( OK )",
			"locale" => "OK | Pushmataha"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Roger Mills",
			"label" => "Roger Mills ( OK )",
			"locale" => "OK | Roger Mills"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Rogers",
			"label" => "Rogers ( OK )",
			"locale" => "OK | Rogers"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Seminole",
			"label" => "Seminole ( OK )",
			"locale" => "OK | Seminole"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Sequoyah",
			"label" => "Sequoyah ( OK )",
			"locale" => "OK | Sequoyah"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Stephens",
			"label" => "Stephens ( OK )",
			"locale" => "OK | Stephens"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Texas",
			"label" => "Texas ( OK )",
			"locale" => "OK | Texas"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Tillman",
			"label" => "Tillman ( OK )",
			"locale" => "OK | Tillman"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Tulsa",
			"label" => "Tulsa ( OK )",
			"locale" => "OK | Tulsa"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Wagoner",
			"label" => "Wagoner ( OK )",
			"locale" => "OK | Wagoner"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( OK )",
			"locale" => "OK | Washington"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Washita",
			"label" => "Washita ( OK )",
			"locale" => "OK | Washita"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Woods",
			"label" => "Woods ( OK )",
			"locale" => "OK | Woods"
		]);
		County::create([
			"state_id" => 37,
			"location_id" => 0,
			"county" => "Woodward",
			"label" => "Woodward ( OK )",
			"locale" => "OK | Woodward"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Baker",
			"label" => "Baker ( OR )",
			"locale" => "OR | Baker"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Benton",
			"label" => "Benton ( OR )",
			"locale" => "OR | Benton"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Clackamas",
			"label" => "Clackamas ( OR )",
			"locale" => "OR | Clackamas"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Clatsop",
			"label" => "Clatsop ( OR )",
			"locale" => "OR | Clatsop"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Columbia",
			"label" => "Columbia ( OR )",
			"locale" => "OR | Columbia"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Coos",
			"label" => "Coos ( OR )",
			"locale" => "OR | Coos"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Crook",
			"label" => "Crook ( OR )",
			"locale" => "OR | Crook"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Curry",
			"label" => "Curry ( OR )",
			"locale" => "OR | Curry"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Deschutes",
			"label" => "Deschutes ( OR )",
			"locale" => "OR | Deschutes"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Douglas",
			"label" => "Douglas ( OR )",
			"locale" => "OR | Douglas"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Gilliam",
			"label" => "Gilliam ( OR )",
			"locale" => "OR | Gilliam"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Grant",
			"label" => "Grant ( OR )",
			"locale" => "OR | Grant"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Harney",
			"label" => "Harney ( OR )",
			"locale" => "OR | Harney"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Hood River",
			"label" => "Hood River ( OR )",
			"locale" => "OR | Hood River"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Jackson",
			"label" => "Jackson ( OR )",
			"locale" => "OR | Jackson"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Jefferson",
			"label" => "Jefferson ( OR )",
			"locale" => "OR | Jefferson"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Josephine",
			"label" => "Josephine ( OR )",
			"locale" => "OR | Josephine"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Klamath",
			"label" => "Klamath ( OR )",
			"locale" => "OR | Klamath"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Lake",
			"label" => "Lake ( OR )",
			"locale" => "OR | Lake"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Lane",
			"label" => "Lane ( OR )",
			"locale" => "OR | Lane"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Lincoln",
			"label" => "Lincoln ( OR )",
			"locale" => "OR | Lincoln"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Linn",
			"label" => "Linn ( OR )",
			"locale" => "OR | Linn"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Malheur",
			"label" => "Malheur ( OR )",
			"locale" => "OR | Malheur"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Marion",
			"label" => "Marion ( OR )",
			"locale" => "OR | Marion"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Morrow",
			"label" => "Morrow ( OR )",
			"locale" => "OR | Morrow"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Multnomah",
			"label" => "Multnomah ( OR )",
			"locale" => "OR | Multnomah"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Polk",
			"label" => "Polk ( OR )",
			"locale" => "OR | Polk"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Sherman",
			"label" => "Sherman ( OR )",
			"locale" => "OR | Sherman"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Tillamook",
			"label" => "Tillamook ( OR )",
			"locale" => "OR | Tillamook"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Umatilla",
			"label" => "Umatilla ( OR )",
			"locale" => "OR | Umatilla"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Union",
			"label" => "Union ( OR )",
			"locale" => "OR | Union"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Wallowa",
			"label" => "Wallowa ( OR )",
			"locale" => "OR | Wallowa"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Wasco",
			"label" => "Wasco ( OR )",
			"locale" => "OR | Wasco"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( OR )",
			"locale" => "OR | Washington"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Wheeler",
			"label" => "Wheeler ( OR )",
			"locale" => "OR | Wheeler"
		]);
		County::create([
			"state_id" => 38,
			"location_id" => 0,
			"county" => "Yamhill",
			"label" => "Yamhill ( OR )",
			"locale" => "OR | Yamhill"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Adams",
			"label" => "Adams ( PA )",
			"locale" => "PA | Adams"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Allegheny",
			"label" => "Allegheny ( PA )",
			"locale" => "PA | Allegheny"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Armstrong",
			"label" => "Armstrong ( PA )",
			"locale" => "PA | Armstrong"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Beaver",
			"label" => "Beaver ( PA )",
			"locale" => "PA | Beaver"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Bedford",
			"label" => "Bedford ( PA )",
			"locale" => "PA | Bedford"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Berks",
			"label" => "Berks ( PA )",
			"locale" => "PA | Berks"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Blair",
			"label" => "Blair ( PA )",
			"locale" => "PA | Blair"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Bradford",
			"label" => "Bradford ( PA )",
			"locale" => "PA | Bradford"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Bucks",
			"label" => "Bucks ( PA )",
			"locale" => "PA | Bucks"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Butler",
			"label" => "Butler ( PA )",
			"locale" => "PA | Butler"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Cambria",
			"label" => "Cambria ( PA )",
			"locale" => "PA | Cambria"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Cameron",
			"label" => "Cameron ( PA )",
			"locale" => "PA | Cameron"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Carbon",
			"label" => "Carbon ( PA )",
			"locale" => "PA | Carbon"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Centre",
			"label" => "Centre ( PA )",
			"locale" => "PA | Centre"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Chester",
			"label" => "Chester ( PA )",
			"locale" => "PA | Chester"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Clarion",
			"label" => "Clarion ( PA )",
			"locale" => "PA | Clarion"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Clearfield",
			"label" => "Clearfield ( PA )",
			"locale" => "PA | Clearfield"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Clinton",
			"label" => "Clinton ( PA )",
			"locale" => "PA | Clinton"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Columbia",
			"label" => "Columbia ( PA )",
			"locale" => "PA | Columbia"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Crawford",
			"label" => "Crawford ( PA )",
			"locale" => "PA | Crawford"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Cumberland",
			"label" => "Cumberland ( PA )",
			"locale" => "PA | Cumberland"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Dauphin",
			"label" => "Dauphin ( PA )",
			"locale" => "PA | Dauphin"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Delaware",
			"label" => "Delaware ( PA )",
			"locale" => "PA | Delaware"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Elk",
			"label" => "Elk ( PA )",
			"locale" => "PA | Elk"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Erie",
			"label" => "Erie ( PA )",
			"locale" => "PA | Erie"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Fayette",
			"label" => "Fayette ( PA )",
			"locale" => "PA | Fayette"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Forest",
			"label" => "Forest ( PA )",
			"locale" => "PA | Forest"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Franklin",
			"label" => "Franklin ( PA )",
			"locale" => "PA | Franklin"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Fulton",
			"label" => "Fulton ( PA )",
			"locale" => "PA | Fulton"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Greene",
			"label" => "Greene ( PA )",
			"locale" => "PA | Greene"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Huntingdon",
			"label" => "Huntingdon ( PA )",
			"locale" => "PA | Huntingdon"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Indiana",
			"label" => "Indiana ( PA )",
			"locale" => "PA | Indiana"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Jefferson",
			"label" => "Jefferson ( PA )",
			"locale" => "PA | Jefferson"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Juniata",
			"label" => "Juniata ( PA )",
			"locale" => "PA | Juniata"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Lackawanna",
			"label" => "Lackawanna ( PA )",
			"locale" => "PA | Lackawanna"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Lancaster",
			"label" => "Lancaster ( PA )",
			"locale" => "PA | Lancaster"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Lawrence",
			"label" => "Lawrence ( PA )",
			"locale" => "PA | Lawrence"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Lebanon",
			"label" => "Lebanon ( PA )",
			"locale" => "PA | Lebanon"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Lehigh",
			"label" => "Lehigh ( PA )",
			"locale" => "PA | Lehigh"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Luzerne",
			"label" => "Luzerne ( PA )",
			"locale" => "PA | Luzerne"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Lycoming",
			"label" => "Lycoming ( PA )",
			"locale" => "PA | Lycoming"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "McKean",
			"label" => "McKean ( PA )",
			"locale" => "PA | McKean"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Mercer",
			"label" => "Mercer ( PA )",
			"locale" => "PA | Mercer"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Mifflin",
			"label" => "Mifflin ( PA )",
			"locale" => "PA | Mifflin"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Monroe",
			"label" => "Monroe ( PA )",
			"locale" => "PA | Monroe"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Montgomery",
			"label" => "Montgomery ( PA )",
			"locale" => "PA | Montgomery"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Montour",
			"label" => "Montour ( PA )",
			"locale" => "PA | Montour"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Northampton",
			"label" => "Northampton ( PA )",
			"locale" => "PA | Northampton"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Northumberland",
			"label" => "Northumberland ( PA )",
			"locale" => "PA | Northumberland"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Perry",
			"label" => "Perry ( PA )",
			"locale" => "PA | Perry"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Pike",
			"label" => "Pike ( PA )",
			"locale" => "PA | Pike"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Potter",
			"label" => "Potter ( PA )",
			"locale" => "PA | Potter"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Schuylkill",
			"label" => "Schuylkill ( PA )",
			"locale" => "PA | Schuylkill"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Snyder",
			"label" => "Snyder ( PA )",
			"locale" => "PA | Snyder"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Somerset",
			"label" => "Somerset ( PA )",
			"locale" => "PA | Somerset"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Sullivan",
			"label" => "Sullivan ( PA )",
			"locale" => "PA | Sullivan"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Susquehanna",
			"label" => "Susquehanna ( PA )",
			"locale" => "PA | Susquehanna"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Tioga",
			"label" => "Tioga ( PA )",
			"locale" => "PA | Tioga"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Union",
			"label" => "Union ( PA )",
			"locale" => "PA | Union"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Venango",
			"label" => "Venango ( PA )",
			"locale" => "PA | Venango"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Warren",
			"label" => "Warren ( PA )",
			"locale" => "PA | Warren"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( PA )",
			"locale" => "PA | Washington"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Wayne",
			"label" => "Wayne ( PA )",
			"locale" => "PA | Wayne"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Westmoreland",
			"label" => "Westmoreland ( PA )",
			"locale" => "PA | Westmoreland"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "Wyoming",
			"label" => "Wyoming ( PA )",
			"locale" => "PA | Wyoming"
		]);
		County::create([
			"state_id" => 39,
			"location_id" => 0,
			"county" => "York",
			"label" => "York ( PA )",
			"locale" => "PA | York"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Abbeville",
			"label" => "Abbeville ( SC )",
			"locale" => "SC | Abbeville"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Aiken",
			"label" => "Aiken ( SC )",
			"locale" => "SC | Aiken"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Allendale",
			"label" => "Allendale ( SC )",
			"locale" => "SC | Allendale"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Anderson",
			"label" => "Anderson ( SC )",
			"locale" => "SC | Anderson"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Bamberg",
			"label" => "Bamberg ( SC )",
			"locale" => "SC | Bamberg"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Barnwell",
			"label" => "Barnwell ( SC )",
			"locale" => "SC | Barnwell"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Beaufort",
			"label" => "Beaufort ( SC )",
			"locale" => "SC | Beaufort"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Berkeley",
			"label" => "Berkeley ( SC )",
			"locale" => "SC | Berkeley"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Calhoun",
			"label" => "Calhoun ( SC )",
			"locale" => "SC | Calhoun"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Charleston",
			"label" => "Charleston ( SC )",
			"locale" => "SC | Charleston"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Cherokee",
			"label" => "Cherokee ( SC )",
			"locale" => "SC | Cherokee"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Chester",
			"label" => "Chester ( SC )",
			"locale" => "SC | Chester"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Chesterfield",
			"label" => "Chesterfield ( SC )",
			"locale" => "SC | Chesterfield"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Clarendon",
			"label" => "Clarendon ( SC )",
			"locale" => "SC | Clarendon"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Colleton",
			"label" => "Colleton ( SC )",
			"locale" => "SC | Colleton"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Darlington",
			"label" => "Darlington ( SC )",
			"locale" => "SC | Darlington"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Dillon",
			"label" => "Dillon ( SC )",
			"locale" => "SC | Dillon"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Dorchester",
			"label" => "Dorchester ( SC )",
			"locale" => "SC | Dorchester"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Edgefield",
			"label" => "Edgefield ( SC )",
			"locale" => "SC | Edgefield"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Fairfield",
			"label" => "Fairfield ( SC )",
			"locale" => "SC | Fairfield"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Florence",
			"label" => "Florence ( SC )",
			"locale" => "SC | Florence"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Georgetown",
			"label" => "Georgetown ( SC )",
			"locale" => "SC | Georgetown"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Greenville",
			"label" => "Greenville ( SC )",
			"locale" => "SC | Greenville"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Greenwood",
			"label" => "Greenwood ( SC )",
			"locale" => "SC | Greenwood"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Hampton",
			"label" => "Hampton ( SC )",
			"locale" => "SC | Hampton"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Horry",
			"label" => "Horry ( SC )",
			"locale" => "SC | Horry"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Jasper",
			"label" => "Jasper ( SC )",
			"locale" => "SC | Jasper"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Kershaw",
			"label" => "Kershaw ( SC )",
			"locale" => "SC | Kershaw"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Lancaster",
			"label" => "Lancaster ( SC )",
			"locale" => "SC | Lancaster"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Laurens",
			"label" => "Laurens ( SC )",
			"locale" => "SC | Laurens"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Lee",
			"label" => "Lee ( SC )",
			"locale" => "SC | Lee"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Lexington",
			"label" => "Lexington ( SC )",
			"locale" => "SC | Lexington"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Marion",
			"label" => "Marion ( SC )",
			"locale" => "SC | Marion"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Marlboro",
			"label" => "Marlboro ( SC )",
			"locale" => "SC | Marlboro"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "McCormick",
			"label" => "McCormick ( SC )",
			"locale" => "SC | McCormick"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Newberry",
			"label" => "Newberry ( SC )",
			"locale" => "SC | Newberry"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Oconee",
			"label" => "Oconee ( SC )",
			"locale" => "SC | Oconee"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Orangeburg",
			"label" => "Orangeburg ( SC )",
			"locale" => "SC | Orangeburg"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Pickens",
			"label" => "Pickens ( SC )",
			"locale" => "SC | Pickens"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Richland",
			"label" => "Richland ( SC )",
			"locale" => "SC | Richland"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Saluda",
			"label" => "Saluda ( SC )",
			"locale" => "SC | Saluda"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Spartanburg",
			"label" => "Spartanburg ( SC )",
			"locale" => "SC | Spartanburg"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Sumter",
			"label" => "Sumter ( SC )",
			"locale" => "SC | Sumter"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Union",
			"label" => "Union ( SC )",
			"locale" => "SC | Union"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "Williamsburg",
			"label" => "Williamsburg ( SC )",
			"locale" => "SC | Williamsburg"
		]);
		County::create([
			"state_id" => 41,
			"location_id" => 0,
			"county" => "York",
			"label" => "York ( SC )",
			"locale" => "SC | York"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Aurora",
			"label" => "Aurora ( SD )",
			"locale" => "SD | Aurora"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Beadle",
			"label" => "Beadle ( SD )",
			"locale" => "SD | Beadle"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Bennett",
			"label" => "Bennett ( SD )",
			"locale" => "SD | Bennett"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Bon Homme",
			"label" => "Bon Homme ( SD )",
			"locale" => "SD | Bon Homme"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Brookings",
			"label" => "Brookings ( SD )",
			"locale" => "SD | Brookings"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Brown",
			"label" => "Brown ( SD )",
			"locale" => "SD | Brown"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Brule",
			"label" => "Brule ( SD )",
			"locale" => "SD | Brule"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Buffalo",
			"label" => "Buffalo ( SD )",
			"locale" => "SD | Buffalo"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Butte",
			"label" => "Butte ( SD )",
			"locale" => "SD | Butte"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Campbell",
			"label" => "Campbell ( SD )",
			"locale" => "SD | Campbell"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Charles Mix",
			"label" => "Charles Mix ( SD )",
			"locale" => "SD | Charles Mix"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Clark",
			"label" => "Clark ( SD )",
			"locale" => "SD | Clark"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Clay",
			"label" => "Clay ( SD )",
			"locale" => "SD | Clay"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Codington",
			"label" => "Codington ( SD )",
			"locale" => "SD | Codington"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Corson",
			"label" => "Corson ( SD )",
			"locale" => "SD | Corson"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Custer",
			"label" => "Custer ( SD )",
			"locale" => "SD | Custer"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Davison",
			"label" => "Davison ( SD )",
			"locale" => "SD | Davison"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Day",
			"label" => "Day ( SD )",
			"locale" => "SD | Day"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Deuel",
			"label" => "Deuel ( SD )",
			"locale" => "SD | Deuel"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Dewey",
			"label" => "Dewey ( SD )",
			"locale" => "SD | Dewey"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Douglas",
			"label" => "Douglas ( SD )",
			"locale" => "SD | Douglas"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Edmunds",
			"label" => "Edmunds ( SD )",
			"locale" => "SD | Edmunds"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Fall River",
			"label" => "Fall River ( SD )",
			"locale" => "SD | Fall River"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Faulk",
			"label" => "Faulk ( SD )",
			"locale" => "SD | Faulk"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Grant",
			"label" => "Grant ( SD )",
			"locale" => "SD | Grant"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Gregory",
			"label" => "Gregory ( SD )",
			"locale" => "SD | Gregory"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Haakon",
			"label" => "Haakon ( SD )",
			"locale" => "SD | Haakon"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Hamlin",
			"label" => "Hamlin ( SD )",
			"locale" => "SD | Hamlin"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Hand",
			"label" => "Hand ( SD )",
			"locale" => "SD | Hand"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Hanson",
			"label" => "Hanson ( SD )",
			"locale" => "SD | Hanson"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Harding",
			"label" => "Harding ( SD )",
			"locale" => "SD | Harding"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Hughes",
			"label" => "Hughes ( SD )",
			"locale" => "SD | Hughes"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Hutchinson",
			"label" => "Hutchinson ( SD )",
			"locale" => "SD | Hutchinson"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Hyde",
			"label" => "Hyde ( SD )",
			"locale" => "SD | Hyde"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Jackson",
			"label" => "Jackson ( SD )",
			"locale" => "SD | Jackson"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Jerauld",
			"label" => "Jerauld ( SD )",
			"locale" => "SD | Jerauld"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Jones",
			"label" => "Jones ( SD )",
			"locale" => "SD | Jones"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Kingsbury",
			"label" => "Kingsbury ( SD )",
			"locale" => "SD | Kingsbury"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Lake",
			"label" => "Lake ( SD )",
			"locale" => "SD | Lake"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Lawrence",
			"label" => "Lawrence ( SD )",
			"locale" => "SD | Lawrence"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Lincoln",
			"label" => "Lincoln ( SD )",
			"locale" => "SD | Lincoln"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Lyman",
			"label" => "Lyman ( SD )",
			"locale" => "SD | Lyman"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Marshall",
			"label" => "Marshall ( SD )",
			"locale" => "SD | Marshall"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "McCook",
			"label" => "McCook ( SD )",
			"locale" => "SD | McCook"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "McPherson",
			"label" => "McPherson ( SD )",
			"locale" => "SD | McPherson"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Meade",
			"label" => "Meade ( SD )",
			"locale" => "SD | Meade"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Mellette",
			"label" => "Mellette ( SD )",
			"locale" => "SD | Mellette"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Miner",
			"label" => "Miner ( SD )",
			"locale" => "SD | Miner"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Minnehaha",
			"label" => "Minnehaha ( SD )",
			"locale" => "SD | Minnehaha"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Moody",
			"label" => "Moody ( SD )",
			"locale" => "SD | Moody"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Pennington",
			"label" => "Pennington ( SD )",
			"locale" => "SD | Pennington"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Perkins",
			"label" => "Perkins ( SD )",
			"locale" => "SD | Perkins"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Potter",
			"label" => "Potter ( SD )",
			"locale" => "SD | Potter"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Roberts",
			"label" => "Roberts ( SD )",
			"locale" => "SD | Roberts"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Sanborn",
			"label" => "Sanborn ( SD )",
			"locale" => "SD | Sanborn"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Shannon",
			"label" => "Shannon ( SD )",
			"locale" => "SD | Shannon"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Spink",
			"label" => "Spink ( SD )",
			"locale" => "SD | Spink"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Stanley",
			"label" => "Stanley ( SD )",
			"locale" => "SD | Stanley"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Sully",
			"label" => "Sully ( SD )",
			"locale" => "SD | Sully"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Todd",
			"label" => "Todd ( SD )",
			"locale" => "SD | Todd"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Tripp",
			"label" => "Tripp ( SD )",
			"locale" => "SD | Tripp"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Turner",
			"label" => "Turner ( SD )",
			"locale" => "SD | Turner"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Union",
			"label" => "Union ( SD )",
			"locale" => "SD | Union"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Walworth",
			"label" => "Walworth ( SD )",
			"locale" => "SD | Walworth"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Yankton",
			"label" => "Yankton ( SD )",
			"locale" => "SD | Yankton"
		]);
		County::create([
			"state_id" => 42,
			"location_id" => 0,
			"county" => "Ziebach",
			"label" => "Ziebach ( SD )",
			"locale" => "SD | Ziebach"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Anderson",
			"label" => "Anderson ( TN )",
			"locale" => "TN | Anderson"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Bedford",
			"label" => "Bedford ( TN )",
			"locale" => "TN | Bedford"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Benton",
			"label" => "Benton ( TN )",
			"locale" => "TN | Benton"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Bledsoe",
			"label" => "Bledsoe ( TN )",
			"locale" => "TN | Bledsoe"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Blount",
			"label" => "Blount ( TN )",
			"locale" => "TN | Blount"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Bradley",
			"label" => "Bradley ( TN )",
			"locale" => "TN | Bradley"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Campbell",
			"label" => "Campbell ( TN )",
			"locale" => "TN | Campbell"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Cannon",
			"label" => "Cannon ( TN )",
			"locale" => "TN | Cannon"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Carroll",
			"label" => "Carroll ( TN )",
			"locale" => "TN | Carroll"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Carter",
			"label" => "Carter ( TN )",
			"locale" => "TN | Carter"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Cheatham",
			"label" => "Cheatham ( TN )",
			"locale" => "TN | Cheatham"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Chester",
			"label" => "Chester ( TN )",
			"locale" => "TN | Chester"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Claiborne",
			"label" => "Claiborne ( TN )",
			"locale" => "TN | Claiborne"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Clay",
			"label" => "Clay ( TN )",
			"locale" => "TN | Clay"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Cocke",
			"label" => "Cocke ( TN )",
			"locale" => "TN | Cocke"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Coffee",
			"label" => "Coffee ( TN )",
			"locale" => "TN | Coffee"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Crockett",
			"label" => "Crockett ( TN )",
			"locale" => "TN | Crockett"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Cumberland",
			"label" => "Cumberland ( TN )",
			"locale" => "TN | Cumberland"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Decatur",
			"label" => "Decatur ( TN )",
			"locale" => "TN | Decatur"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "DeKalb",
			"label" => "DeKalb ( TN )",
			"locale" => "TN | DeKalb"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Dickson",
			"label" => "Dickson ( TN )",
			"locale" => "TN | Dickson"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Dyer",
			"label" => "Dyer ( TN )",
			"locale" => "TN | Dyer"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Fayette",
			"label" => "Fayette ( TN )",
			"locale" => "TN | Fayette"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Fentress",
			"label" => "Fentress ( TN )",
			"locale" => "TN | Fentress"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Franklin",
			"label" => "Franklin ( TN )",
			"locale" => "TN | Franklin"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Gibson",
			"label" => "Gibson ( TN )",
			"locale" => "TN | Gibson"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Giles",
			"label" => "Giles ( TN )",
			"locale" => "TN | Giles"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Grainger",
			"label" => "Grainger ( TN )",
			"locale" => "TN | Grainger"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Greene",
			"label" => "Greene ( TN )",
			"locale" => "TN | Greene"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Grundy",
			"label" => "Grundy ( TN )",
			"locale" => "TN | Grundy"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Hamblen",
			"label" => "Hamblen ( TN )",
			"locale" => "TN | Hamblen"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Hamilton",
			"label" => "Hamilton ( TN )",
			"locale" => "TN | Hamilton"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Hancock",
			"label" => "Hancock ( TN )",
			"locale" => "TN | Hancock"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Hardeman",
			"label" => "Hardeman ( TN )",
			"locale" => "TN | Hardeman"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Hardin",
			"label" => "Hardin ( TN )",
			"locale" => "TN | Hardin"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Hawkins",
			"label" => "Hawkins ( TN )",
			"locale" => "TN | Hawkins"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Haywood",
			"label" => "Haywood ( TN )",
			"locale" => "TN | Haywood"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Henderson",
			"label" => "Henderson ( TN )",
			"locale" => "TN | Henderson"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Henry",
			"label" => "Henry ( TN )",
			"locale" => "TN | Henry"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Hickman",
			"label" => "Hickman ( TN )",
			"locale" => "TN | Hickman"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Houston",
			"label" => "Houston ( TN )",
			"locale" => "TN | Houston"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Humphreys",
			"label" => "Humphreys ( TN )",
			"locale" => "TN | Humphreys"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Jackson",
			"label" => "Jackson ( TN )",
			"locale" => "TN | Jackson"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Jefferson",
			"label" => "Jefferson ( TN )",
			"locale" => "TN | Jefferson"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Johnson",
			"label" => "Johnson ( TN )",
			"locale" => "TN | Johnson"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Knox",
			"label" => "Knox ( TN )",
			"locale" => "TN | Knox"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Lake",
			"label" => "Lake ( TN )",
			"locale" => "TN | Lake"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Lauderdale",
			"label" => "Lauderdale ( TN )",
			"locale" => "TN | Lauderdale"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Lawrence",
			"label" => "Lawrence ( TN )",
			"locale" => "TN | Lawrence"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Lewis",
			"label" => "Lewis ( TN )",
			"locale" => "TN | Lewis"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Lincoln",
			"label" => "Lincoln ( TN )",
			"locale" => "TN | Lincoln"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Loudon",
			"label" => "Loudon ( TN )",
			"locale" => "TN | Loudon"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Macon",
			"label" => "Macon ( TN )",
			"locale" => "TN | Macon"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Madison",
			"label" => "Madison ( TN )",
			"locale" => "TN | Madison"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Marion",
			"label" => "Marion ( TN )",
			"locale" => "TN | Marion"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Marshall",
			"label" => "Marshall ( TN )",
			"locale" => "TN | Marshall"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Maury",
			"label" => "Maury ( TN )",
			"locale" => "TN | Maury"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "McMinn",
			"label" => "McMinn ( TN )",
			"locale" => "TN | McMinn"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "McNairy",
			"label" => "McNairy ( TN )",
			"locale" => "TN | McNairy"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Meigs",
			"label" => "Meigs ( TN )",
			"locale" => "TN | Meigs"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Monroe",
			"label" => "Monroe ( TN )",
			"locale" => "TN | Monroe"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Montgomery",
			"label" => "Montgomery ( TN )",
			"locale" => "TN | Montgomery"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Morgan",
			"label" => "Morgan ( TN )",
			"locale" => "TN | Morgan"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Obion",
			"label" => "Obion ( TN )",
			"locale" => "TN | Obion"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Overton",
			"label" => "Overton ( TN )",
			"locale" => "TN | Overton"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Perry",
			"label" => "Perry ( TN )",
			"locale" => "TN | Perry"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Pickett",
			"label" => "Pickett ( TN )",
			"locale" => "TN | Pickett"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Polk",
			"label" => "Polk ( TN )",
			"locale" => "TN | Polk"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Putnam",
			"label" => "Putnam ( TN )",
			"locale" => "TN | Putnam"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Rhea",
			"label" => "Rhea ( TN )",
			"locale" => "TN | Rhea"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Roane",
			"label" => "Roane ( TN )",
			"locale" => "TN | Roane"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Robertson",
			"label" => "Robertson ( TN )",
			"locale" => "TN | Robertson"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Rutherford",
			"label" => "Rutherford ( TN )",
			"locale" => "TN | Rutherford"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Scott",
			"label" => "Scott ( TN )",
			"locale" => "TN | Scott"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Sequatchie",
			"label" => "Sequatchie ( TN )",
			"locale" => "TN | Sequatchie"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Sevier",
			"label" => "Sevier ( TN )",
			"locale" => "TN | Sevier"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 6,
			"county" => "Shelby",
			"label" => "Shelby ( TN )",
			"locale" => "TN | Shelby"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Smith",
			"label" => "Smith ( TN )",
			"locale" => "TN | Smith"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Stewart",
			"label" => "Stewart ( TN )",
			"locale" => "TN | Stewart"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Sullivan",
			"label" => "Sullivan ( TN )",
			"locale" => "TN | Sullivan"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Sumner",
			"label" => "Sumner ( TN )",
			"locale" => "TN | Sumner"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Tipton",
			"label" => "Tipton ( TN )",
			"locale" => "TN | Tipton"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Unicoi",
			"label" => "Unicoi ( TN )",
			"locale" => "TN | Unicoi"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Union",
			"label" => "Union ( TN )",
			"locale" => "TN | Union"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Van Buren",
			"label" => "Van Buren ( TN )",
			"locale" => "TN | Van Buren"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Warren",
			"label" => "Warren ( TN )",
			"locale" => "TN | Warren"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( TN )",
			"locale" => "TN | Washington"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Wayne",
			"label" => "Wayne ( TN )",
			"locale" => "TN | Wayne"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Weakley",
			"label" => "Weakley ( TN )",
			"locale" => "TN | Weakley"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "White",
			"label" => "White ( TN )",
			"locale" => "TN | White"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Williamson",
			"label" => "Williamson ( TN )",
			"locale" => "TN | Williamson"
		]);
		County::create([
			"state_id" => 43,
			"location_id" => 0,
			"county" => "Wilson",
			"label" => "Wilson ( TN )",
			"locale" => "TN | Wilson"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Anderson",
			"label" => "Anderson ( TX )",
			"locale" => "TX | Anderson"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Andrews",
			"label" => "Andrews ( TX )",
			"locale" => "TX | Andrews"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Angelina",
			"label" => "Angelina ( TX )",
			"locale" => "TX | Angelina"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Aransas",
			"label" => "Aransas ( TX )",
			"locale" => "TX | Aransas"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Archer",
			"label" => "Archer ( TX )",
			"locale" => "TX | Archer"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Armstrong",
			"label" => "Armstrong ( TX )",
			"locale" => "TX | Armstrong"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Atascosa",
			"label" => "Atascosa ( TX )",
			"locale" => "TX | Atascosa"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Austin",
			"label" => "Austin ( TX )",
			"locale" => "TX | Austin"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Bailey",
			"label" => "Bailey ( TX )",
			"locale" => "TX | Bailey"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Bandera",
			"label" => "Bandera ( TX )",
			"locale" => "TX | Bandera"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Bastrop",
			"label" => "Bastrop ( TX )",
			"locale" => "TX | Bastrop"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Baylor",
			"label" => "Baylor ( TX )",
			"locale" => "TX | Baylor"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Bee",
			"label" => "Bee ( TX )",
			"locale" => "TX | Bee"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Bell",
			"label" => "Bell ( TX )",
			"locale" => "TX | Bell"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Bexar",
			"label" => "Bexar ( TX )",
			"locale" => "TX | Bexar"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Blanco",
			"label" => "Blanco ( TX )",
			"locale" => "TX | Blanco"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Borden",
			"label" => "Borden ( TX )",
			"locale" => "TX | Borden"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Bosque",
			"label" => "Bosque ( TX )",
			"locale" => "TX | Bosque"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Bowie",
			"label" => "Bowie ( TX )",
			"locale" => "TX | Bowie"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Brazoria",
			"label" => "Brazoria ( TX )",
			"locale" => "TX | Brazoria"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Brazos",
			"label" => "Brazos ( TX )",
			"locale" => "TX | Brazos"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Brewster",
			"label" => "Brewster ( TX )",
			"locale" => "TX | Brewster"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Briscoe",
			"label" => "Briscoe ( TX )",
			"locale" => "TX | Briscoe"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Brooks",
			"label" => "Brooks ( TX )",
			"locale" => "TX | Brooks"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Brown",
			"label" => "Brown ( TX )",
			"locale" => "TX | Brown"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Burleson",
			"label" => "Burleson ( TX )",
			"locale" => "TX | Burleson"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Burnet",
			"label" => "Burnet ( TX )",
			"locale" => "TX | Burnet"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Caldwell",
			"label" => "Caldwell ( TX )",
			"locale" => "TX | Caldwell"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Calhoun",
			"label" => "Calhoun ( TX )",
			"locale" => "TX | Calhoun"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Callahan",
			"label" => "Callahan ( TX )",
			"locale" => "TX | Callahan"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Cameron",
			"label" => "Cameron ( TX )",
			"locale" => "TX | Cameron"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Camp",
			"label" => "Camp ( TX )",
			"locale" => "TX | Camp"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Carson",
			"label" => "Carson ( TX )",
			"locale" => "TX | Carson"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Cass",
			"label" => "Cass ( TX )",
			"locale" => "TX | Cass"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Castro",
			"label" => "Castro ( TX )",
			"locale" => "TX | Castro"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Chambers",
			"label" => "Chambers ( TX )",
			"locale" => "TX | Chambers"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Cherokee",
			"label" => "Cherokee ( TX )",
			"locale" => "TX | Cherokee"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Childress",
			"label" => "Childress ( TX )",
			"locale" => "TX | Childress"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Clay",
			"label" => "Clay ( TX )",
			"locale" => "TX | Clay"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Cochran",
			"label" => "Cochran ( TX )",
			"locale" => "TX | Cochran"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Coke",
			"label" => "Coke ( TX )",
			"locale" => "TX | Coke"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Coleman",
			"label" => "Coleman ( TX )",
			"locale" => "TX | Coleman"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Collin",
			"label" => "Collin ( TX )",
			"locale" => "TX | Collin"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Collingsworth",
			"label" => "Collingsworth ( TX )",
			"locale" => "TX | Collingsworth"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Colorado",
			"label" => "Colorado ( TX )",
			"locale" => "TX | Colorado"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Comal",
			"label" => "Comal ( TX )",
			"locale" => "TX | Comal"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Comanche",
			"label" => "Comanche ( TX )",
			"locale" => "TX | Comanche"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Concho",
			"label" => "Concho ( TX )",
			"locale" => "TX | Concho"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Cooke",
			"label" => "Cooke ( TX )",
			"locale" => "TX | Cooke"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Coryell",
			"label" => "Coryell ( TX )",
			"locale" => "TX | Coryell"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Cottle",
			"label" => "Cottle ( TX )",
			"locale" => "TX | Cottle"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Crane",
			"label" => "Crane ( TX )",
			"locale" => "TX | Crane"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Crockett",
			"label" => "Crockett ( TX )",
			"locale" => "TX | Crockett"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Crosby",
			"label" => "Crosby ( TX )",
			"locale" => "TX | Crosby"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Culberson",
			"label" => "Culberson ( TX )",
			"locale" => "TX | Culberson"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Dallam",
			"label" => "Dallam ( TX )",
			"locale" => "TX | Dallam"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Dallas",
			"label" => "Dallas ( TX )",
			"locale" => "TX | Dallas"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Dawson",
			"label" => "Dawson ( TX )",
			"locale" => "TX | Dawson"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Deaf Smith",
			"label" => "Deaf Smith ( TX )",
			"locale" => "TX | Deaf Smith"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Delta",
			"label" => "Delta ( TX )",
			"locale" => "TX | Delta"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Denton",
			"label" => "Denton ( TX )",
			"locale" => "TX | Denton"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "DeWitt",
			"label" => "DeWitt ( TX )",
			"locale" => "TX | DeWitt"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Dickens",
			"label" => "Dickens ( TX )",
			"locale" => "TX | Dickens"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Dimmit",
			"label" => "Dimmit ( TX )",
			"locale" => "TX | Dimmit"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Donley",
			"label" => "Donley ( TX )",
			"locale" => "TX | Donley"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Duval",
			"label" => "Duval ( TX )",
			"locale" => "TX | Duval"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Eastland",
			"label" => "Eastland ( TX )",
			"locale" => "TX | Eastland"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Ector",
			"label" => "Ector ( TX )",
			"locale" => "TX | Ector"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Edwards",
			"label" => "Edwards ( TX )",
			"locale" => "TX | Edwards"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "El Paso",
			"label" => "El Paso ( TX )",
			"locale" => "TX | El Paso"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Ellis",
			"label" => "Ellis ( TX )",
			"locale" => "TX | Ellis"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Erath",
			"label" => "Erath ( TX )",
			"locale" => "TX | Erath"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Falls",
			"label" => "Falls ( TX )",
			"locale" => "TX | Falls"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Fannin",
			"label" => "Fannin ( TX )",
			"locale" => "TX | Fannin"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Fayette",
			"label" => "Fayette ( TX )",
			"locale" => "TX | Fayette"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Fisher",
			"label" => "Fisher ( TX )",
			"locale" => "TX | Fisher"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Floyd",
			"label" => "Floyd ( TX )",
			"locale" => "TX | Floyd"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Foard",
			"label" => "Foard ( TX )",
			"locale" => "TX | Foard"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Fort Bend",
			"label" => "Fort Bend ( TX )",
			"locale" => "TX | Fort Bend"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Franklin",
			"label" => "Franklin ( TX )",
			"locale" => "TX | Franklin"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Freestone",
			"label" => "Freestone ( TX )",
			"locale" => "TX | Freestone"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Frio",
			"label" => "Frio ( TX )",
			"locale" => "TX | Frio"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Gaines",
			"label" => "Gaines ( TX )",
			"locale" => "TX | Gaines"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Galveston",
			"label" => "Galveston ( TX )",
			"locale" => "TX | Galveston"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Garza",
			"label" => "Garza ( TX )",
			"locale" => "TX | Garza"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Gillespie",
			"label" => "Gillespie ( TX )",
			"locale" => "TX | Gillespie"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Glasscock",
			"label" => "Glasscock ( TX )",
			"locale" => "TX | Glasscock"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Goliad",
			"label" => "Goliad ( TX )",
			"locale" => "TX | Goliad"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Gonzales",
			"label" => "Gonzales ( TX )",
			"locale" => "TX | Gonzales"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Gray",
			"label" => "Gray ( TX )",
			"locale" => "TX | Gray"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Grayson",
			"label" => "Grayson ( TX )",
			"locale" => "TX | Grayson"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Gregg",
			"label" => "Gregg ( TX )",
			"locale" => "TX | Gregg"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Grimes",
			"label" => "Grimes ( TX )",
			"locale" => "TX | Grimes"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Guadalupe",
			"label" => "Guadalupe ( TX )",
			"locale" => "TX | Guadalupe"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Hale",
			"label" => "Hale ( TX )",
			"locale" => "TX | Hale"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Hall",
			"label" => "Hall ( TX )",
			"locale" => "TX | Hall"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Hamilton",
			"label" => "Hamilton ( TX )",
			"locale" => "TX | Hamilton"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Hansford",
			"label" => "Hansford ( TX )",
			"locale" => "TX | Hansford"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Hardeman",
			"label" => "Hardeman ( TX )",
			"locale" => "TX | Hardeman"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Hardin",
			"label" => "Hardin ( TX )",
			"locale" => "TX | Hardin"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Harris",
			"label" => "Harris ( TX )",
			"locale" => "TX | Harris"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Harrison",
			"label" => "Harrison ( TX )",
			"locale" => "TX | Harrison"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Hartley",
			"label" => "Hartley ( TX )",
			"locale" => "TX | Hartley"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Haskell",
			"label" => "Haskell ( TX )",
			"locale" => "TX | Haskell"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Hays",
			"label" => "Hays ( TX )",
			"locale" => "TX | Hays"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Hemphill",
			"label" => "Hemphill ( TX )",
			"locale" => "TX | Hemphill"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Henderson",
			"label" => "Henderson ( TX )",
			"locale" => "TX | Henderson"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Hidalgo",
			"label" => "Hidalgo ( TX )",
			"locale" => "TX | Hidalgo"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Hill",
			"label" => "Hill ( TX )",
			"locale" => "TX | Hill"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Hockley",
			"label" => "Hockley ( TX )",
			"locale" => "TX | Hockley"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Hood",
			"label" => "Hood ( TX )",
			"locale" => "TX | Hood"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Hopkins",
			"label" => "Hopkins ( TX )",
			"locale" => "TX | Hopkins"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Houston",
			"label" => "Houston ( TX )",
			"locale" => "TX | Houston"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Howard",
			"label" => "Howard ( TX )",
			"locale" => "TX | Howard"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Hudspeth",
			"label" => "Hudspeth ( TX )",
			"locale" => "TX | Hudspeth"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Hunt",
			"label" => "Hunt ( TX )",
			"locale" => "TX | Hunt"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Hutchinson",
			"label" => "Hutchinson ( TX )",
			"locale" => "TX | Hutchinson"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Irion",
			"label" => "Irion ( TX )",
			"locale" => "TX | Irion"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Jack",
			"label" => "Jack ( TX )",
			"locale" => "TX | Jack"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Jackson",
			"label" => "Jackson ( TX )",
			"locale" => "TX | Jackson"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Jasper",
			"label" => "Jasper ( TX )",
			"locale" => "TX | Jasper"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Jeff Davis",
			"label" => "Jeff Davis ( TX )",
			"locale" => "TX | Jeff Davis"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Jefferson",
			"label" => "Jefferson ( TX )",
			"locale" => "TX | Jefferson"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Jim Hogg",
			"label" => "Jim Hogg ( TX )",
			"locale" => "TX | Jim Hogg"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Jim Wells",
			"label" => "Jim Wells ( TX )",
			"locale" => "TX | Jim Wells"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Johnson",
			"label" => "Johnson ( TX )",
			"locale" => "TX | Johnson"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Jones",
			"label" => "Jones ( TX )",
			"locale" => "TX | Jones"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Karnes",
			"label" => "Karnes ( TX )",
			"locale" => "TX | Karnes"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Kaufman",
			"label" => "Kaufman ( TX )",
			"locale" => "TX | Kaufman"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Kendall",
			"label" => "Kendall ( TX )",
			"locale" => "TX | Kendall"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Kenedy",
			"label" => "Kenedy ( TX )",
			"locale" => "TX | Kenedy"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Kent",
			"label" => "Kent ( TX )",
			"locale" => "TX | Kent"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Kerr",
			"label" => "Kerr ( TX )",
			"locale" => "TX | Kerr"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Kimble",
			"label" => "Kimble ( TX )",
			"locale" => "TX | Kimble"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "King",
			"label" => "King ( TX )",
			"locale" => "TX | King"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Kinney",
			"label" => "Kinney ( TX )",
			"locale" => "TX | Kinney"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Kleberg",
			"label" => "Kleberg ( TX )",
			"locale" => "TX | Kleberg"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Knox",
			"label" => "Knox ( TX )",
			"locale" => "TX | Knox"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "La Salle",
			"label" => "La Salle ( TX )",
			"locale" => "TX | La Salle"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Lamar",
			"label" => "Lamar ( TX )",
			"locale" => "TX | Lamar"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Lamb",
			"label" => "Lamb ( TX )",
			"locale" => "TX | Lamb"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Lampasas",
			"label" => "Lampasas ( TX )",
			"locale" => "TX | Lampasas"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Lavaca",
			"label" => "Lavaca ( TX )",
			"locale" => "TX | Lavaca"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Lee",
			"label" => "Lee ( TX )",
			"locale" => "TX | Lee"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Leon",
			"label" => "Leon ( TX )",
			"locale" => "TX | Leon"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Liberty",
			"label" => "Liberty ( TX )",
			"locale" => "TX | Liberty"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Limestone",
			"label" => "Limestone ( TX )",
			"locale" => "TX | Limestone"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Lipscomb",
			"label" => "Lipscomb ( TX )",
			"locale" => "TX | Lipscomb"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Live Oak",
			"label" => "Live Oak ( TX )",
			"locale" => "TX | Live Oak"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Llano",
			"label" => "Llano ( TX )",
			"locale" => "TX | Llano"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Loving",
			"label" => "Loving ( TX )",
			"locale" => "TX | Loving"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Lubbock",
			"label" => "Lubbock ( TX )",
			"locale" => "TX | Lubbock"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Lynn",
			"label" => "Lynn ( TX )",
			"locale" => "TX | Lynn"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Madison",
			"label" => "Madison ( TX )",
			"locale" => "TX | Madison"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Marion",
			"label" => "Marion ( TX )",
			"locale" => "TX | Marion"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Martin",
			"label" => "Martin ( TX )",
			"locale" => "TX | Martin"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Mason",
			"label" => "Mason ( TX )",
			"locale" => "TX | Mason"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Matagorda",
			"label" => "Matagorda ( TX )",
			"locale" => "TX | Matagorda"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Maverick",
			"label" => "Maverick ( TX )",
			"locale" => "TX | Maverick"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "McCulloch",
			"label" => "McCulloch ( TX )",
			"locale" => "TX | McCulloch"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "McLennan",
			"label" => "McLennan ( TX )",
			"locale" => "TX | McLennan"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "McMullen",
			"label" => "McMullen ( TX )",
			"locale" => "TX | McMullen"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Medina",
			"label" => "Medina ( TX )",
			"locale" => "TX | Medina"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Menard",
			"label" => "Menard ( TX )",
			"locale" => "TX | Menard"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Midland",
			"label" => "Midland ( TX )",
			"locale" => "TX | Midland"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Milam",
			"label" => "Milam ( TX )",
			"locale" => "TX | Milam"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Mills",
			"label" => "Mills ( TX )",
			"locale" => "TX | Mills"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Mitchell",
			"label" => "Mitchell ( TX )",
			"locale" => "TX | Mitchell"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Montague",
			"label" => "Montague ( TX )",
			"locale" => "TX | Montague"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Montgomery",
			"label" => "Montgomery ( TX )",
			"locale" => "TX | Montgomery"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Moore",
			"label" => "Moore ( TX )",
			"locale" => "TX | Moore"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Morris",
			"label" => "Morris ( TX )",
			"locale" => "TX | Morris"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Motley",
			"label" => "Motley ( TX )",
			"locale" => "TX | Motley"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Nacogdoches",
			"label" => "Nacogdoches ( TX )",
			"locale" => "TX | Nacogdoches"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Navarro",
			"label" => "Navarro ( TX )",
			"locale" => "TX | Navarro"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Newton",
			"label" => "Newton ( TX )",
			"locale" => "TX | Newton"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Nolan",
			"label" => "Nolan ( TX )",
			"locale" => "TX | Nolan"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Nueces",
			"label" => "Nueces ( TX )",
			"locale" => "TX | Nueces"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Ochiltree",
			"label" => "Ochiltree ( TX )",
			"locale" => "TX | Ochiltree"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Oldham",
			"label" => "Oldham ( TX )",
			"locale" => "TX | Oldham"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Orange",
			"label" => "Orange ( TX )",
			"locale" => "TX | Orange"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Palo Pinto",
			"label" => "Palo Pinto ( TX )",
			"locale" => "TX | Palo Pinto"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Panola",
			"label" => "Panola ( TX )",
			"locale" => "TX | Panola"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Parker",
			"label" => "Parker ( TX )",
			"locale" => "TX | Parker"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Parmer",
			"label" => "Parmer ( TX )",
			"locale" => "TX | Parmer"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Pecos",
			"label" => "Pecos ( TX )",
			"locale" => "TX | Pecos"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Polk",
			"label" => "Polk ( TX )",
			"locale" => "TX | Polk"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Potter",
			"label" => "Potter ( TX )",
			"locale" => "TX | Potter"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Presidio",
			"label" => "Presidio ( TX )",
			"locale" => "TX | Presidio"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Rains",
			"label" => "Rains ( TX )",
			"locale" => "TX | Rains"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Randall",
			"label" => "Randall ( TX )",
			"locale" => "TX | Randall"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Reagan",
			"label" => "Reagan ( TX )",
			"locale" => "TX | Reagan"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Real",
			"label" => "Real ( TX )",
			"locale" => "TX | Real"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Red River",
			"label" => "Red River ( TX )",
			"locale" => "TX | Red River"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Reeves",
			"label" => "Reeves ( TX )",
			"locale" => "TX | Reeves"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Refugio",
			"label" => "Refugio ( TX )",
			"locale" => "TX | Refugio"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Roberts",
			"label" => "Roberts ( TX )",
			"locale" => "TX | Roberts"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Robertson",
			"label" => "Robertson ( TX )",
			"locale" => "TX | Robertson"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Rockwall",
			"label" => "Rockwall ( TX )",
			"locale" => "TX | Rockwall"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Runnels",
			"label" => "Runnels ( TX )",
			"locale" => "TX | Runnels"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Rusk",
			"label" => "Rusk ( TX )",
			"locale" => "TX | Rusk"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Sabine",
			"label" => "Sabine ( TX )",
			"locale" => "TX | Sabine"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "San Augustine",
			"label" => "San Augustine ( TX )",
			"locale" => "TX | San Augustine"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "San Jacinto",
			"label" => "San Jacinto ( TX )",
			"locale" => "TX | San Jacinto"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "San Patricio",
			"label" => "San Patricio ( TX )",
			"locale" => "TX | San Patricio"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "San Saba",
			"label" => "San Saba ( TX )",
			"locale" => "TX | San Saba"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Schleicher",
			"label" => "Schleicher ( TX )",
			"locale" => "TX | Schleicher"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Scurry",
			"label" => "Scurry ( TX )",
			"locale" => "TX | Scurry"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Shackelford",
			"label" => "Shackelford ( TX )",
			"locale" => "TX | Shackelford"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Shelby",
			"label" => "Shelby ( TX )",
			"locale" => "TX | Shelby"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Sherman",
			"label" => "Sherman ( TX )",
			"locale" => "TX | Sherman"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Smith",
			"label" => "Smith ( TX )",
			"locale" => "TX | Smith"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Somervell",
			"label" => "Somervell ( TX )",
			"locale" => "TX | Somervell"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Starr",
			"label" => "Starr ( TX )",
			"locale" => "TX | Starr"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Stephens",
			"label" => "Stephens ( TX )",
			"locale" => "TX | Stephens"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Sterling",
			"label" => "Sterling ( TX )",
			"locale" => "TX | Sterling"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Stonewall",
			"label" => "Stonewall ( TX )",
			"locale" => "TX | Stonewall"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Sutton",
			"label" => "Sutton ( TX )",
			"locale" => "TX | Sutton"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Swisher",
			"label" => "Swisher ( TX )",
			"locale" => "TX | Swisher"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Tarrant",
			"label" => "Tarrant ( TX )",
			"locale" => "TX | Tarrant"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Taylor",
			"label" => "Taylor ( TX )",
			"locale" => "TX | Taylor"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Terrell",
			"label" => "Terrell ( TX )",
			"locale" => "TX | Terrell"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Terry",
			"label" => "Terry ( TX )",
			"locale" => "TX | Terry"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Throckmorton",
			"label" => "Throckmorton ( TX )",
			"locale" => "TX | Throckmorton"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Titus",
			"label" => "Titus ( TX )",
			"locale" => "TX | Titus"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Tom Green",
			"label" => "Tom Green ( TX )",
			"locale" => "TX | Tom Green"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Travis",
			"label" => "Travis ( TX )",
			"locale" => "TX | Travis"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Trinity",
			"label" => "Trinity ( TX )",
			"locale" => "TX | Trinity"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Tyler",
			"label" => "Tyler ( TX )",
			"locale" => "TX | Tyler"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Upshur",
			"label" => "Upshur ( TX )",
			"locale" => "TX | Upshur"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Upton",
			"label" => "Upton ( TX )",
			"locale" => "TX | Upton"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Uvalde",
			"label" => "Uvalde ( TX )",
			"locale" => "TX | Uvalde"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Val Verde",
			"label" => "Val Verde ( TX )",
			"locale" => "TX | Val Verde"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Van Zandt",
			"label" => "Van Zandt ( TX )",
			"locale" => "TX | Van Zandt"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Victoria",
			"label" => "Victoria ( TX )",
			"locale" => "TX | Victoria"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Walker",
			"label" => "Walker ( TX )",
			"locale" => "TX | Walker"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Waller",
			"label" => "Waller ( TX )",
			"locale" => "TX | Waller"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Ward",
			"label" => "Ward ( TX )",
			"locale" => "TX | Ward"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( TX )",
			"locale" => "TX | Washington"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Webb",
			"label" => "Webb ( TX )",
			"locale" => "TX | Webb"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Wharton",
			"label" => "Wharton ( TX )",
			"locale" => "TX | Wharton"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Wheeler",
			"label" => "Wheeler ( TX )",
			"locale" => "TX | Wheeler"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Wichita",
			"label" => "Wichita ( TX )",
			"locale" => "TX | Wichita"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Wilbarger",
			"label" => "Wilbarger ( TX )",
			"locale" => "TX | Wilbarger"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Willacy",
			"label" => "Willacy ( TX )",
			"locale" => "TX | Willacy"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Williamson",
			"label" => "Williamson ( TX )",
			"locale" => "TX | Williamson"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Wilson",
			"label" => "Wilson ( TX )",
			"locale" => "TX | Wilson"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Winkler",
			"label" => "Winkler ( TX )",
			"locale" => "TX | Winkler"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Wise",
			"label" => "Wise ( TX )",
			"locale" => "TX | Wise"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Wood",
			"label" => "Wood ( TX )",
			"locale" => "TX | Wood"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Yoakum",
			"label" => "Yoakum ( TX )",
			"locale" => "TX | Yoakum"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Young",
			"label" => "Young ( TX )",
			"locale" => "TX | Young"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Zapata",
			"label" => "Zapata ( TX )",
			"locale" => "TX | Zapata"
		]);
		County::create([
			"state_id" => 44,
			"location_id" => 0,
			"county" => "Zavala",
			"label" => "Zavala ( TX )",
			"locale" => "TX | Zavala"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Beaver",
			"label" => "Beaver ( UT )",
			"locale" => "UT | Beaver"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Box Elder",
			"label" => "Box Elder ( UT )",
			"locale" => "UT | Box Elder"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Cache",
			"label" => "Cache ( UT )",
			"locale" => "UT | Cache"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Carbon",
			"label" => "Carbon ( UT )",
			"locale" => "UT | Carbon"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Daggett",
			"label" => "Daggett ( UT )",
			"locale" => "UT | Daggett"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Davis",
			"label" => "Davis ( UT )",
			"locale" => "UT | Davis"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Duchesne",
			"label" => "Duchesne ( UT )",
			"locale" => "UT | Duchesne"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Emery",
			"label" => "Emery ( UT )",
			"locale" => "UT | Emery"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Garfield",
			"label" => "Garfield ( UT )",
			"locale" => "UT | Garfield"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Grand",
			"label" => "Grand ( UT )",
			"locale" => "UT | Grand"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Iron",
			"label" => "Iron ( UT )",
			"locale" => "UT | Iron"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Juab",
			"label" => "Juab ( UT )",
			"locale" => "UT | Juab"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Kane",
			"label" => "Kane ( UT )",
			"locale" => "UT | Kane"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Millard",
			"label" => "Millard ( UT )",
			"locale" => "UT | Millard"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Morgan",
			"label" => "Morgan ( UT )",
			"locale" => "UT | Morgan"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Piute",
			"label" => "Piute ( UT )",
			"locale" => "UT | Piute"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Rich",
			"label" => "Rich ( UT )",
			"locale" => "UT | Rich"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Salt Lake",
			"label" => "Salt Lake ( UT )",
			"locale" => "UT | Salt Lake"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "San Juan",
			"label" => "San Juan ( UT )",
			"locale" => "UT | San Juan"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Sanpete",
			"label" => "Sanpete ( UT )",
			"locale" => "UT | Sanpete"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Sevier",
			"label" => "Sevier ( UT )",
			"locale" => "UT | Sevier"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Summit",
			"label" => "Summit ( UT )",
			"locale" => "UT | Summit"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Tooele",
			"label" => "Tooele ( UT )",
			"locale" => "UT | Tooele"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Uintah",
			"label" => "Uintah ( UT )",
			"locale" => "UT | Uintah"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Utah",
			"label" => "Utah ( UT )",
			"locale" => "UT | Utah"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Wasatch",
			"label" => "Wasatch ( UT )",
			"locale" => "UT | Wasatch"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( UT )",
			"locale" => "UT | Washington"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Wayne",
			"label" => "Wayne ( UT )",
			"locale" => "UT | Wayne"
		]);
		County::create([
			"state_id" => 45,
			"location_id" => 0,
			"county" => "Weber",
			"label" => "Weber ( UT )",
			"locale" => "UT | Weber"
		]);
		County::create([
			"state_id" => 46,
			"location_id" => 0,
			"county" => "Addison",
			"label" => "Addison ( VT )",
			"locale" => "VT | Addison"
		]);
		County::create([
			"state_id" => 46,
			"location_id" => 0,
			"county" => "Bennington",
			"label" => "Bennington ( VT )",
			"locale" => "VT | Bennington"
		]);
		County::create([
			"state_id" => 46,
			"location_id" => 0,
			"county" => "Caledonia",
			"label" => "Caledonia ( VT )",
			"locale" => "VT | Caledonia"
		]);
		County::create([
			"state_id" => 46,
			"location_id" => 0,
			"county" => "Chittenden",
			"label" => "Chittenden ( VT )",
			"locale" => "VT | Chittenden"
		]);
		County::create([
			"state_id" => 46,
			"location_id" => 0,
			"county" => "Essex",
			"label" => "Essex ( VT )",
			"locale" => "VT | Essex"
		]);
		County::create([
			"state_id" => 46,
			"location_id" => 0,
			"county" => "Franklin",
			"label" => "Franklin ( VT )",
			"locale" => "VT | Franklin"
		]);
		County::create([
			"state_id" => 46,
			"location_id" => 0,
			"county" => "Grand Isle",
			"label" => "Grand Isle ( VT )",
			"locale" => "VT | Grand Isle"
		]);
		County::create([
			"state_id" => 46,
			"location_id" => 0,
			"county" => "Lamoille",
			"label" => "Lamoille ( VT )",
			"locale" => "VT | Lamoille"
		]);
		County::create([
			"state_id" => 46,
			"location_id" => 0,
			"county" => "Orange",
			"label" => "Orange ( VT )",
			"locale" => "VT | Orange"
		]);
		County::create([
			"state_id" => 46,
			"location_id" => 0,
			"county" => "Orleans",
			"label" => "Orleans ( VT )",
			"locale" => "VT | Orleans"
		]);
		County::create([
			"state_id" => 46,
			"location_id" => 0,
			"county" => "Rutland",
			"label" => "Rutland ( VT )",
			"locale" => "VT | Rutland"
		]);
		County::create([
			"state_id" => 46,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( VT )",
			"locale" => "VT | Washington"
		]);
		County::create([
			"state_id" => 46,
			"location_id" => 0,
			"county" => "Windham",
			"label" => "Windham ( VT )",
			"locale" => "VT | Windham"
		]);
		County::create([
			"state_id" => 46,
			"location_id" => 0,
			"county" => "Windsor",
			"label" => "Windsor ( VT )",
			"locale" => "VT | Windsor"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Accomack",
			"label" => "Accomack ( VA )",
			"locale" => "VA | Accomack"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Albemarle",
			"label" => "Albemarle ( VA )",
			"locale" => "VA | Albemarle"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Alleghany",
			"label" => "Alleghany ( VA )",
			"locale" => "VA | Alleghany"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Amelia",
			"label" => "Amelia ( VA )",
			"locale" => "VA | Amelia"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Amherst",
			"label" => "Amherst ( VA )",
			"locale" => "VA | Amherst"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Appomattox",
			"label" => "Appomattox ( VA )",
			"locale" => "VA | Appomattox"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Arlington",
			"label" => "Arlington ( VA )",
			"locale" => "VA | Arlington"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Augusta",
			"label" => "Augusta ( VA )",
			"locale" => "VA | Augusta"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Bath",
			"label" => "Bath ( VA )",
			"locale" => "VA | Bath"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Bedford",
			"label" => "Bedford ( VA )",
			"locale" => "VA | Bedford"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Bland",
			"label" => "Bland ( VA )",
			"locale" => "VA | Bland"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Botetourt",
			"label" => "Botetourt ( VA )",
			"locale" => "VA | Botetourt"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Brunswick",
			"label" => "Brunswick ( VA )",
			"locale" => "VA | Brunswick"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Buchanan",
			"label" => "Buchanan ( VA )",
			"locale" => "VA | Buchanan"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Buckingham",
			"label" => "Buckingham ( VA )",
			"locale" => "VA | Buckingham"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Campbell",
			"label" => "Campbell ( VA )",
			"locale" => "VA | Campbell"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Caroline",
			"label" => "Caroline ( VA )",
			"locale" => "VA | Caroline"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Carroll",
			"label" => "Carroll ( VA )",
			"locale" => "VA | Carroll"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Charles City",
			"label" => "Charles City ( VA )",
			"locale" => "VA | Charles City"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Charlotte",
			"label" => "Charlotte ( VA )",
			"locale" => "VA | Charlotte"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Chesterfield",
			"label" => "Chesterfield ( VA )",
			"locale" => "VA | Chesterfield"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Clarke",
			"label" => "Clarke ( VA )",
			"locale" => "VA | Clarke"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Craig",
			"label" => "Craig ( VA )",
			"locale" => "VA | Craig"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Culpeper",
			"label" => "Culpeper ( VA )",
			"locale" => "VA | Culpeper"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Cumberland",
			"label" => "Cumberland ( VA )",
			"locale" => "VA | Cumberland"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Dickenson",
			"label" => "Dickenson ( VA )",
			"locale" => "VA | Dickenson"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Dinwiddie",
			"label" => "Dinwiddie ( VA )",
			"locale" => "VA | Dinwiddie"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Essex",
			"label" => "Essex ( VA )",
			"locale" => "VA | Essex"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Fairfax",
			"label" => "Fairfax ( VA )",
			"locale" => "VA | Fairfax"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Fauquier",
			"label" => "Fauquier ( VA )",
			"locale" => "VA | Fauquier"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Floyd",
			"label" => "Floyd ( VA )",
			"locale" => "VA | Floyd"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Fluvanna",
			"label" => "Fluvanna ( VA )",
			"locale" => "VA | Fluvanna"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Franklin",
			"label" => "Franklin ( VA )",
			"locale" => "VA | Franklin"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Frederick",
			"label" => "Frederick ( VA )",
			"locale" => "VA | Frederick"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Giles",
			"label" => "Giles ( VA )",
			"locale" => "VA | Giles"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Gloucester",
			"label" => "Gloucester ( VA )",
			"locale" => "VA | Gloucester"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Goochland",
			"label" => "Goochland ( VA )",
			"locale" => "VA | Goochland"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Grayson",
			"label" => "Grayson ( VA )",
			"locale" => "VA | Grayson"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Greene",
			"label" => "Greene ( VA )",
			"locale" => "VA | Greene"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Greensville",
			"label" => "Greensville ( VA )",
			"locale" => "VA | Greensville"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Halifax",
			"label" => "Halifax ( VA )",
			"locale" => "VA | Halifax"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Hanover",
			"label" => "Hanover ( VA )",
			"locale" => "VA | Hanover"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Henrico",
			"label" => "Henrico ( VA )",
			"locale" => "VA | Henrico"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Henry",
			"label" => "Henry ( VA )",
			"locale" => "VA | Henry"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Highland",
			"label" => "Highland ( VA )",
			"locale" => "VA | Highland"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Isle of Wight",
			"label" => "Isle of Wight ( VA )",
			"locale" => "VA | Isle of Wight"
		]);
        County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "James City",
			"label" => "James City ( VA )",
			"locale" => "VA | James City"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "King and Queen",
			"label" => "King and Queen ( VA )",
			"locale" => "VA | King and Queen"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "King George",
			"label" => "King George ( VA )",
			"locale" => "VA | King George"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "King William",
			"label" => "King William ( VA )",
			"locale" => "VA | King William"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Lancaster",
			"label" => "Lancaster ( VA )",
			"locale" => "VA | Lancaster"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Lee",
			"label" => "Lee ( VA )",
			"locale" => "VA | Lee"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Loudoun",
			"label" => "Loudoun ( VA )",
			"locale" => "VA | Loudoun"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Louisa",
			"label" => "Louisa ( VA )",
			"locale" => "VA | Louisa"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Lunenburg",
			"label" => "Lunenburg ( VA )",
			"locale" => "VA | Lunenburg"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Madison",
			"label" => "Madison ( VA )",
			"locale" => "VA | Madison"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Mathews",
			"label" => "Mathews ( VA )",
			"locale" => "VA | Mathews"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Mecklenburg",
			"label" => "Mecklenburg ( VA )",
			"locale" => "VA | Mecklenburg"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Middlesex",
			"label" => "Middlesex ( VA )",
			"locale" => "VA | Middlesex"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Montgomery",
			"label" => "Montgomery ( VA )",
			"locale" => "VA | Montgomery"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Nelson",
			"label" => "Nelson ( VA )",
			"locale" => "VA | Nelson"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "New Kent",
			"label" => "New Kent ( VA )",
			"locale" => "VA | New Kent"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Northampton",
			"label" => "Northampton ( VA )",
			"locale" => "VA | Northampton"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Northumberland",
			"label" => "Northumberland ( VA )",
			"locale" => "VA | Northumberland"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Nottoway",
			"label" => "Nottoway ( VA )",
			"locale" => "VA | Nottoway"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Orange",
			"label" => "Orange ( VA )",
			"locale" => "VA | Orange"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Page",
			"label" => "Page ( VA )",
			"locale" => "VA | Page"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Patrick",
			"label" => "Patrick ( VA )",
			"locale" => "VA | Patrick"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Pittsylvania",
			"label" => "Pittsylvania ( VA )",
			"locale" => "VA | Pittsylvania"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Powhatan",
			"label" => "Powhatan ( VA )",
			"locale" => "VA | Powhatan"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Prince Edward",
			"label" => "Prince Edward ( VA )",
			"locale" => "VA | Prince Edward"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Prince George",
			"label" => "Prince George ( VA )",
			"locale" => "VA | Prince George"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Prince William",
			"label" => "Prince William ( VA )",
			"locale" => "VA | Prince William"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Pulaski",
			"label" => "Pulaski ( VA )",
			"locale" => "VA | Pulaski"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Rappahannock",
			"label" => "Rappahannock ( VA )",
			"locale" => "VA | Rappahannock"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Richmond",
			"label" => "Richmond ( VA )",
			"locale" => "VA | Richmond"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Roanoke",
			"label" => "Roanoke ( VA )",
			"locale" => "VA | Roanoke"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Rockbridge",
			"label" => "Rockbridge ( VA )",
			"locale" => "VA | Rockbridge"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Rockingham",
			"label" => "Rockingham ( VA )",
			"locale" => "VA | Rockingham"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Russell",
			"label" => "Russell ( VA )",
			"locale" => "VA | Russell"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Scott",
			"label" => "Scott ( VA )",
			"locale" => "VA | Scott"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Shenandoah",
			"label" => "Shenandoah ( VA )",
			"locale" => "VA | Shenandoah"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Smyth",
			"label" => "Smyth ( VA )",
			"locale" => "VA | Smyth"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Southampton",
			"label" => "Southampton ( VA )",
			"locale" => "VA | Southampton"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Spotsylvania",
			"label" => "Spotsylvania ( VA )",
			"locale" => "VA | Spotsylvania"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Stafford",
			"label" => "Stafford ( VA )",
			"locale" => "VA | Stafford"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Surry",
			"label" => "Surry ( VA )",
			"locale" => "VA | Surry"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Sussex",
			"label" => "Sussex ( VA )",
			"locale" => "VA | Sussex"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Tazewell",
			"label" => "Tazewell ( VA )",
			"locale" => "VA | Tazewell"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Warren",
			"label" => "Warren ( VA )",
			"locale" => "VA | Warren"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( VA )",
			"locale" => "VA | Washington"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Westmoreland",
			"label" => "Westmoreland ( VA )",
			"locale" => "VA | Westmoreland"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Wise",
			"label" => "Wise ( VA )",
			"locale" => "VA | Wise"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "Wythe",
			"label" => "Wythe ( VA )",
			"locale" => "VA | Wythe"
		]);
		County::create([
			"state_id" => 47,
			"location_id" => 0,
			"county" => "York",
			"label" => "York ( VA )",
			"locale" => "VA | York"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Adams",
			"label" => "Adams ( WA )",
			"locale" => "WA | Adams"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Asotin",
			"label" => "Asotin ( WA )",
			"locale" => "WA | Asotin"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Benton",
			"label" => "Benton ( WA )",
			"locale" => "WA | Benton"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Chelan",
			"label" => "Chelan ( WA )",
			"locale" => "WA | Chelan"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Clallam",
			"label" => "Clallam ( WA )",
			"locale" => "WA | Clallam"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Clark",
			"label" => "Clark ( WA )",
			"locale" => "WA | Clark"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Columbia",
			"label" => "Columbia ( WA )",
			"locale" => "WA | Columbia"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Cowlitz",
			"label" => "Cowlitz ( WA )",
			"locale" => "WA | Cowlitz"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Douglas",
			"label" => "Douglas ( WA )",
			"locale" => "WA | Douglas"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Ferry",
			"label" => "Ferry ( WA )",
			"locale" => "WA | Ferry"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Franklin",
			"label" => "Franklin ( WA )",
			"locale" => "WA | Franklin"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Garfield",
			"label" => "Garfield ( WA )",
			"locale" => "WA | Garfield"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Grant",
			"label" => "Grant ( WA )",
			"locale" => "WA | Grant"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Grays Harbor",
			"label" => "Grays Harbor ( WA )",
			"locale" => "WA | Grays Harbor"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Island",
			"label" => "Island ( WA )",
			"locale" => "WA | Island"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Jefferson",
			"label" => "Jefferson ( WA )",
			"locale" => "WA | Jefferson"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "King",
			"label" => "King ( WA )",
			"locale" => "WA | King"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Kitsap",
			"label" => "Kitsap ( WA )",
			"locale" => "WA | Kitsap"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Kittitas",
			"label" => "Kittitas ( WA )",
			"locale" => "WA | Kittitas"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Klickitat",
			"label" => "Klickitat ( WA )",
			"locale" => "WA | Klickitat"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Lewis",
			"label" => "Lewis ( WA )",
			"locale" => "WA | Lewis"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Lincoln",
			"label" => "Lincoln ( WA )",
			"locale" => "WA | Lincoln"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Mason",
			"label" => "Mason ( WA )",
			"locale" => "WA | Mason"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Okanogan",
			"label" => "Okanogan ( WA )",
			"locale" => "WA | Okanogan"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Pacific",
			"label" => "Pacific ( WA )",
			"locale" => "WA | Pacific"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Pend Oreille",
			"label" => "Pend Oreille ( WA )",
			"locale" => "WA | Pend Oreille"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Pierce",
			"label" => "Pierce ( WA )",
			"locale" => "WA | Pierce"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "San Juan",
			"label" => "San Juan ( WA )",
			"locale" => "WA | San Juan"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Skagit",
			"label" => "Skagit ( WA )",
			"locale" => "WA | Skagit"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Skamania",
			"label" => "Skamania ( WA )",
			"locale" => "WA | Skamania"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Snohomish",
			"label" => "Snohomish ( WA )",
			"locale" => "WA | Snohomish"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Spokane",
			"label" => "Spokane ( WA )",
			"locale" => "WA | Spokane"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Stevens",
			"label" => "Stevens ( WA )",
			"locale" => "WA | Stevens"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Thurston",
			"label" => "Thurston ( WA )",
			"locale" => "WA | Thurston"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Wahkiakum",
			"label" => "Wahkiakum ( WA )",
			"locale" => "WA | Wahkiakum"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Walla Walla",
			"label" => "Walla Walla ( WA )",
			"locale" => "WA | Walla Walla"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Whatcom",
			"label" => "Whatcom ( WA )",
			"locale" => "WA | Whatcom"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Whitman",
			"label" => "Whitman ( WA )",
			"locale" => "WA | Whitman"
		]);
		County::create([
			"state_id" => 48,
			"location_id" => 0,
			"county" => "Yakima",
			"label" => "Yakima ( WA )",
			"locale" => "WA | Yakima"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Barbour",
			"label" => "Barbour ( WV )",
			"locale" => "WV | Barbour"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Berkeley",
			"label" => "Berkeley ( WV )",
			"locale" => "WV | Berkeley"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Boone",
			"label" => "Boone ( WV )",
			"locale" => "WV | Boone"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Braxton",
			"label" => "Braxton ( WV )",
			"locale" => "WV | Braxton"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Brooke",
			"label" => "Brooke ( WV )",
			"locale" => "WV | Brooke"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Cabell",
			"label" => "Cabell ( WV )",
			"locale" => "WV | Cabell"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Calhoun",
			"label" => "Calhoun ( WV )",
			"locale" => "WV | Calhoun"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Clay",
			"label" => "Clay ( WV )",
			"locale" => "WV | Clay"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Doddridge",
			"label" => "Doddridge ( WV )",
			"locale" => "WV | Doddridge"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Fayette",
			"label" => "Fayette ( WV )",
			"locale" => "WV | Fayette"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Gilmer",
			"label" => "Gilmer ( WV )",
			"locale" => "WV | Gilmer"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Grant",
			"label" => "Grant ( WV )",
			"locale" => "WV | Grant"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Greenbrier",
			"label" => "Greenbrier ( WV )",
			"locale" => "WV | Greenbrier"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Hampshire",
			"label" => "Hampshire ( WV )",
			"locale" => "WV | Hampshire"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Hancock",
			"label" => "Hancock ( WV )",
			"locale" => "WV | Hancock"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Hardy",
			"label" => "Hardy ( WV )",
			"locale" => "WV | Hardy"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Harrison",
			"label" => "Harrison ( WV )",
			"locale" => "WV | Harrison"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Jackson",
			"label" => "Jackson ( WV )",
			"locale" => "WV | Jackson"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Jefferson",
			"label" => "Jefferson ( WV )",
			"locale" => "WV | Jefferson"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Kanawha",
			"label" => "Kanawha ( WV )",
			"locale" => "WV | Kanawha"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Lewis",
			"label" => "Lewis ( WV )",
			"locale" => "WV | Lewis"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Lincoln",
			"label" => "Lincoln ( WV )",
			"locale" => "WV | Lincoln"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Logan",
			"label" => "Logan ( WV )",
			"locale" => "WV | Logan"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Marion",
			"label" => "Marion ( WV )",
			"locale" => "WV | Marion"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Marshall",
			"label" => "Marshall ( WV )",
			"locale" => "WV | Marshall"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Mason",
			"label" => "Mason ( WV )",
			"locale" => "WV | Mason"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "McDowell",
			"label" => "McDowell ( WV )",
			"locale" => "WV | McDowell"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Mercer",
			"label" => "Mercer ( WV )",
			"locale" => "WV | Mercer"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Mineral",
			"label" => "Mineral ( WV )",
			"locale" => "WV | Mineral"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Mingo",
			"label" => "Mingo ( WV )",
			"locale" => "WV | Mingo"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Monongalia",
			"label" => "Monongalia ( WV )",
			"locale" => "WV | Monongalia"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Monroe",
			"label" => "Monroe ( WV )",
			"locale" => "WV | Monroe"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Morgan",
			"label" => "Morgan ( WV )",
			"locale" => "WV | Morgan"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Nicholas",
			"label" => "Nicholas ( WV )",
			"locale" => "WV | Nicholas"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Ohio",
			"label" => "Ohio ( WV )",
			"locale" => "WV | Ohio"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Pendleton",
			"label" => "Pendleton ( WV )",
			"locale" => "WV | Pendleton"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Pleasants",
			"label" => "Pleasants ( WV )",
			"locale" => "WV | Pleasants"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Pocahontas",
			"label" => "Pocahontas ( WV )",
			"locale" => "WV | Pocahontas"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Preston",
			"label" => "Preston ( WV )",
			"locale" => "WV | Preston"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Putnam",
			"label" => "Putnam ( WV )",
			"locale" => "WV | Putnam"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Raleigh",
			"label" => "Raleigh ( WV )",
			"locale" => "WV | Raleigh"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Randolph",
			"label" => "Randolph ( WV )",
			"locale" => "WV | Randolph"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Ritchie",
			"label" => "Ritchie ( WV )",
			"locale" => "WV | Ritchie"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Roane",
			"label" => "Roane ( WV )",
			"locale" => "WV | Roane"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Summers",
			"label" => "Summers ( WV )",
			"locale" => "WV | Summers"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Taylor",
			"label" => "Taylor ( WV )",
			"locale" => "WV | Taylor"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Tucker",
			"label" => "Tucker ( WV )",
			"locale" => "WV | Tucker"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Tyler",
			"label" => "Tyler ( WV )",
			"locale" => "WV | Tyler"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Upshur",
			"label" => "Upshur ( WV )",
			"locale" => "WV | Upshur"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Wayne",
			"label" => "Wayne ( WV )",
			"locale" => "WV | Wayne"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Webster",
			"label" => "Webster ( WV )",
			"locale" => "WV | Webster"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Wetzel",
			"label" => "Wetzel ( WV )",
			"locale" => "WV | Wetzel"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Wirt",
			"label" => "Wirt ( WV )",
			"locale" => "WV | Wirt"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Wood",
			"label" => "Wood ( WV )",
			"locale" => "WV | Wood"
		]);
		County::create([
			"state_id" => 49,
			"location_id" => 0,
			"county" => "Wyoming",
			"label" => "Wyoming ( WV )",
			"locale" => "WV | Wyoming"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Adams",
			"label" => "Adams ( WI )",
			"locale" => "WI | Adams"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Ashland",
			"label" => "Ashland ( WI )",
			"locale" => "WI | Ashland"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Barron",
			"label" => "Barron ( WI )",
			"locale" => "WI | Barron"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Bayfield",
			"label" => "Bayfield ( WI )",
			"locale" => "WI | Bayfield"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Brown",
			"label" => "Brown ( WI )",
			"locale" => "WI | Brown"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Buffalo",
			"label" => "Buffalo ( WI )",
			"locale" => "WI | Buffalo"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Burnett",
			"label" => "Burnett ( WI )",
			"locale" => "WI | Burnett"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Calumet",
			"label" => "Calumet ( WI )",
			"locale" => "WI | Calumet"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Chippewa",
			"label" => "Chippewa ( WI )",
			"locale" => "WI | Chippewa"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Clark",
			"label" => "Clark ( WI )",
			"locale" => "WI | Clark"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Columbia",
			"label" => "Columbia ( WI )",
			"locale" => "WI | Columbia"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Crawford",
			"label" => "Crawford ( WI )",
			"locale" => "WI | Crawford"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Dane",
			"label" => "Dane ( WI )",
			"locale" => "WI | Dane"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Dodge",
			"label" => "Dodge ( WI )",
			"locale" => "WI | Dodge"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Door",
			"label" => "Door ( WI )",
			"locale" => "WI | Door"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Douglas",
			"label" => "Douglas ( WI )",
			"locale" => "WI | Douglas"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Dunn",
			"label" => "Dunn ( WI )",
			"locale" => "WI | Dunn"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Eau Claire",
			"label" => "Eau Claire ( WI )",
			"locale" => "WI | Eau Claire"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Florence",
			"label" => "Florence ( WI )",
			"locale" => "WI | Florence"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Fond du Lac",
			"label" => "Fond du Lac ( WI )",
			"locale" => "WI | Fond du Lac"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Forest",
			"label" => "Forest ( WI )",
			"locale" => "WI | Forest"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Grant",
			"label" => "Grant ( WI )",
			"locale" => "WI | Grant"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Green",
			"label" => "Green ( WI )",
			"locale" => "WI | Green"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Green Lake",
			"label" => "Green Lake ( WI )",
			"locale" => "WI | Green Lake"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Iowa",
			"label" => "Iowa ( WI )",
			"locale" => "WI | Iowa"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Iron",
			"label" => "Iron ( WI )",
			"locale" => "WI | Iron"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Jackson",
			"label" => "Jackson ( WI )",
			"locale" => "WI | Jackson"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Jefferson",
			"label" => "Jefferson ( WI )",
			"locale" => "WI | Jefferson"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Juneau",
			"label" => "Juneau ( WI )",
			"locale" => "WI | Juneau"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Kenosha",
			"label" => "Kenosha ( WI )",
			"locale" => "WI | Kenosha"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Kewaunee",
			"label" => "Kewaunee ( WI )",
			"locale" => "WI | Kewaunee"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "La Crosse",
			"label" => "La Crosse ( WI )",
			"locale" => "WI | La Crosse"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Lafayette",
			"label" => "Lafayette ( WI )",
			"locale" => "WI | Lafayette"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Langlade",
			"label" => "Langlade ( WI )",
			"locale" => "WI | Langlade"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Lincoln",
			"label" => "Lincoln ( WI )",
			"locale" => "WI | Lincoln"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Manitowoc",
			"label" => "Manitowoc ( WI )",
			"locale" => "WI | Manitowoc"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Marathon",
			"label" => "Marathon ( WI )",
			"locale" => "WI | Marathon"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Marinette",
			"label" => "Marinette ( WI )",
			"locale" => "WI | Marinette"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Marquette",
			"label" => "Marquette ( WI )",
			"locale" => "WI | Marquette"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Menominee",
			"label" => "Menominee ( WI )",
			"locale" => "WI | Menominee"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Milwaukee",
			"label" => "Milwaukee ( WI )",
			"locale" => "WI | Milwaukee"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Monroe",
			"label" => "Monroe ( WI )",
			"locale" => "WI | Monroe"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Oconto",
			"label" => "Oconto ( WI )",
			"locale" => "WI | Oconto"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Oneida",
			"label" => "Oneida ( WI )",
			"locale" => "WI | Oneida"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Outagamie",
			"label" => "Outagamie ( WI )",
			"locale" => "WI | Outagamie"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Ozaukee",
			"label" => "Ozaukee ( WI )",
			"locale" => "WI | Ozaukee"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Pepin",
			"label" => "Pepin ( WI )",
			"locale" => "WI | Pepin"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Pierce",
			"label" => "Pierce ( WI )",
			"locale" => "WI | Pierce"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Polk",
			"label" => "Polk ( WI )",
			"locale" => "WI | Polk"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Portage",
			"label" => "Portage ( WI )",
			"locale" => "WI | Portage"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Price",
			"label" => "Price ( WI )",
			"locale" => "WI | Price"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Racine",
			"label" => "Racine ( WI )",
			"locale" => "WI | Racine"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Richland",
			"label" => "Richland ( WI )",
			"locale" => "WI | Richland"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Rock",
			"label" => "Rock ( WI )",
			"locale" => "WI | Rock"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Rusk",
			"label" => "Rusk ( WI )",
			"locale" => "WI | Rusk"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Sauk",
			"label" => "Sauk ( WI )",
			"locale" => "WI | Sauk"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Sawyer",
			"label" => "Sawyer ( WI )",
			"locale" => "WI | Sawyer"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Shawano",
			"label" => "Shawano ( WI )",
			"locale" => "WI | Shawano"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Sheboygan",
			"label" => "Sheboygan ( WI )",
			"locale" => "WI | Sheboygan"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "St. Croix",
			"label" => "St. Croix ( WI )",
			"locale" => "WI | St. Croix"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Taylor",
			"label" => "Taylor ( WI )",
			"locale" => "WI | Taylor"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Trempealeau",
			"label" => "Trempealeau ( WI )",
			"locale" => "WI | Trempealeau"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Vernon",
			"label" => "Vernon ( WI )",
			"locale" => "WI | Vernon"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Vilas",
			"label" => "Vilas ( WI )",
			"locale" => "WI | Vilas"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Walworth",
			"label" => "Walworth ( WI )",
			"locale" => "WI | Walworth"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Washburn",
			"label" => "Washburn ( WI )",
			"locale" => "WI | Washburn"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Washington",
			"label" => "Washington ( WI )",
			"locale" => "WI | Washington"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Waukesha",
			"label" => "Waukesha ( WI )",
			"locale" => "WI | Waukesha"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Waupaca",
			"label" => "Waupaca ( WI )",
			"locale" => "WI | Waupaca"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Waushara",
			"label" => "Waushara ( WI )",
			"locale" => "WI | Waushara"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Winnebago",
			"label" => "Winnebago ( WI )",
			"locale" => "WI | Winnebago"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Wood",
			"label" => "Wood ( WI )",
			"locale" => "WI | Wood"
		]);
		County::create([
			"state_id" => 50,
			"location_id" => 0,
			"county" => "Bad River",
			"label" => "Bad River ( WI )",
			"locale" => "WI | Bad River"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Albany",
			"label" => "Albany ( WY )",
			"locale" => "WY | Albany"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Big Horn",
			"label" => "Big Horn ( WY )",
			"locale" => "WY | Big Horn"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Campbell",
			"label" => "Campbell ( WY )",
			"locale" => "WY | Campbell"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Carbon",
			"label" => "Carbon ( WY )",
			"locale" => "WY | Carbon"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Converse",
			"label" => "Converse ( WY )",
			"locale" => "WY | Converse"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Crook",
			"label" => "Crook ( WY )",
			"locale" => "WY | Crook"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Fremont",
			"label" => "Fremont ( WY )",
			"locale" => "WY | Fremont"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Goshen",
			"label" => "Goshen ( WY )",
			"locale" => "WY | Goshen"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Hot Springs",
			"label" => "Hot Springs ( WY )",
			"locale" => "WY | Hot Springs"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Johnson",
			"label" => "Johnson ( WY )",
			"locale" => "WY | Johnson"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Laramie",
			"label" => "Laramie ( WY )",
			"locale" => "WY | Laramie"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Lincoln",
			"label" => "Lincoln ( WY )",
			"locale" => "WY | Lincoln"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Natrona",
			"label" => "Natrona ( WY )",
			"locale" => "WY | Natrona"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Niobrara",
			"label" => "Niobrara ( WY )",
			"locale" => "WY | Niobrara"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Park",
			"label" => "Park ( WY )",
			"locale" => "WY | Park"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Platte",
			"label" => "Platte ( WY )",
			"locale" => "WY | Platte"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Sheridan",
			"label" => "Sheridan ( WY )",
			"locale" => "WY | Sheridan"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Sublette",
			"label" => "Sublette ( WY )",
			"locale" => "WY | Sublette"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Sweetwater",
			"label" => "Sweetwater ( WY )",
			"locale" => "WY | Sweetwater"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Teton",
			"label" => "Teton ( WY )",
			"locale" => "WY | Teton"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Uinta",
			"label" => "Uinta ( WY )",
			"locale" => "WY | Uinta"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Washakie",
			"label" => "Washakie ( WY )",
			"locale" => "WY | Washakie"
		]);
		County::create([
			"state_id" => 51,
			"location_id" => 0,
			"county" => "Weston",
			"label" => "Weston ( WY )",
			"locale" => "WY | Weston"
		]);
    }

}
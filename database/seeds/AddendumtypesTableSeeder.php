<?php

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;
use App\Addendumtype as Addendumtype;

class AddendumtypesTableSeeder extends Seeder {

    public function run()
    {
        Addendumtype::create([
            'type' =>	'original'
        ]);

        Addendumtype::create([
            'type' =>	'loan'
        ]);

        Addendumtype::create([
            'type' =>	'reconciliation'
        ]);
    }

}
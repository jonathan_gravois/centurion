<?php

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;
use App\Region as Region;

class RegionsTableSeeder extends Seeder {

    public function run()
    {
        Region::create([
            'region' => 'East'
        ]);
        Region::create([
            'region' => 'West'
        ]);
        Region::create([
            'region' => 'Mid-West'
        ]);
    }

}
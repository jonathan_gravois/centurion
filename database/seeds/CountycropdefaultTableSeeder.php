<?php

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;
use App\Countycropdefault as Countycropdefault;

class CountycropdefaultsTableSeeder extends Seeder
{

    public function run()
    {
        for($c=1; $c<3240; $c++){
            Countycropdefault::create([
                'county_id' => $c
            ]);
        } // end for
    }

}
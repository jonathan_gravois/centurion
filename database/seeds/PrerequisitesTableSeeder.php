<?php

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;
use App\Prerequisite as Prerequisite;

class PrerequisitesTableSeeder extends Seeder
{

    public function run()
    {
        Prerequisite::create([
            'loan_id'	=>	1,
            'document' => "Driver's License",
            'date_requested' => '2014-09-02',
            'date_received' => '2014-09-02',
            'path' => '2015_1/',
            'filename' => 'driversLicense.pdf'
        ]);

        Prerequisite::create([
            'loan_id'	=>	1,
            'document' => "Financials",
            'date_requested' => '2014-09-02',
            'reason_pending' => 'Client unprepared'
        ]);

        Prerequisite::create([
            'loan_id'	=>	1,
            'document' => "Equipment List",
            'date_requested' => '2014-09-02',
            'date_received' => '2014-09-02',
            'path' => '2015_1/',
            'filename' => 'equipmentList.pdf'
        ]);

        Prerequisite::create([
            'loan_id'	=>	1,
            'document' => "Leases"
        ]);

        Prerequisite::create([
            'loan_id'	=>	1,
            'document' => "FSA Information"
        ]);

        Prerequisite::create([
            'loan_id'	=>	1,
            'document' => "Insurance Information"
        ]);

        Prerequisite::create([
            'loan_id'	=>	1,
            'document' => "Crop Insurance Info & Database"
        ]);

        Prerequisite::create([
            'loan_id'	=>	1,
            'document' => "Grain Contracts"
        ]);
    }

}
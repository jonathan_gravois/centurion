<?php

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;
use App\Unit as Unit;

class UnitsTableSeeder extends Seeder {

    public function run()
    {
        Unit::create([
            'unit' => 'bushel',
            'abr' => 'bu'
        ]);
        Unit::create([
            'unit' => 'pound',
            'abr' => 'lb'
        ]);
        Unit::create([
            'unit' => 'hundredweight',
            'abr' => 'cwt'
        ]);
        Unit::create([
            'unit' => 'ton',
            'abr' => 'ton'
        ]);
        Unit::create([
            'unit' => 'barrel',
            'abr' => 'bbl'
        ]);
        Unit::create([
            'unit' => 'bale',
            'abr' => 'bl'
        ]);
    }

}
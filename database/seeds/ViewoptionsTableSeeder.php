<?php

use Illuminate\Database\Seeder;
use App\Viewoption as Viewoption;

class ViewoptionsTableSeeder extends Seeder
{

    public function run()
    {
        Viewoption::create([
            'user_id' => 1
        ]);

        Viewoption::create([
            'user_id' => 2,
            'view_commit_total' =>  1,
            'view_commit_distributor' => 1,
            'view_fee_total' => 1
        ]);

        Viewoption::create([
            'user_id' => 3
        ]);

        Viewoption::create([
            'user_id' => 4
        ]);

        Viewoption::create([
            'user_id' => 5
        ]);

        Viewoption::create([
            'user_id' => 6
        ]);
    }

}
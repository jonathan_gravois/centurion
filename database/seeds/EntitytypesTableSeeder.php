<?php

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;
use App\Entitytype as Entitytype;

class EntitytypesTableSeeder extends Seeder
{

    public function run()
    {
        Entitytype::create([
            'entitytype' => 'Corporation'
        ]);
        Entitytype::create([
            'entitytype' => 'Individual'
        ]);
        Entitytype::create([
            'entitytype' => 'JointVenture'
        ]);
        Entitytype::create([
            'entitytype' => 'Partnership'
        ]);
        Entitytype::create([
            'entitytype' => 'Spousal'
        ]);
    }

}
<?php

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;
use App\Cropexpense as Cropexpense;

class CropexpensesTableSeeder extends Seeder
{

    public function run()
    {
        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 1,
            'loancrop_id' => 1,
            'cat_id' => 1,
            'expense' => 'Fertilizer',
            'arm' => 0,
            'arm_adj' => 0,
            'dist' => 164.97,
            'dist_adj' => 164.97,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 1,
            'loancrop_id' => 1,
            'cat_id' => 2,
            'expense' => 'Seed',
            'arm' => 0,
            'arm_adj' => 0,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 115,
            'other_adj' => 115
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 1,
            'loancrop_id' => 1,
            'cat_id' => 3,
            'expense' => 'Fungicide',
            'arm' => 0,
            'arm_adj' => 0,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 1,
            'loancrop_id' => 1,
            'cat_id' => 4,
            'expense' => 'Herbicide',
            'arm' => 0,
            'arm_adj' => 0,
            'dist' => 32.94,
            'dist_adj' => 32.94,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 1,
            'loancrop_id' => 1,
            'cat_id' => 5,
            'expense' => 'Insecticide',
            'arm' => 0,
            'arm_adj' => 0,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 1,
            'loancrop_id' => 1,
            'cat_id' => 6,
            'expense' => 'Custom',
            'arm' => 18,
            'arm_adj' => 18,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 1,
            'loancrop_id' => 1,
            'cat_id' => 7,
            'expense' => 'Fuel',
            'arm' => 40,
            'arm_adj' => 40,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 1,
            'loancrop_id' => 1,
            'cat_id' => 8,
            'expense' => 'Labor',
            'arm' => 20,
            'arm_adj' => 20,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 1,
            'loancrop_id' => 1,
            'cat_id' => 9,
            'expense' => 'Repairs',
            'arm' => 20,
            'arm_adj' => 20,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 1,
            'loancrop_id' => 1,
            'cat_id' => 10,
            'expense' => 'Insurance',
            'arm' => 0,
            'arm_adj' => 0,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 1,
            'loancrop_id' => 1,
            'cat_id' => 11,
            'expense' => 'Harvesting',
            'arm' => 0,
            'arm_adj' => 0,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 1,
            'loancrop_id' => 1,
            'cat_id' => 12,
            'expense' => 'Misc Acre',
            'arm' => 20,
            'arm_adj' => 20,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 2,
            'loancrop_id' => 2,
            'cat_id' => 1,
            'expense' => 'Fertilizer',
            'arm' => 0,
            'arm_adj' => 0,
            'dist' => 26.50,
            'dist_adj' => 26.50,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 2,
            'loancrop_id' => 2,
            'cat_id' => 2,
            'expense' => 'Seed',
            'arm' => 0,
            'arm_adj' => 0,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 60,
            'other_adj' => 60
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 2,
            'loancrop_id' => 2,
            'cat_id' => 3,
            'expense' => 'Fungicide',
            'arm' => 0,
            'arm_adj' => 0,
            'dist' => 12.50,
            'dist_adj' => 12.50,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 2,
            'loancrop_id' => 2,
            'cat_id' => 4,
            'expense' => 'Herbicide',
            'arm' => 0,
            'arm_adj' => 0,
            'dist' => 20.06,
            'dist_adj' => 20.06,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 2,
            'loancrop_id' => 2,
            'cat_id' => 5,
            'expense' => 'Insecticide',
            'arm' => 0,
            'arm_adj' => 0,
            'dist' => 10.67,
            'dist_adj' => 10.67,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 2,
            'loancrop_id' => 2,
            'cat_id' => 6,
            'expense' => 'Custom',
            'arm' => 22,
            'arm_adj' => 22,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 2,
            'loancrop_id' => 2,
            'cat_id' => 7,
            'expense' => 'Fuel',
            'arm' => 30,
            'arm_adj' => 30,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 2,
            'loancrop_id' => 2,
            'cat_id' => 8,
            'expense' => 'Labor',
            'arm' => 20,
            'arm_adj' => 20,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 2,
            'loancrop_id' => 2,
            'cat_id' => 9,
            'expense' => 'Repairs',
            'arm' => 10,
            'arm_adj' => 10,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 2,
            'loancrop_id' => 2,
            'cat_id' => 10,
            'expense' => 'Insurance',
            'arm' => 0,
            'arm_adj' => 0,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 14.35,
            'other_adj' => 14.35
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 2,
            'loancrop_id' => 2,
            'cat_id' => 11,
            'expense' => 'Harvesting',
            'arm' => 0,
            'arm_adj' => 0,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 2,
            'loancrop_id' => 2,
            'cat_id' => 12,
            'expense' => 'Misc Acre',
            'arm' => 20,
            'arm_adj' => 20,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 1,
            'loancrop_id' => 1,
            'cat_id' => 13,
            'expense' => 'Living Expenses',
            'arm' => 60,
            'arm_adj' => 60,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 0,
            'other_adj' => 0
        ]);

        Cropexpense::create([
            'loan_id' => 1,
            'crop_id' => 2,
            'loancrop_id' => 2,
            'cat_id' => 13,
            'expense' => 'Living Expenses',
            'arm' => 43,
            'arm_adj' => 43,
            'dist' => 0,
            'dist_adj' => 0,
            'other' => 0,
            'other_adj' => 0
        ]);
    }

}
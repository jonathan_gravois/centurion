<?php

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;
use App\Notification as Notification;

class NotificationsTableSeeder extends Seeder
{

    public function run()
    {
        Notification::create([
            'user_id' => 2,
            'notification_type' => 'report',
            'task' => 'Confirm Activity Detail Report'
        ]);

        Notification::create([
            'user_id' => 2,
            'loan_id' => 1,
            'notification_type' => 'vote',
            'task' => 'Review Loan: Tony Stark - Glass Towers'
        ]);

        Notification::create([
            'user_id' => 2,
            'notification_type' => 'report',
            'task' => 'Confirm Customer Budget Report'
        ]);

        Notification::create([
            'user_id' => 2,
            'notification_type' => 'report',
            'task' => 'Confirm Account Reconciliation Report'
        ]);

        Notification::create([
            'user_id' => 2,
            'loan_id' => 5,
            'notification_type' => 'vote',
            'task' => 'Review Loan: Clint Barton - Nested Row'
        ]);

        Notification::create([
            'user_id' => 2,
            'notification_type' => 'office',
            'task' => 'Staff Meeting on Wednesday, Dec. 10, 2014',
            'status' => 'acknowledged'
        ]);

        Notification::create([
            'user_id' => 8,
            'notification_type' => 'office',
            'task' => 'Review Site'
        ]);

        Notification::create([
            'user_id' => 8,
            'notification_type' => 'office',
            'task' => 'Report Progress'
        ]);

        Notification::create([
            'user_id' => 8,
            'notification_type' => 'office',
            'task' => 'Enjoy life!'
        ]);
    }

}
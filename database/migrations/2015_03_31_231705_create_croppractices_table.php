<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCroppracticesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('croppractices', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('crop_id');
            $table->string('croppractice');
            $table->string('crop');
            $table->string('practice');
            $table->string('measurement');
            $table->string('rebate_measurement');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('croppractices');
	}

}

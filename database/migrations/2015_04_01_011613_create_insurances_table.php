<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsurancesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('insurances', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('loan_id');
            $table->string('agency_id')->nullable();
            $table->string('agent_id')->nullable();
            $table->string('policy')->nullable();
            $table->boolean('is_assigned')->default(0);
            $table->string('fsn')->nullable();
            $table->integer('loancounty_id')->nullable();
            $table->integer('loancrop_id')->nullable();
            $table->integer('croppractice_id')->nullable();
            $table->string('type')->nullable();
            $table->string('option')->nullable();
            $table->double('acres')->nullable();
            $table->double('price')->nullable();
            $table->double('yield')->nullable();
            $table->double('level')->nullable();
            $table->double('premium')->nullable();
            $table->double('share')->nullable();
            $table->double('guaranty')->nullable();
            $table->double('value')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('insurances');
	}

}

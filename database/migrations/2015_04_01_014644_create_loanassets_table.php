<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanassetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('loanassets', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('loan_id');
            $table->double('total_arm');
            $table->double('total_dist');
            $table->double('total_other');
            $table->double('total_total');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('loanassets');
	}

}

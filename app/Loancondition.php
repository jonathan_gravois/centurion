<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Loancondition extends Model {

    protected $table = 'loanconditions';
    protected $guarded = ['id'];
    protected $dates = array('action_date');
}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cropexpense extends Model {

    protected $table = 'cropexpenses';
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function crop()
    {
        return $this->belongsTo('App\Crop', 'crop_id');
    }

    public function loancrop()
    {
        return $this->belongsTo('App\Loancrop', 'loancrop_id');
    }
    /* RELATIONSHIPS */

}

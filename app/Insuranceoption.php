<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Insuranceoption extends Model {

    protected $table = 'insuranceoptions';
    public $timestamps = false;
    protected $guarded = ['id'];

}

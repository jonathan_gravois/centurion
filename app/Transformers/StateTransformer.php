<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class StateTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($state)
    {
        return [
            'id'		=> $state['id'],
            'state' 	=> $state['state'],
            'abr' 		=> $state['abr']
        ];
    }
}


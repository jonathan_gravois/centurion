<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class RequireddocumentTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return [
            'id'	=>	(integer) $arr['id'],
            'loantype_id'	=> (integer) $arr['loantype_id'],
            'loantype'	=>	$arr['loantype']['loantype'],
            'document' =>	$arr['document']
        ];
    }
}


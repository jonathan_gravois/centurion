<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class ScreenTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return [
            'id' =>	$arr['id'],
            'loantype_id'	=>	$arr['loantype_id'],
            'screen' =>	$arr['screen'],
            'label' =>	$arr['label'],
            'sort_order' => (integer) $arr['sort_order']
        ];
    }
}


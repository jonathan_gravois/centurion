<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class CropexpenseTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return [
            'id' =>	$arr['id'],
            'loan_id' => (integer) $arr['loan_id'],
            'crop_id' => (integer) $arr['crop_id'],
            'cat_id' => (integer) $arr['cat_id'],
            'expense' => $arr['expense'],
            'arm' => (double) $arr['arm'],
            'arm_adj' => (double) $arr['arm_adj'],
            'dist' => (double) $arr['dist'],
            'dist_adj' => (double) $arr['dist_adj'],
            'other' => (double) $arr['other'],
            'other_adj' => (double) $arr['other_adj'],
        ];
    }
}


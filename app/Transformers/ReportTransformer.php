<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class ReportTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return array(
            'id' =>	$arr['id'],
            'report' => $arr['report'],
            'rptPath' => $arr['rptPath'],
            'is_required' => (boolean) $arr['is_required']
        );
    }
}


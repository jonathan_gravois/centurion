<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class CroppracticeTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return [
            'id' =>	$arr['id'],
            'crop_id' => $arr['crop_id'],
            'crop' => $arr['crop'],
            'practice' => $arr['practice'],
            'measurement' => $arr['measurement'],
            'rebate_measurement' => $arr['rebate_measurement'],
        ];
    }
}


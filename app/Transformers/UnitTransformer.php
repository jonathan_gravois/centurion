<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class UnitTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        return [
            'id' =>	$arr['id'],
            'unit' =>	$arr['unit'],
            'abr' =>	$arr['abr'],
            'toPounds' =>	(double) $arr['toPounds'],
            'fromPounds' =>	(double) $arr['fromPounds']
        ];
    }
}


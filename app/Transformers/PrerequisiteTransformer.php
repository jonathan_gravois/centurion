<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class PrerequisiteTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return [
            'id' =>	$arr['id'],
            'loan_id' => $arr['loan_id'],
            'document' => $arr['document'],
            'date_requested' => $arr['date_requested'] ? $arr['date_requested']->format('m/d/Y') : null,
            'date_received' => $arr['date_received'] ? $arr['date_received']->format('m/d/Y') : null,
            'path' => $arr['path'],
            'filename' => $arr['filename'],
            'reason_pending' => $arr['reason_pending'],
        ];
    }
}


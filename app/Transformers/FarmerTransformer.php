<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Carbon\Carbon;

class FarmerTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;

        $dtToday = Carbon::now();
        $dtDOB = $arr['dob'];
        $age_of_farmer = $dtToday->diffInYears($dtDOB);

        return [
            'id' =>	$arr['id'],
            'farmer' => $arr['farmer'],
            'nick' => $arr['nick'],
            'address' => $arr['address'],
            'city' =>	$arr['city'],
            'state_id' =>	$arr['state_id'],
            'state' => $arr['state']['abr'],
            'zip' => $arr['zip'],
            'email' => $arr['email'],
            'phone' => $arr['phone'],
            'ssn' => $arr['ssn'],
            'dob' => $arr['dob']->format('m/d/Y'),
            'age' => $age_of_farmer,
            'first_year_farmer' => $arr['first_year_farmer'],
            'farm_exp' =>	$arr['farm_exp'],
            'new_client' => (boolean) $arr['new_client']
        ];
    }
}


<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class PdfappTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        // return $arr;
        return [
            'id' => $arr['id'],
            'title' => $arr['title'],
            'description' => $arr['description'],
            'pdfLink' => $arr['pdfLink'],
            'rank' => (integer) $arr['rank'],
            'visible' => (boolean) $arr['isVisible']
        ];
    }
}


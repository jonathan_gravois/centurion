<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class UserTransformer extends TransformerAbstract
{
	/**
	 * List of resources possible to include
	 *
	 * @var  array
	 */
	protected $availableIncludes = [];

	/**
	 * List of resources to automatically include
	 *
	 * @var  array
	 */
	protected $defaultIncludes = [];

	public function transform($array)
	{
        $arr = $array[0];
        //return $arr;
        return [
            'id' => (integer) $arr['id'],
            'username' => $arr['username'],
            'email' => $arr['email'],
            'role' => $arr['role']['role'],
            'role_abr' => $arr['role']['abr'],
            'portal' => $arr['portal']['portal'],
            'region' => $arr['region']['region'],
            'location' => (is_null($arr['location']) ? null : $arr['location']['location']),
            'loc_abr' => (is_null($arr['location']) ? null : $arr['location']['loc_abr']),
            'manager_id' => $arr['manager_id'],
            'manager' => $arr['manager']['username'],
            'is_admin' => (boolean) $arr['is_admin'],
            'is_approver' => (boolean) $arr['is_approver'],
            'is_manager' => (boolean) $arr['is_manager'],
            'view_options' => [
                'view_region' => (boolean) $arr['viewoptions']['view_region'],
                'view_season' => (boolean) $arr['viewoptions']['view_season'],
                'view_distributor' => (boolean) $arr['viewoptions']['view_distributor'],
                'view_agency' => (boolean) $arr['viewoptions']['view_agency'],
                'view_close_date' => (boolean) $arr['viewoptions']['view_close_date'],
                'view_commit_total' => (boolean) $arr['viewoptions']['view_commit_total'],
                'view_commit_arm' => (boolean) $arr['viewoptions']['view_commit_arm'],
                'view_commit_distributor' => (boolean) $arr['viewoptions']['view_commit_distributor'],
                'view_commit_other' => (boolean) $arr['viewoptions']['view_commit_other'],
                'view_fee_percentage' => (boolean) $arr['viewoptions']['view_fee_percentage'],
                'view_fee_total' => (boolean) $arr['viewoptions']['view_fee_total'],
                'view_rate_arm' => (boolean) $arr['viewoptions']['view_rate_arm'],
                'view_rate_dist' => (boolean) $arr['viewoptions']['view_rate_dist'],
                'view_balance_due' => (boolean) $arr['viewoptions']['view_balance_due'],
                'view_acres_total' => (boolean) $arr['viewoptions']['view_acres_total'],
                'view_acres_corn' => (boolean) $arr['viewoptions']['view_acres_corn'],
                'view_acres_soybeans' => (boolean) $arr['viewoptions']['view_acres_soybeans'],
                'view_acres_sorghum' => (boolean) $arr['viewoptions']['view_acres_sorghum'],
                'view_acres_wheat' => (boolean) $arr['viewoptions']['view_acres_wheat'],
                'view_acres_cotton' => (boolean) $arr['viewoptions']['view_acres_cotton'],
                'view_acres_rice' => (boolean) $arr['viewoptions']['view_acres_rice'],
                'view_acres_peanuts' => (boolean) $arr['viewoptions']['view_acres_peanuts'],
                'view_acres_sugar_cane' => (boolean) $arr['viewoptions']['view_acres_sugar_cane']
                ],
            'notifications' => $arr['notifications']
        ];
	}
}

?>

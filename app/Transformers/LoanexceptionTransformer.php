<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class LoanexceptionTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return array(
            'id' => $arr['id'],
            'loan_id' => $arr['loan_id'],
            'exception_id' => $arr['exception_id'],
            'exception' => $arr['exceptions']['title'],
            'msg' => $arr['msg']
        );
    }
}


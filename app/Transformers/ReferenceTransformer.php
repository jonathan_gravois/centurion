<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class ReferenceTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return [
            'id' => $arr['id'],
            'loan_id' => $arr['loan_id'],
            'creditor' => $arr['creditor'],
            'city_state' => $arr['city_state'],
            'contact' => $arr['contact'],
            'phone' => $arr['phone'],
            'email' => $arr['email']
        ];
    }
}


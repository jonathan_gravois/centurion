<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class DistributorTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return [
            'id' =>	$arr['id'],
            'distributor' => $arr['distributor'],
            'name' => $arr['name'],
            'address' => $arr['address'],
            'city' => $arr['city'],
            'state_id' => $arr['state_id'],
            'state' => $arr['state']['abr'],
            'zip' => $arr['zip'],
            'locale' => $arr['city'] . ', ' . $arr['state']['abr'],
            'phone' => $arr['phone'],
            'email' => $arr['email']
        ];
    }
}


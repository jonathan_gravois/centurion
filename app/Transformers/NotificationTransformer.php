<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class NotificationTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return [
            'user_id' => $arr['user_id'],
            'loan_id' => $arr['loan_id'],
            'notification_type' => $arr['notification_type'],
            'task' => $arr['task'],
            'status' => $arr['status']
        ];
    }
}


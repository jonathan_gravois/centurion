<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class RoleTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return array(
            'id' =>	$arr['id'],
            'abr' => $arr['abr'],
            'role' => $arr['role'],
            'employment_status' => $arr['employment_status'],
            'matrix' => $arr['matrix']
        );
    }
}


<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class MatrixTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return array(
            'id' =>	$arr['id'],
            'group_heading' =>	$arr['group_heading'],
            'responsibility' =>	$arr['responsibility'],
            'CEO' =>	$arr['CEO'],
            'ABM' =>	$arr['ABM'],
            'MGR' =>	$arr['MGR'],
            'OAS' =>	$arr['OAS'],
            'LBM' =>	$arr['LBM'],
            'LOF' =>	$arr['LOF'],
            'LAN' =>	$arr['LAN'],
            'CON' =>	$arr['CON'],
            'HRM' =>	$arr['HRM'],
            'IBM' =>	$arr['IBM'],
            'IAS' =>	$arr['IAS'],
            'COM' =>	$arr['COM']
        );
    }
}


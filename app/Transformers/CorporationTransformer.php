<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class CorporationTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return [
            'id' =>	$arr['id'],
            'corporation' => $arr['corporation'],
            'ssn' => $arr['ssn'],
            'address' => $arr['address'],
            'city' => $arr['city'],
            'state_id' => $arr['state_id'],
            'zip' => $arr['zip'],
            'email' => $arr['email'],
            'phone' => $arr['phone'],
            'description' => $arr['description'],
            'president' => $arr['president'],
            'vicepresident' => $arr['vicepresident'],
            'secretary' => $arr['secretary'],
            'treasurer' => $arr['treasurer']
        ];
    }
}


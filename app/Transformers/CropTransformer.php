<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class CropTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return [
            'id' =>	(integer) $arr['id'],
            'crop' => $arr['crop'],
            'tea' => $arr['tea'],
            'arm_default_price' => (double) $arr['arm_default_price'],
            'arm_default_ins_price' => (double) $arr['arm_default_ins_price'],
            'arm_default_yield' => (double) $arr['arm_default_yield'],
            'measurement' => $arr['measurement'],
            'rebate_measurement' => $arr['rebate_measurement']
        ];
    }
}


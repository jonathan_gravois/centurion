<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class CountyTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return [
            'id'		=> $arr['id'],
            'county' 	=> $arr['county'],
            'state_id' 	=> $arr['state_id'],
            'label'		=> $arr['label'],
            'locale'	=> $arr['locale']
        ];
    }
}


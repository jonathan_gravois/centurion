<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class AgencyTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

	public function transform($arr)
	{
        //return $arr;
        return [
            'id' =>	$arr['id'],
            'agency' => $arr['agency'],
            'address' => $arr['address'],
            'city' => $arr['city'],
            'state_id' => $arr['state_id'],
            'state' => $arr['state'],
            'zip' => $arr['zip'],
            'phone' => $arr['phone'],
            'email' => $arr['email']
        ];
	}
}


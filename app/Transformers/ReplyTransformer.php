<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class ReplyTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return array(
            'id' =>	$arr['id'],
            'loan_id' => $arr['loan_id'],
            'comment_id' => $arr['comment_id'],
            'user_id' => $arr['user_id'],
            'body' =>$arr['body'],
            'created' => $arr['created_at']->format('m/d/Y')
        );
    }
}


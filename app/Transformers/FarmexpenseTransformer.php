<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class FarmexpenseTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return [
            'id'			=>	(integer) $arr['id'],
            'expense'		=>	$arr['expense'],
            'cost'			=>	(double) $arr['cost']
        ];
    }
}


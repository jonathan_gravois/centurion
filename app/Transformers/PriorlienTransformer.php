<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class PriorlienTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return [
            'id' => $arr['id'],
            'loan_id' => $arr['loan_id'],
            'lien_holder' => $arr['lien_holder'],
            'city_state' => $arr['city_state'],
            'contact' => $arr['contact'],
            'phone' => $arr['phone'],
            'email' => $arr['email'],
            'projected_crops' => $arr['projected_crops'],
            'ins_over_discount' => $arr['ins_over_discount'],
            'fsa_payments' => $arr['fsa_payments'],
            'claims' => $arr['claims'],
            'equipment' => $arr['equipment'],
            'realestate' => $arr['realestate'],
            'total' => $arr['total']
        ];
    }
}


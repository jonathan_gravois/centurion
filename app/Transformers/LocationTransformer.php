<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class LocationTransformer extends TransformerAbstract
{
	/**
	 * List of resources possible to include
	 *
	 * @var  array
	 */
	protected $availableIncludes = [];

	/**
	 * List of resources to automatically include
	 *
	 * @var  array
	 */
	protected $defaultIncludes = [];

	public function transform($arr)
	{
        //return $arr;
        return [
            'id' => (integer) $arr['id'],
            'location' => $arr['location'],
            'loc_abr' => $arr['loc_abr'],
            'region_id' => $arr['region_id'],
            'region' => $arr['region->region'],
            'address' => $arr['address'],
            'city' => $arr['city'],
            'state' => $arr['state'],
            'zip' => $arr['zip'],
            'phone' => $arr['phone'],
            'manager_id' => (integer) $arr['manager_id'],
            'manager' => $arr['manager']['username']
        ];
	}
}

?>

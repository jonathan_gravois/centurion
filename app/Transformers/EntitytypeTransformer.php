<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class EntitytypeTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        return $arr;
        return [
            'id' =>	$arr['id'],
            'loantype' => $arr['loantype'],
            'ltPath' => $arr['ltPath']
        ];
    }
}


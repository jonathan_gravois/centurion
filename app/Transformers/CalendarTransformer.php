<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class CalendarTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        //return $arr;
        return [
            'id' => $arr['id'],
            'title' => $arr['title'],
            'start' => $arr['start']->format('Y-m-d h:i:s'),
            'end' => $arr['end']->format('Y-m-d h:i:s'),
            'allDay' => (boolean) $arr['allDay'],
            'durationEditable' => (boolean) $arr['durationEditable'],
            'calendar_group' => $arr['calendar_group'],
            'event_access' => $arr['event_access'],
            'event_access' => $arr['event_access']
        ];
    }
}


<?php namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class LoanconditionTransformer extends TransformerAbstract
{
	protected $availableIncludes = [];
    protected $defaultIncludes = [];

    public function transform($arr)
    {
        if(!$arr['action_date']){
            $action_date = null;
        } else {
            $action_date = $arr['action_date']->format('m/d/Y');
        }

        return [
            'id' => $arr['id'],
            'crop_year' => $arr['crop_year'],
            'loan_id' => $arr['loan_id'],
            'condition_id' => $arr['condition_id'],
            'condition' => $arr['condition'],
            'status' => $arr['status'],
            'action_date' => $action_date
        ];
    }
}


<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicationglobal extends Model {

    protected $table = 'applicationglobals';
    public $timestamps = false;
    protected $guarded = ['id'];

}

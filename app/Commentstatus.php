<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Commentstatus extends Model {

    protected $table = 'commentstatus';
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function comment()
    {
        return $this->belongsTo('App\Comment', 'comment_id');
    }
    /* RELATIONSHIPS */

}

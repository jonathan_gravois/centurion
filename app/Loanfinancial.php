<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Loanfinancial extends Model {

    protected $table = 'loanfinancials';
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function loan()
    {
        return $this->belongsTo('App\Loan', 'loan_id');
    }
    /* RELATIONSHIPS */

}

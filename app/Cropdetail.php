<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cropdetail extends Model {

    protected $table = 'cropdetails';
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function loan()
    {
        return $this->belongsTo('App\Loan', 'loan_id');
    }

    public function crop()
    {
        return $this->belongsTo('App\Crop', 'crop_id');
    }

    public function cropyield()
    {
        return $this->hasMany('App\Cropyield');
    }
    /* RELATIONSHIPS */
}

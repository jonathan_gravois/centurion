<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Loancrop extends Model {

    protected $table = 'loancrops';
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function crop()
    {
        return $this->belongsTo('App\Crop', 'crop_id');
    }

    public function croppractice()
    {
        return $this->belongsTo('App\Croppractices', 'croppractice_id');
    }

    public function farm()
    {
        return $this->belongsTo('App\Farm', 'farm_id');
    }

    public function farmcrop()
    {
        return $this->hasMany('App\Farmcrop');
    }

    public function expenses()
    {
        return $this->hasMany('App\Cropexpenses');
    }

    public function loan()
    {
        return $this->belongsTo('App\Loan', 'loan_id');
    }
    /* RELATIONSHIPS */

}

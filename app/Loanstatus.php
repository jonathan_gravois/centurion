<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Loanstatus extends Model {

    protected $table = 'loanstatuses';
    public $timestamps = false;
    protected $guarded = ['id'];

}

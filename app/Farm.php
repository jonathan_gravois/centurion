<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Farm extends Model {

    protected $table = 'farms';
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function loan()
    {
        return $this->belongsTo('App\Loan', 'loan_id');
    }

    public function county()
    {
        return $this->belongsTo('App\County','county_id');
    }

    public function details(){
        return $this->belongsTo('App\Farmcrop');
    }

    public function loanpractices()
    {
        return $this->hasMany('App\Loanpractice');
    }
    /* RELATIONSHIPS */

}

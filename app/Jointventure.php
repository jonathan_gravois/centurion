<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Jointventure extends Model {

    protected $table = 'jointventures';
    protected $guarded = ['id'];

    /* RELATIONSHIP */
    public function states()
    {
        return $this->belongsTo('App\State', 'state_id');
    }
    /* RELATIONSHIP */

}

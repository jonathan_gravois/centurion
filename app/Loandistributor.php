<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Loandistributor extends Model {

    protected $table = 'loandistributors';
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function distributor()
    {
        return $this->belongsTo('App\Distributor', 'distributor_id');
    }
    /* RELATIONSHIPS */

}

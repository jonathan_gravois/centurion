<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Screen extends Model {

    protected $table = 'screens';
    public $timestamps = false;
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function loantypes()
    {
        return $this->belongsTo('App\Loantypes', 'loantype_id');
    }
    /* RELATIONSHIPS */

}

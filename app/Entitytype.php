<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Entitytype extends Model {

    protected $table = 'entitytypes';
    public $timestamps = false;
    protected $guarded = ['id'];

}

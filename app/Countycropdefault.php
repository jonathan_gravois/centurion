<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Countycropdefault extends Model {

    protected $table = 'countycropdefaults';
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function county()
    {
        return $this->belongsTo('App\County', 'county_id');
    }
    /* RELATIONSHIPS */

}

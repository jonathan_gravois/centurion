<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Farmexpense extends Model {

    protected $table = 'farmexpenses';
    protected $guarded = ['id'];

}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Croppractice extends Model {

    protected $table = 'croppractices';
    public $timestamps = false;
    protected $guarded = ['id'];

}

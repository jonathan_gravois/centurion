<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Viewoption extends Model {

    protected $table = 'viewoptions';
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    /* RELATIONSHIPS */

}

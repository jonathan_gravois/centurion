<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model {

    protected $table = 'replies';
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function comments()
    {
        return $this->belongsTo('App\Comments', 'comment_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    /* RELATIONSHIPS */

}

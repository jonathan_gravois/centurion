<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Spendcat extends Model {

    protected $table = 'spendcats';
    public $timestamps = false;
    protected $guarded = ['id'];

}

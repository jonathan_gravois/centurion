<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Farmer extends Model {

    protected $table = 'farmers';
    protected $guarded = ['id'];
    protected $dates = array('dob');

    /* RELATIONSHIPS */
    public function location()
    {
        return $this->belongsTo('App\Location', 'loc_id');
    }

    public function state()
    {
        return $this->belongsTo('App\State', 'state_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    /* RELATIONSHIPS */

}

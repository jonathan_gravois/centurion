<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model {

    protected $table = 'units';
    public $timestamps = false;
    protected $guarded = ['id'];

    /* RELATIONSHIPS */

    /* RELATIONSHIPS */

    /* METHODS */

    /* METHODS */

}

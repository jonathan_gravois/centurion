<?php namespace App\Http\Controllers;

use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use App\Role as Role;
use App\Transformers\RoleTransformer;

class RolesController extends ApiGuardController {

    /*protected $apiMethods = [
        'index' => [
            'keyAuthentication' => false
        ]
    ];*/

	public function index()
	{
		$all = Role::all();
		return $this->response->withCollection($all, new RoleTransformer);
	}

	public function show($id)
	{
		try {
		    $single = Role::findOrFail($id);
		    return $this->response->withItem($single, new RoleTransformer);
		} catch (ModelNotFoundException $e) {
		    return $this->response->errorNotFound();
		}
	}
	
	public function destroy($id)
    {
        Role::where('id', $id)->delete();
        return response()->json(['data' => 'Deleted successfully'], 200);
    }
    
    public function store()
	{
        if( ! Input::get('')){
            return response()->json(['error' => 'invalid_credentials'], 406);
        } // end if

        Role::create(Input::all());

        return response()->json(['data' => 'Created successfully'], 200);
	}

	public function update($id)
	{
        $single = Role::find($id);

        if(!$single){
          Role::create(Input::all());
          return response()->json(['data' => 'Created successfully'], 200);
        }

        $single->fill(Input::all())->save();

        return response()->json(['data' => 'Updated successfully'], 200);
	}

}

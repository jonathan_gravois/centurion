<?php namespace App\Http\Controllers;

use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use App\Countycropdefault as Countycropdefault;
use App\Transformers\CountycropdefaultTransformer;

class CountycropdefaultsController extends ApiGuardController {

    /*protected $apiMethods = [
        'index' => [
            'keyAuthentication' => false
        ]
    ];*/

	public function index()
	{
		$all = Countycropdefault::all();
		return $this->response->withCollection($all, new CountycropdefaultTransformer);
	}

	public function show($id)
	{
		try {
		    $single = Countycropdefault::findOrFail($id);
		    return $this->response->withItem($single, new CountycropdefaultTransformer);
		} catch (ModelNotFoundException $e) {
		    return $this->response->errorNotFound();
		}
	}
	
	public function destroy($id)
    {
        Countycropdefault::where('id', $id)->delete();
        return response()->json(['data' => 'Deleted successfully'], 200);
    }
    
    public function store()
	{
        if( ! Input::get('')){
            return response()->json(['error' => 'invalid_credentials'], 406);
        } // end if

        Countycropdefault::create(Input::all());

        return response()->json(['data' => 'Created successfully'], 200);
	}

	public function update($id)
	{
        $single = Countycropdefault::find($id);

        if(!$single){
          Countycropdefault::create(Input::all());
          return response()->json(['data' => 'Created successfully'], 200);
        }

        $single->fill(Input::all())->save();

        return response()->json(['data' => 'Updated successfully'], 200);
	}

}

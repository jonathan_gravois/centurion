<?php namespace App\Http\Controllers;

use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use App\Profile as Profile;
use App\Transformers\ProfileTransformer;

class ProfilesController extends ApiGuardController {

    /*protected $apiMethods = [
        'index' => [
            'keyAuthentication' => false
        ]
    ];*/

	public function index()
	{
		$all = Profile::all();
		return $this->response->withCollection($all, new ProfileTransformer);
	}

	public function show($id)
	{
		try {
		    $single = Profile::findOrFail($id);
		    return $this->response->withItem($single, new ProfileTransformer);
		} catch (ModelNotFoundException $e) {
		    return $this->response->errorNotFound();
		}
	}
	
	public function destroy($id)
    {
        Profile::where('id', $id)->delete();
        return response()->json(['data' => 'Deleted successfully'], 200);
    }
    
    public function store()
	{
        if( ! Input::get('')){
            return response()->json(['error' => 'invalid_credentials'], 406);
        } // end if

        Profile::create(Input::all());

        return response()->json(['data' => 'Created successfully'], 200);
	}

	public function update($id)
	{
        $single = Profile::find($id);

        if(!$single){
          Profile::create(Input::all());
          return response()->json(['data' => 'Created successfully'], 200);
        }

        $single->fill(Input::all())->save();

        return response()->json(['data' => 'Updated successfully'], 200);
	}

}

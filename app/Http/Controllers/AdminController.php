<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\User;

class AdminController extends Controller {

    function __construct()
    {

    }

    public function index()
    {
        $users = User::with('portal')->get();
        return view('Admin.home', compact('users'));
    }
    public function agencies()
    {
        return view('Admin.agencies');
    }
    public function applicants()
    {
        return view('Admin.applicants');
    }
    public function applicationvalues()
    {
        return view('Admin.applicationvalues');
    }
    public function archiveloans()
    {
        return view('Admin.archiveloans');
    }
    public function backuplenda()
    {
        return view('Admin.backuplenda');
    }
    public function calendar()
    {
        return view('Admin.calendar');
    }
    public function committeespecs()
    {
        return view('Admin.committeespecs');
    }
    public function crops()
    {
        return view('Admin.crops');
    }
    public function defaultexpenses()
    {
        return view('Admin.defaultexpenses');
    }
    public function distributors()
    {
        return view('Admin.distributors');
    }
    public function entitytypes()
    {
        return view('Admin.entitytypes');
    }
    public function farmers()
    {
        return view('Admin.farmers');
    }
    public function graderspecs()
    {
        return view('Admin.graderspecs');
    }
    public function importqb()
    {
        return view('Admin.importqb');
    }
    public function insurancetypes()
    {
        return view('Admin.insurancetypes');
    }
    public function insurancevalues()
    {
        return view('Admin.insurancevalues');
    }
    public function legaldocs()
    {
        return view('Admin.legaldocs');
    }
    public function loanapps()
    {
        return view('Admin.loanapps');
    }
    public function loanproducts()
    {
        return view('Admin.loanproducts');
    }
    public function loans()
    {
        return view('Admin.loans');
    }
    public function loantypes()
    {
        return view('Admin.loantypes');
    }
    public function locations()
    {
        return view('Admin.locations');
    }
    public function matrix()
    {
        return view('Admin.matrix');
    }
    public function newloanscreens()
    {
        return view('Admin.newloanscreens');
    }
    public function policies()
    {
        return view('Admin.policies');
    }
    public function prerequisites()
    {
        return view('Admin.prerequisites');
    }
    public function regions()
    {
        return view('Admin.regions');
    }
    public function reports()
    {
        return view('Admin.reports');
    }
    public function resources()
    {
        return view('Admin.resources');
    }
    public function roles()
    {
        return view('Admin.roles');
    }
    public function staff()
    {
        return view('Admin.staff');
    }
    public function unitconvertor()
    {
        return view('Admin.unitconvertor');
    }
    public function units()
    {
        return view('Admin.units');
    }
}

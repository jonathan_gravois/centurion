<?php namespace App\Http\Controllers;

use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use App\Insuranceoption as Insuranceoption;
use App\Transformers\InsuranceoptionTransformer;

class InsuranceoptionsController extends ApiGuardController {

    /*protected $apiMethods = [
        'index' => [
            'keyAuthentication' => false
        ]
    ];*/

	public function index()
	{
		$all = Insuranceoption::all();
		return $this->response->withCollection($all, new InsuranceoptionTransformer);
	}

	public function show($id)
	{
		try {
		    $single = Insuranceoption::findOrFail($id);
		    return $this->response->withItem($single, new InsuranceoptionTransformer);
		} catch (ModelNotFoundException $e) {
		    return $this->response->errorNotFound();
		}
	}
	
	public function destroy($id)
    {
        Insuranceoption::where('id', $id)->delete();
        return response()->json(['data' => 'Deleted successfully'], 200);
    }
    
    public function store()
	{
        if( ! Input::get('')){
            return response()->json(['error' => 'invalid_credentials'], 406);
        } // end if

        Insuranceoption::create(Input::all());

        return response()->json(['data' => 'Created successfully'], 200);
	}

	public function update($id)
	{
        $single = Insuranceoption::find($id);

        if(!$single){
          Insuranceoption::create(Input::all());
          return response()->json(['data' => 'Created successfully'], 200);
        }

        $single->fill(Input::all())->save();

        return response()->json(['data' => 'Updated successfully'], 200);
	}

}

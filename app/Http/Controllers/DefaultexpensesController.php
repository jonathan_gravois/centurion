<?php namespace App\Http\Controllers;

use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use App\Defaultexpense as Defaultexpense;
use App\Transformers\DefaultexpenseTransformer;

class DefaultexpensesController extends ApiGuardController {

    /*protected $apiMethods = [
        'index' => [
            'keyAuthentication' => false
        ]
    ];*/

	public function index()
	{
		$all = Defaultexpense::all();
		return $this->response->withCollection($all, new DefaultexpenseTransformer);
	}

	public function show($id)
	{
		try {
		    $single = Defaultexpense::findOrFail($id);
		    return $this->response->withItem($single, new DefaultexpenseTransformer);
		} catch (ModelNotFoundException $e) {
		    return $this->response->errorNotFound();
		}
	}
	
	public function destroy($id)
    {
        Defaultexpense::where('id', $id)->delete();
        return response()->json(['data' => 'Deleted successfully'], 200);
    }
    
    public function store()
	{
        if( ! Input::get('')){
            return response()->json(['error' => 'invalid_credentials'], 406);
        } // end if

        Defaultexpense::create(Input::all());

        return response()->json(['data' => 'Created successfully'], 200);
	}

	public function update($id)
	{
        $single = Defaultexpense::find($id);

        if(!$single){
          Defaultexpense::create(Input::all());
          return response()->json(['data' => 'Created successfully'], 200);
        }

        $single->fill(Input::all())->save();

        return response()->json(['data' => 'Updated successfully'], 200);
	}

}

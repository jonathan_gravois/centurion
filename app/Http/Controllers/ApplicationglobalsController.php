<?php namespace App\Http\Controllers;

use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use App\Applicationglobal as Applicationglobal;
use App\Transformers\ApplicationglobalTransformer;

class ApplicationglobalsController extends ApiGuardController {

    /*protected $apiMethods = [
        'index' => [
            'keyAuthentication' => false
        ]
    ];*/

	public function index()
	{
		$all = Applicationglobal::all();
		return $this->response->withCollection($all, new ApplicationglobalTransformer);
	}

	public function show($id)
	{
		try {
		    $single = Applicationglobal::findOrFail($id);
		    return $this->response->withItem($single, new ApplicationglobalTransformer);
		} catch (ModelNotFoundException $e) {
		    return $this->response->errorNotFound();
		}
	}
	
	public function destroy($id)
    {
        Applicationglobal::where('id', $id)->delete();
        return response()->json(['data' => 'Deleted successfully'], 200);
    }
    
    public function store()
	{
        if( ! Input::get('')){
            return response()->json(['error' => 'invalid_credentials'], 406);
        } // end if

        Applicationglobal::create(Input::all());

        return response()->json(['data' => 'Created successfully'], 200);
	}

	public function update($id)
	{
        $single = Applicationglobal::find($id);

        if(!$single){
          Applicationglobal::create(Input::all());
          return response()->json(['data' => 'Created successfully'], 200);
        }

        $single->fill(Input::all())->save();

        return response()->json(['data' => 'Updated successfully'], 200);
	}

}

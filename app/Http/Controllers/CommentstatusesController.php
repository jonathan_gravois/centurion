<?php namespace App\Http\Controllers;

use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use App\Commentstatus as Commentstatus;
use App\Transformers\CommentstatusTransformer;

class CommentstatusesController extends ApiGuardController {

    /*protected $apiMethods = [
        'index' => [
            'keyAuthentication' => false
        ]
    ];*/

	public function index()
	{
		$all = Commentstatus::all();
		return $this->response->withCollection($all, new CommentstatusTransformer);
	}

	public function show($id)
	{
		try {
		    $single = Commentstatus::findOrFail($id);
		    return $this->response->withItem($single, new CommentstatusTransformer);
		} catch (ModelNotFoundException $e) {
		    return $this->response->errorNotFound();
		}
	}
	
	public function destroy($id)
    {
        Commentstatus::where('id', $id)->delete();
        return response()->json(['data' => 'Deleted successfully'], 200);
    }
    
    public function store()
	{
        if( ! Input::get('')){
            return response()->json(['error' => 'invalid_credentials'], 406);
        } // end if

        Commentstatus::create(Input::all());

        return response()->json(['data' => 'Created successfully'], 200);
	}

	public function update($id)
	{
        $single = Commentstatus::find($id);

        if(!$single){
          Commentstatus::create(Input::all());
          return response()->json(['data' => 'Created successfully'], 200);
        }

        $single->fill(Input::all())->save();

        return response()->json(['data' => 'Updated successfully'], 200);
	}

}

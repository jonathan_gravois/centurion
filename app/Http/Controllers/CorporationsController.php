<?php namespace App\Http\Controllers;

use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use App\Corporation as Corporation;
use App\Transformers\CorporationTransformer;

class CorporationsController extends ApiGuardController {

    /*protected $apiMethods = [
        'index' => [
            'keyAuthentication' => false
        ]
    ];*/

	public function index()
	{
		$all = Corporation::all();
		return $this->response->withCollection($all, new CorporationTransformer);
	}

	public function show($id)
	{
		try {
		    $single = Corporation::findOrFail($id);
		    return $this->response->withItem($single, new CorporationTransformer);
		} catch (ModelNotFoundException $e) {
		    return $this->response->errorNotFound();
		}
	}
	
	public function destroy($id)
    {
        Corporation::where('id', $id)->delete();
        return response()->json(['data' => 'Deleted successfully'], 200);
    }
    
    public function store()
	{
        if( ! Input::get('')){
            return response()->json(['error' => 'invalid_credentials'], 406);
        } // end if

        Corporation::create(Input::all());

        return response()->json(['data' => 'Created successfully'], 200);
	}

	public function update($id)
	{
        $single = Corporation::find($id);

        if(!$single){
          Corporation::create(Input::all());
          return response()->json(['data' => 'Created successfully'], 200);
        }

        $single->fill(Input::all())->save();

        return response()->json(['data' => 'Updated successfully'], 200);
	}

}

<?php namespace App\Http\Controllers;

use App\User as User;

class WelcomeController extends Controller {

	public function __construct()
	{
		$this->middleware('guest');
	}

	public function index()
	{
		$all = User::all();
		dd($all);
		return view('welcome')->with($all);
	}

}

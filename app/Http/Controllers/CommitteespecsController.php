<?php namespace App\Http\Controllers;

use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use App\Committeespec as Committeespec;
use App\Transformers\CommitteespecTransformer;

class CommitteespecsController extends ApiGuardController {

    /*protected $apiMethods = [
        'index' => [
            'keyAuthentication' => false
        ]
    ];*/

	public function index()
	{
		$all = Committeespec::all();
		return $this->response->withCollection($all, new CommitteespecTransformer);
	}

	public function show($id)
	{
		try {
		    $single = Committeespec::findOrFail($id);
		    return $this->response->withItem($single, new CommitteespecTransformer);
		} catch (ModelNotFoundException $e) {
		    return $this->response->errorNotFound();
		}
	}
	
	public function destroy($id)
    {
        Committeespec::where('id', $id)->delete();
        return response()->json(['data' => 'Deleted successfully'], 200);
    }
    
    public function store()
	{
        if( ! Input::get('')){
            return response()->json(['error' => 'invalid_credentials'], 406);
        } // end if

        Committeespec::create(Input::all());

        return response()->json(['data' => 'Created successfully'], 200);
	}

	public function update($id)
	{
        $single = Committeespec::find($id);

        if(!$single){
          Committeespec::create(Input::all());
          return response()->json(['data' => 'Created successfully'], 200);
        }

        $single->fill(Input::all())->save();

        return response()->json(['data' => 'Updated successfully'], 200);
	}

}

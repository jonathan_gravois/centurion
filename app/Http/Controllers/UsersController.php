<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;

use App\User as User;

class UsersController extends Controller {

	public function index()
	{
        $all = User::all();
        return $all;
        return Fractal::collection($all, new UsersTransformer)->responseJson(200);
	}

}

<?php

Route::get('/', 'LandingController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::group(
    ['prefix' => 'admin'],
    function () {
        Route::get('agencies', 'AdminController@agencies');
        Route::get('applicants', 'AdminController@applicants');
        Route::get('applicationvalues', 'AdminController@applicationvalues');
        Route::get('archiveloans', 'AdminController@archiveloans');
        Route::get('backuplenda', 'AdminController@backuplenda');
        Route::get('calendar', 'AdminController@calendar');
        Route::get('committeespecs', 'AdminController@committeespecs');
        Route::get('crops', 'AdminController@crops');
        Route::get('defaultexpenses', 'AdminController@defaultexpenses');
        Route::get('distributors', 'AdminController@distributors');
        Route::get('entitytypes', 'AdminController@entitytypes');
        Route::get('farmers', 'AdminController@farmers');
        Route::get('graderspecs', 'AdminController@graderspecs');
        Route::get('home', 'AdminController@index');
        Route::get('insurancevalues', 'AdminController@insurancevalues');
        Route::get('importqb', 'AdminController@importqb');
        Route::get('insurancetypes', 'AdminController@insurancetypes');
        Route::get('legaldocs', 'AdminController@legaldocs');
        Route::get('loanapps', 'AdminController@loanapps');
        Route::get('loanproducts', 'AdminController@loanproducts');
        Route::get('loans', 'AdminController@loans');
        Route::get('loantypes', 'AdminController@loantypes');
        Route::get('locations', 'AdminController@locations');
        Route::get('matrix', 'AdminController@matrix');
        Route::get('newloanscreens', 'AdminController@newloanscreens');
        Route::get('policies', 'AdminController@policies');
        Route::get('prerequisites', 'AdminController@prerequisites');
        Route::get('regions', 'AdminController@regions');
        Route::get('reports', 'AdminController@reports');
        Route::get('resources', 'AdminController@resources');
        Route::get('roles', 'AdminController@roles');
        Route::get('staff', 'AdminController@staff');
        Route::get('unitconvertor', 'AdminController@unitconvertor');
        Route::get('units', 'AdminController@units');
    }
);

Route::group(
    ['prefix' => 'v1', 'after' => 'allowOrigin'],
    function(){
        Route::resource('addendumtypes', 'AddendumtypesController');
        Route::resource('addfins', 'AddfinsController');
        Route::resource('addloans', 'AddloansController');
        Route::resource('admingrader', 'AdmingradersController');
        Route::resource('agencies', 'AgenciesController');
        Route::resource('agents', 'AgentsController');
        Route::resource('applicants', 'ApplicantsController');
        Route::resource('calendars', 'CalendarsController');
        Route::resource('comments', 'CommentsController');
        Route::resource('commentstatus', 'CommentstatusesController');
        Route::resource('committees', 'CommitteesController');
        Route::resource('committeespecs', 'CommitteespecsController');
        Route::resource('conditions', 'ConditionsController');
        Route::resource('corporations', 'CorporationsController');
        Route::resource('counties', 'CountiesController');
        Route::resource('countycropdefaults', 'CountycropdefaultsController');
        Route::resource('crops', 'CropsController');
        Route::resource('cropdetails', 'CropdetailsController');
        Route::resource('cropexpenses', 'CropexpensesController');
        Route::resource('croppractices', 'CroppracticesController');
        Route::resource('defaultexpenses', 'DefaultexpensesController');
        Route::resource('distributors', 'DistributorsController');
        Route::resource('entitytypes', 'EntitytypesController');
        Route::resource('exceptions', 'ExceptionsController');
        Route::resource('expenses', 'ExpensesController');
        Route::resource('farms', 'FarmsController');
        Route::resource('farmcrops', 'FarmcropsController');
        Route::resource('farmers', 'FarmersController');
        Route::resource('farmexpenses', 'FarmexpensesController');
        Route::resource('farmpractices', 'FarmpracticesController');
        Route::resource('globals', 'ApplicationglobalsController');
        Route::resource('grainstorages', 'GrainstoragesController');
        Route::resource('guarantors', 'GuarantorsController');
        Route::resource('insurances', 'InsurancesController');
        Route::resource('insuranceoptions', 'InsuranceoptionsController');
        Route::resource('insurancetypes', 'InsurancetypesController');
        Route::resource('jointventures', 'JointventuresController');
        Route::resource('loans', 'LoansController');
        Route::resource('loanassets', 'LoanassetsController');
        Route::resource('loancapacities', 'LoancapacitiesController');
        Route::resource('loanconditions', 'LoanconditionsController');
        Route::resource('loancrops', 'LoancropsController');
        Route::resource('loandistributors', 'LoandistributorsController');
        Route::resource('loanexceptions', 'LoanexceptionsController');
        Route::resource('loanfinancials', 'LoanfinancialsController');
        Route::resource('loanpractices', 'LoanpracticesController');
        Route::resource('loanquestions', 'LoanquestionsController');
        Route::resource('loanstatuses', 'LoanstatusesController');
        Route::resource('loantypes', 'LoantypesController');
        Route::resource('locations', 'LocationsController');
        Route::resource('matrices', 'MatricesController');
        Route::resource('notifications', 'NotificationsController');
        Route::resource('partners', 'PartnersController');
        Route::resource('pdfapps', 'PdfappsController');
        Route::resource('portals', 'PortalsController');
        Route::resource('prerequisites', 'PrerequisitesController');
        Route::resource('priorliens', 'PriorliensController');
        Route::resource('profiles', 'ProfilesController');
        Route::resource('references', 'ReferencesController');
        Route::resource('regions', 'RegionsController');
        Route::resource('reports', 'ReportsController');
        Route::resource('replies', 'RepliesController');
        Route::resource('requireddocuments', 'RequireddocumentsController');
        Route::resource('roles', 'RolesController');
        Route::resource('screens', 'ScreensController');
        Route::resource('spendcats', 'SpendcatsController');
        Route::resource('states', 'StatesController');
        Route::resource('systemics', 'SystemicsController');
        Route::resource('units', 'UnitsController');
        Route::resource('users', 'UsersController');
        Route::resource('viewoptions', 'ViewoptionsController');
    }
);

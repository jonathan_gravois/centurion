<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Loanexception extends Model {

    protected $table = 'loanexceptions';
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function exceptions()
    {
        return $this->belongsTo('App\Exceptions', 'exception_id');
    }
    /* RELATIONSHIPS */

}

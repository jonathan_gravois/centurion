<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model {

    protected $table = 'partners';
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function states()
    {
        return $this->belongsTo('App\State', 'state_id');
    }
    /* RELATIONSHIPS */

}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Loanpractice extends Model {

    protected $table = 'loanpractices';
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function crop()
    {
        return $this->belongsTo('App\Crop', 'crop_id');
    }
    /* RELATIONSHIPS */

}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Ratioconstraint extends Model {

    protected $table = 'ratioconstraints';
    public $timestamps = false;
    protected $guarded = ['id'];

}

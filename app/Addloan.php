<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Addloan extends Model {

    protected $table = 'addloans';
    protected $guarded = ['id'];
    protected $dates = array('app_date', 'distributor_approval_date', 'decision_date', 'due_date', 'default_due_date');

    /* RELATIONSHIPS */
    public function addendumtype()
    {
        return $this->hasOne('Addendumtype', 'id', 'addendum_type');
    }

    public function addfins()
    {
        return $this->hasOne('Addfin', 'loan_id', 'id');
    }
    /* RELATIONSHIPS */
}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model {

    protected $table = 'applicants';
    protected $guarded = ['id'];
    protected $dates = array('dob');

    /* RELATIONSHIPS */
    public function entitytype()
    {
        return $this->belongsTo('App\Entitytype', 'entity_id');
    }

    public function farmer()
    {
        return $this->belongsTo('App\Farmer', 'farmer_id');
    }

    public function loan()
    {
        return $this->hasMany('App\Loan', 'applicant_id');
    }

    public function location()
    {
        return $this->belongsTo('App\Location', 'loc_id');
    }

    public function state()
    {
        return $this->belongsTo('App\State', 'state_id');
    }
    /* RELATIONSHIPS */
}

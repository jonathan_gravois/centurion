<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Insurancetype extends Model {

    protected $table = 'insurancetypes';
    public $timestamps = false;
    protected $guarded = ['id'];

}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Crop extends Model {

    protected $table = 'crops';
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function cropdetails()
    {
        return $this->hasOne('App\Cropdetails');
    }

    public function loan()
    {
        return $this->belongsToMany('App\Loan');
    }
    /* RELATIONSHIPS */

}

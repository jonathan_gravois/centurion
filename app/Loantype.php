<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Loantype extends Model {

    protected $table = 'loantypes';
    public $timestamps = false;
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function reqdocs()
    {
        return $this->hasMany('App\Requireddocuments');
    }
    /* RELATIONSHIPS */

}

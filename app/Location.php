<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model {

    protected $table = 'locations';
    public $timestamps = false;
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function counties()
    {
        return $this->hasMany('App\County');
    }
    public function manager()
    {
        return $this->belongsTo('App\User', 'manager_id');
    }
    public function region()
    {
        return $this->belongsTo('App\Region');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    /* RELATIONSHIPS */

    /* METHODS */

    /* METHODS */

}

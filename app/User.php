<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	protected $table = 'users';
	protected $guarded = ['id'];
	protected $hidden = ['password', 'remember_token'];
	
    /* RELATIONSHIPS */
    public function location()
    {
        return $this->belongsTo('App\Location', 'loc_id');
    }
    public function manager()
    {
        return $this->belongsTo('App\User', 'manager_id');
    }
    public function notifications()
    {
        return $this->hasMany('App\Notification');
    }
    public function portal()
    {
        return $this->belongsTo('App\Portal', 'portal_id');
    }
    public function profile()
    {
        return $this->hasOne('App\Profile');
    }
    public function region()
    {
        return $this->belongsTo('App\Region', 'region_id');
    }
    public function role()
    {
        return $this->belongsTo('App\Role', 'role_id');
    }
    public function viewoptions()
    {
        return $this->belongsTo('App\Viewoption', 'id', 'user_id');
    }
    /* RELATIONSHIPS */

    /* METHODS */
    public function setPasswordAttribute($value){
        $this->attributes['password'] = bcrypt($value);
    }
    /* METHODS */

}

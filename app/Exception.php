<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Exception extends Model {

    protected $table = 'exceptions';
    public $timestamps = false;
    protected $guarded = ['id'];

}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model {

    protected $table = 'regions';
    public $timestamps = false;
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function loans()
    {
        return $this->belongsTo('App\Loan');
    }
    /* RELATIONSHIPS */

    /* METHODS */

    /* METHODS */

}

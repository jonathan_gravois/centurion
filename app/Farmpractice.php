<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Farmpractice extends Model {

    protected $table = 'farmpractices';
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function farm()
    {
        return $this->belongsTo('App\Farm', 'farm_id');
    }

    public function crop()
    {
        return $this->belongsTo('App\Crop', 'crop_id');
    }
    /* RELATIONSHIPS */

}

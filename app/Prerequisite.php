<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Prerequisite extends Model {

    protected $table = 'prerequisites';
    protected $guarded = ['id'];
    protected $dates = array('date_requested', 'date_received');
}

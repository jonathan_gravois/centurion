<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class County extends Model {

    protected $table = 'counties';
    public $timestamps = false;
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function states()
    {
        return $this->belongsTo('App\State', 'state_id');
    }

    public function farms()
    {
        return $this->hasMany('App\Farm','county_id');
    }

    public function defaults()
    {
        return $this->hasOne('App\Countiescropdefault');
    }

    public function locations()
    {
        return $this->belongsTo('App\Location', 'location_id');
    }
    /* RELATIONSHIPS */

}

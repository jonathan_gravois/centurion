<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Guarantor extends Model {

    protected $table = 'guarantors';
    protected $guarded = ['id'];

}

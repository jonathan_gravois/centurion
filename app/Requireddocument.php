<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Requireddocument extends Model {

    protected $table = 'requireddocuments';
    public $timestamps = false;
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function loantype()
    {
        return $this->belongsTo('App\Loantype', 'loantype_id');
    }
    /* RELATIONSHIPS */

}

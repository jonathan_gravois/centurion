<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Systemic extends Model {

    protected $table = 'systemics';
    protected $guarded = ['id'];

}

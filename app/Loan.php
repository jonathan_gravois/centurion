<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model {

	protected $table = 'loans';
    protected $guarded = ['id'];
    protected $dates = array('app_date', 'distributor_approval_date', 'decision_date', 'due_date', 'default_due_date');
    
    /* RELATIONSHIPS */
    public function applicants()
    {
        return $this->belongsTo('App\Applicant', 'applicant_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function committee()
    {
        return $this->hasMany('App\Committee');
    }

    public function corporations()
    {
        return $this->hasMany('App\Corporation');
    }

    public function crop()
    {
        return $this->belongsToMany('App\Crop');
    }

    public function cropdetails()
    {
        return $this->hasMany('App\Cropdetail');
    }

    public function cropyields()
    {
        return $this->hasMany('App\Cropyield');
    }

    public function distributor()
    {
        return $this->belongsTo('App\Distributor', 'distributor_id');
    }

    public function farmer()
    {
        return $this->belongsTo('App\Farmer', 'farmer_id');
    }

    public function farms()
    {
        return $this->hasMany('App\Farm');
    }

    public function farmexpenses()
    {
        return $this->hasMany('App\Farmexpense');
    }

    public function financials()
    {
        return $this->hasOne('App\Loanfinancial');
    }

    public function funduses()
    {
        return $this->hasMany('App\Expense');
    }

    public function insurance()
    {
        return $this->hasOne('App\Insurance');
    }

    public function loanconditions()
    {
        return $this->hasMany('App\Loancondition');
    }

    public function loancrop()
    {
        return $this->hasMany('App\Loancrop');
    }

    public function loanexceptions()
    {
        return $this->hasMany('App\Loanexception');
    }

    public function loanfinancial()
    {
        return $this->hasOne('App\Loanfinancial');
    }

    public function loanquestions()
    {
        return $this->hasMany('App\Loanquestion');
    }

    public function loanstatus()
    {
        return $this->belongsTo('App\Loanstatus', 'status_id');
    }

    public function loantype()
    {
        return $this->belongsTo('App\Loantype', 'loan_type_id');
    }

    public function location()
    {
        return $this->belongsTo('App\Location', 'loc_id');
    }

    public function partners()
    {
        return $this->hasMany('App\Partner');
    }

    public function prerequisites()
    {
        return $this->hasMany('App\Prerequisite');
    }

    public function priorliens()
    {
        return $this->hasOne('App\Priorlien');
    }

    public function regions()
    {
        return $this->belongsTo('App\Region', 'region_id');
    }

    public function reqdocs()
    {
        return $this->hasMany('App\Requireddocument');
    }

    public function systemics()
    {
        return $this->hasMany('App\Systemic');
    }

    public function ventures()
    {
        return $this->hasMany('App\Jointventure');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    /* RELATIONSHIPS */

    /* METHODS */
    /* METHODS */

}

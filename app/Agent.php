<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model {

    protected $table = 'agents';
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function agency()
    {
        return $this->belongsTo('App\Agency', 'agency_id');
    }
    /* RELATIONSHIPS */
}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Admingrader extends Model {

    protected $table = 'admingraders';
    public $timestamps = false;
    protected $guarded = ['id'];

}

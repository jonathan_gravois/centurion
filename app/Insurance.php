<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Insurance extends Model {

    protected $table = 'insurances';
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function agency()
    {
        return $this->belongsTo('App\Agency', 'agency_id');
    }

    public function agent()
    {
        return $this->belongsTo('App\Agents', 'agent_id');
    }

    public function county()
    {
        return $this->belongsTo('App\County', 'loancounty_id');
    }

    public function crop()
    {
        return $this->belongsTo('App\Crop', 'loancrop_id');
    }

    public function farm()
    {
        return $this->belongsTo('App\Farm', 'fsn', 'fsn');
    }

    public function practice()
    {
        return $this->belongsTo('App\Croppractice', 'croppractice_id');
    }
    /* RELATIONSHIPS */

}

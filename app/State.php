<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model {

    protected $table = 'states';
    public $timestamps = false;
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function counties()
    {
        return $this->hasMany('App\County');
    }
    /* RELATIONSHIPS */

}

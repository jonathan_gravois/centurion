<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Defaultexpense extends Model {

    protected $table = 'defaultexpenses';
    public $timestamps = false;
    protected $guarded = ['id'];

}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model {

    protected $table = 'calendars';
    protected $guarded = ['id'];
    protected $dates = ['start', 'end'];
}

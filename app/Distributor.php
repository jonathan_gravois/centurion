<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Distributor extends Model {

    protected $table = 'distributors';
    public $timestamps = false;
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function state()
    {
        return $this->belongsTo('App\State', 'state_id');
    }
    /* RELATIONSHIPS */

}

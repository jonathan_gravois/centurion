<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Matrix extends Model {

    protected $table = 'matrices';
    public $timestamps = false;
    protected $guarded = ['id'];

}

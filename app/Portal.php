<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Portal extends Model {

    protected $table = 'portals';
    public $timestamps = false;
    protected $guarded = ['id'];

    /* RELATIONSHIPS */

    /* RELATIONSHIPS */

    /* METHODS */

    /* METHODS */

}

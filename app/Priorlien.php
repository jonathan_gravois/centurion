<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Priorlien extends Model {

    protected $table = 'priorliens';
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function loan()
    {
        return $this->belongsTo('App\Loan', 'loan_id');
    }
    /* RELATIONSHIPS */
}

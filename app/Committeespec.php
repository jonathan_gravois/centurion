<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Committeespec extends Model {

    protected $table = 'committeespecs';
    protected $guarded = ['id'];

}

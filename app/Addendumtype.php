<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Addendumtype extends Model {

    protected $table = 'addendumtypes';
    public $timestamps = false;
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function addloan()
    {
        return $this->belongsTo('App\AddLoan');
    }
    /* RELATIONSHIPS */

}

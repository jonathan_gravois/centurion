<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Committee extends Model {

    protected $table = 'committees';
    public $timestamps = false;
    protected $guarded = ['id'];

    /* RELATIONSHIPS */
    public function role()
    {
        return $this->belongsTo('App\Role', 'role_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    /* RELATIONSHIPS */

}

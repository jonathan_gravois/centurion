<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Grainstorage extends Model {

    protected $table = 'grainstorages';
    protected $guarded = ['id'];
    protected $dates = ['contract_date', 'delivery_date'];

    /* RELATIONSHIPS */
    /* RELATIONSHIPS */

    /* METHODS */
    public function setContractDateAttribute($date)
    {
        $this->attributes['contract_date'] = Carbon::parse($date);
    }

    public function setDeliveryDateAttribute($date)
    {
        $this->attributes['delivery_date'] = Carbon::parse($date);
    }
    /* METHODS */
}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Pdfapp extends Model {

    protected $table = 'pdfapps';
    protected $guarded = ['id'];

}

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ag Resource Management Operations</title>

    {!! HTML::style('css/bootstrap.3.3.4.min.css') !!}

    {!! HTML::style('css/splash.css') !!}
</head>
<body>
<div class="container">
    @yield('content')
</div>
</body>
</html>

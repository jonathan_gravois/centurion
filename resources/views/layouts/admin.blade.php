<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LENDA Admin</title>

    {!! HTML::style('css/bootstrap.3.3.4.min.css') !!}

    {!! HTML::style('css/navbar.css') !!}
    {!! HTML::style('css/admin.css') !!}
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">LENDA</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <span class="navbar-brand navbar-right" href="#">ADMIN</span>
        </div><!--/.navbar-collapse -->
    </div>
</nav>

<div class="container" style="margin-top: 60px;">
    <div class="row">
        <div class="col-md-2">@include('admin/nav-admin')</div>
        <div class="col-md-10">@yield('content')</div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

@yield('footer')

</body>
</html>

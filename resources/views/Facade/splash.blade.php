@extends('layouts.master')

@section('content')

    <table>
        <tr style="height:420px;">
            <td valign="middle" style="width:50%;">
                {!! HTML::image('img/ARM_Logo.jpg') !!}
            </td>
            <td valign="middle" style="width:50%;padding:10px;">
                <h2 style="color: #006837;">Ag Resource Management</h2>
                <br/>
                <h4 style="color: #888888;font-style: italic;">-- providing risk management solutions for agribusiness and farmers --</h4>
                <br/>
                <p style="color: #333333;">Founded in 2009, Ag Resource Management's core business is providing niche financial solutions to the retail agriculture market. We focus on bringing value to our customers via the structuring of short-term financial risk. We provide production lending directly to the producer or in conjunction with a local supplier or lender. Ag Resource Management offers a diversified group of loan and crop insurance products to meet agriculture producers' financial and risk management needs.</p>
            </td>
        </tr>
    </table>

@endsection
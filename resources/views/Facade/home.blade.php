@extends('layouts.master')

@section('content')

    <div class="row">
        <a class="btn btn-primary" href="/">Splash Page</a>
    </div>

    <form class="form-horizontal" role="form" method="POST" action="{{ url('/v1/jwtauth') }}" style="margin-top: 100px;">
        <div class="form-group">
            <label class="col-md-4 control-label">E-Mail Address</label>
            <div class="col-md-6">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label">Password</label>
            <div class="col-md-6">
                <input type="password" class="form-control" name="password">
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">Login</button>
            </div>
        </div>
    </form>

@endsection
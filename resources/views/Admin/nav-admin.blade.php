<div id="menu" style="padding-top: 10px;">
    <!--TODO: Active Tab-->
    <div class="panel list-group">
        <a href="#" class="list-group-item head" data-toggle="collapse" data-target="#sm" data-parent="#menu">
            Feeders <span class="glyphicon glyphicon-cog pull-right"></span>
        </a>
        <div id="sm" class="sublinks collapse">
            <a class="list-group-item small" href="{{ url('admin/agencies') }}">
                Agents &amp; Agencies
            </a>
            <a class="list-group-item small" href="{{ url('admin/crops') }}">
                Crops
            </a>
            <a class="list-group-item small" href="{{ url('admin/distributors') }}">
                Distributors
            </a>
            <a class="list-group-item small" href="{{ url('admin/entitytypes') }}">
                Entity Types
            </a>
            <a class="list-group-item small" href="{{ url('admin/insurancetypes') }}">
                Insurance Types
            </a>
            <a class="list-group-item small" href="{{ url('admin/loantypes') }}">
                Loan Types
            </a>
            <a class="list-group-item small" href="{{ url('admin/locations') }}">
                Locations
            </a>
            <a class="list-group-item small" href="{{ url('admin/regions') }}">
                Regions
            </a>
            <a class="list-group-item small" href="{{ url('admin/roles') }}">
                Roles
            </a>
            <a class="list-group-item small" href="{{ url('admin/units') }}">
                Units of Measurement
            </a>
        </div>

        <a href="#" class="list-group-item head" data-toggle="collapse" data-target="#sn" data-parent="#menu">
            Library <span class="glyphicon glyphicon-book pull-right"></span>
        </a>
        <div id="sn" class="sublinks collapse">
            <a class="list-group-item small" href="{{ url('admin/calendar') }}">
                Calendar
            </a>
            <a class="list-group-item small" href="{{ url('admin/resources') }}">
                Employee Resources
            </a>
            <a class="list-group-item small" href="{{ url('admin/legaldocs') }}">
                Legal Documents
            </a>
            <a class="list-group-item small" href="{{ url('admin/loanapps') }}">
                Loan Applications (.pdf)
            </a>
            <a class="list-group-item small" href="{{ url('admin/loanproducts') }}">
                Loan Products
            </a>
            <a class="list-group-item small" href="{{ url('admin/policies') }}">
                Policies &amp; Procedures
            </a>
            <a class="list-group-item small" href="{{ url('admin/matrix') }}">
                Responsibility Matrix
            </a>
        </div>

        <a href="#" class="list-group-item head" data-toggle="collapse" data-target="#so" data-parent="#menu">
            LENDA <span class="glyphicon glyphicon-grain pull-right"></span>
        </a>
        <div id="so" class="sublinks collapse">
            <a class="list-group-item small" href="{{ url('admin/archiveloans') }}">
                Archive Loans
            </a>
            <a class="list-group-item small" href="{{ url('admin/backuplenda') }}">
                Backup LENDA
            </a>
            <a class="list-group-item small" href="{{ url('admin/importqb') }}">
                Import From QuickBooks
            </a>
        </div>

        <a href="#" class="list-group-item head" data-toggle="collapse" data-target="#sp" data-parent="#menu">
            Loans <span class="glyphicon glyphicon-usd pull-right"></span>
        </a>
        <div id="sp" class="sublinks collapse">
            <a class="list-group-item small" href="{{ url('admin/applicationvalues') }}">
                Application Values
            </a>
            <a class="list-group-item small" href="{{ url('admin/committeespecs') }}">
                Committee Specs
            </a>
            <a class="list-group-item small" href="{{ url('admin/defaultexpenses') }}">
                Default Expenses
            </a>
            <a class="list-group-item small" href="{{ url('admin/graderspecs') }}">
                Grader Specs
            </a>
            <a class="list-group-item small" href="{{ url('admin/insurancevalues') }}">
                Insurance Values
            </a>
            <a class="list-group-item small" href="{{ url('admin/loans') }}">
                Loans
            </a>
            <a class="list-group-item small" href="{{ url('admin/unitconvertor') }}">
                Measurements Convertor
            </a>
            <a class="list-group-item small" href="{{ url('admin/newloanscreens') }}">
                New Loan Screens
            </a>
            <a class="list-group-item small" href="{{ url('admin/prerequisites') }}">
                Prerequisites
            </a>
        </div>

        <a href="#" class="list-group-item head" data-toggle="collapse" data-target="#sr" data-parent="#menu">
            Reports <span class="glyphicon glyphicon-print pull-right"></span>
        </a>
        <div id="sr" class="sublinks collapse">
            <a class="list-group-item small" href="{{ url('admin/reports') }}">
                Reports
            </a>
        </div>

        <a href="#" class="list-group-item head" data-toggle="collapse" data-target="#ss" data-parent="#menu">
            Users <span class="glyphicon glyphicon-user pull-right"></span>
        </a>
        <div id="ss" class="sublinks collapse">
            <a class="list-group-item small" href="{{ url('admin/applicants') }}">
                Applicants
            </a>
            <a class="list-group-item small" href="{{ url('admin/farmers') }}">
                Farmers
            </a>
            <a class="list-group-item small" href="{{ url('admin/staff') }}">
                Staff
            </a>
            <a class="list-group-item small" href="{{ url('admin/home') }}">
                Users
            </a>
        </div>
    </div>
</div>

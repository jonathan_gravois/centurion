@extends('layouts.admin')

@section('content')
    <table class="table table-striped">
        @foreach($users as $user)
        <tr>
            <td>
                {!! $user->username !!}
            </td>
            <td>
                {!! $user->email !!}
            </td>
            <td>
                {!! $user->phone !!}
            </td>
            <td>
                {!! $user->portal->portal !!}
            </td>
            <td>
                reset password
            </td>
        </tr>
        @endforeach
    </table>
@endsection